#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys
import pytest

def parse(data):
    return ("".join([line.strip() for line in data])).split(",")


def algo(code):
    current_val = 0
    for c in code:
        current_val += ord(c)
        current_val *= 17
        current_val = current_val % 256

    return current_val


def answer_part_one(data):
    return sum(algo(code) for code in parse(data))


def answer_part_two(data):
    ops = parse(data)
    boxes = {}

    for op in ops:
        if op[-1] == "-":
            label = op[:-1]
            box = algo(label)
            if box in boxes and label in boxes[box]:
                del boxes[box][label]
        else:
            label, foc_len = op.split("=")
            box = algo(label)
            if box not in boxes:
                boxes[box] = {}
            boxes[box][label] = int(foc_len)

    out = 0

    for box in boxes:
        for i, label in enumerate(boxes[box].keys()):
            focal_length = boxes[box][label]
            power = (1 + box) * (i + 1) * focal_length
            print(f"{label}: {box + 1} * {i + 1} * {focal_length} = {power}")
            out += power

    return out


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="fifteen.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = ["rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"]

    @pytest.mark.parametrize(
            "code, expected",
            [
                ("HASH", 52),
                ("rn=1", 30),
                ("cm-", 253),
                ("qp=3", 97),
                ("ab=5", 197),
            ]
            )
    def test_algo(self, code, expected):
        assert algo(code) == expected

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 1320

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 145


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
