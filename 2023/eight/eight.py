#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys
import math


def parse_data(data):
    directions = data[0].strip()

    nodes = {}
    for line in data[2:]:
        node, branch = line.split("=")
        node = node.strip()
        left, right = branch.strip()[1:-1].split(",")
        nodes[node] = {"L": left.strip(), "R": right.strip()}

    return directions, nodes


def answer_part_one(data):
    directions, nodes = parse_data(data)

    current = "AAA"
    steps = 0
    dir_no = 0
    max_d = len(directions)

    while True:
        steps += 1
        current = nodes[current][directions[dir_no]]
        if current == "ZZZ":
            break
        dir_no = (dir_no + 1) % max_d

    return steps


class Map:
    def __init__(self, data):
        self.directions, self.nodes = parse_data(data)
        self.starters = [k for k in self.nodes if k.endswith("A")]
        self.period = len(self.directions)
        self.data = {}

    def analyse(self, nodes):
        for c in nodes:
            initial = c
            steps = 0
            dir_no = 0
            while True:
                steps += 1
                c = self.nodes[c][self.directions[dir_no]]
                if c.endswith("Z"):
                    break
                dir_no = (dir_no + 1) % self.period
            self.data[initial] = {"end": c, "period": steps}

    def result(self):
        return math.lcm(*[d["period"] for d in self.data.values()])


def answer_part_two(data):
    maps = Map(data)
    maps.analyse(maps.starters)

    return maps.result()


def debug(data):
    directions, nodes = parse_data(data)

    current = [k for k in nodes if k.endswith("A")]
    steps = 0
    dir_no = 0
    max_d = len(directions)

    while True:
        steps += 1
        current = [nodes[c][directions[dir_no]] for c in current]
        if all([c.endswith("Z") for c in current]):
            break
        dir_no = (dir_no + 1) % max_d

    return steps


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)
    parser.add_argument("-d", "--debug", help="Debug",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="eight.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data1 = [
"RL",
"",
"AAA = (BBB, CCC)",
"BBB = (DDD, EEE)",
"CCC = (ZZZ, GGG)",
    ]
    data2 = [
"LLR",
"",
"AAA = (BBB, BBB)",
"BBB = (AAA, ZZZ)",
"ZZZ = (ZZZ, ZZZ)",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data1)
        assert answer == 2

        answer = answer_part_one(self.data2)
        assert answer == 6

    def test_answer_part_two(self):
        data = [
            "LR",
            "",
            "11A = (11B, XXX)",
            "11B = (XXX, 11Z)",
            "11Z = (11B, XXX)",
            "22A = (22B, XXX)",
            "22B = (22C, 22C)",
            "22C = (22Z, 22Z)",
            "22Z = (22B, 22B)",
            "XXX = (XXX, XXX)",
        ]
        answer = answer_part_two(data)
        assert answer == 6


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.debug:
        answer = debug(get_data())

