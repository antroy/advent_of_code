#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import math
import sys


class Game:
    def __init__(self, line):
        game, grabs = line.split(":")
        self.number = int(game.strip().split(" ")[1])
        self.bag = {"red": 0, "green": 0, "blue": 0}
        for show in grabs.split(";"):
            for cubes in show.split(","):
                count, colour = cubes.strip().split(" ")
                self.bag[colour] = max(self.bag[colour], int(count))

    def possible(self, other_bag):
        return all(self.bag[x] <= other_bag[x] for x in self.bag.keys())

    def power(self):
        return math.prod(self.bag.values())


def get_games(data):
    return [Game(line) for line in data]


def answer_part_one(data):
    test_bag = {"red": 12, "green": 13, "blue": 14}
    games = get_games(data)

    return sum(game.number for game in games if game.possible(test_bag))


def answer_part_two(data):
    games = get_games(data)

    return sum(game.power() for game in games)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="two.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
        "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
        "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
        "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
        "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
        "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 8

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 2286


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
