#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import pytest
import sys


class Tile:
    def __init__(self, tile):
        self.energised = False
        self.op = {
            ".": self.dot,
            "-": self.h_split,
            "|": self.v_split,
            "\\": self.lr_mirror,
            "/": self.rl_mirror,
        }.get(tile)

    def dot(self, dir_in):
        return [dir_in]

    def h_split(self, dir_in):
        if dir_in in {"E", "W"}:
            return [dir_in]
        else:
            return ["E", "W"]

    def v_split(self, dir_in):
        if dir_in in {"N", "S"}:
            return [dir_in]
        else:
            return ["N", "S"]

    def lr_mirror(self, dir_in):
        return [{"E": "S", "S": "E", "W": "N", "N": "W"}[dir_in]]

    def rl_mirror(self, dir_in):
        return [{"E": "N", "N": "E", "W": "S", "S": "W"}[dir_in]]

    def process(self, dir_in):
        self.energised = True
        return self.op(dir_in)


class Beam:
    def __init__(self, node, direction):
        self.node = node
        self.direction = direction

    def __repr__(self):
        return f"{self.node}: {self.direction}"

    def __eq__(self, other):
        return self.__repr__() == other.__repr__()

    def __hash__(self):
        return hash(self.__repr__())


class Grid:
    def __init__(self, data):
        self.max_x = len(data) - 1
        self.max_y = len(data[0]) - 1
        grid = {}
        for x, line in enumerate(data):
            for y, cell in enumerate(line):
                grid[(x, y)] = Tile(cell)
        self.grid = grid

    def go(self, coord, next_dir):
        x, y = coord
        if next_dir == "N":
            return (x - 1, y) if x > 0 else None
        if next_dir == "W":
            return (x, y - 1) if y > 0 else None
        if next_dir == "S":
            return (x + 1, y) if x < self.max_x else None
        if next_dir == "E":
            return (x, y + 1) if y < self.max_y else None

    def follow_beams(self, start_beam=Beam((0, 0), "E")):
        beam_list = [start_beam]
        beam = beam_list.pop()
        beam_cache = set()

        while True:
            beam_cache.add(beam)
            tile = self.grid[beam.node]
            next_directions = tile.process(beam.direction)
            beams = [Beam(self.go(beam.node, d), d) for d in next_directions]
            beams = [b for b in beams if b.node and b not in beam_cache]
            if len(beams) == 1:
                beam = beams[0]
            else:
                beam_list.extend(beams)
                if not beam_list:
                    break
                beam = beam_list.pop()

    def energised(self):
        return len([n for n in self.grid.values() if n.energised])


def answer_part_one(data):
    grid = Grid(data)
    grid.follow_beams()

    return grid.energised()


def answer_part_two(data):
    grid = Grid(data)
    starting_beams = [Beam((0, y), "S") for y in range(grid.max_y)] + \
        [Beam((grid.max_x, y), "N") for y in range(grid.max_y)] + \
        [Beam((x, 0), "E") for x in range(grid.max_x)] + \
        [Beam((x, grid.max_y), "W") for x in range(grid.max_x)]

    max_energised = 0
    for beam in starting_beams:
        grid = Grid(data)
        grid.follow_beams(beam)
        max_energised = max(max_energised, grid.energised())

    return max_energised


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="sixteen.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = r""".|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....""".split("\n")

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 46

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 51

    def test_grid(self):
        grid = Grid(self.data)
        assert len(grid.grid) == 10 * 10

    @pytest.mark.parametrize(
        "tile, dir, exp",
        [
            (".", "E", ["E"]),
            (".", "S", ["S"]),
            (".", "W", ["W"]),
            (".", "N", ["N"]),
            ("-", "E", ["E"]),
            ("-", "S", ["E", "W"]),
            ("-", "W", ["W"]),
            ("-", "N", ["E", "W"]),
            ("|", "E", ["N", "S"]),
            ("|", "S", ["S"]),
            ("|", "W", ["N", "S"]),
            ("|", "N", ["N"]),
            ("\\", "E", ["S"]),
            ("\\", "S", ["E"]),
            ("\\", "W", ["N"]),
            ("\\", "N", ["W"]),
            ("/", "E", ["N"]),
            ("/", "N", ["E"]),
            ("/", "W", ["S"]),
            ("/", "S", ["W"]),
        ]
    )
    def test_tile(self, tile, dir, exp):
        tile = Tile(tile)
        dirs = tile.process(dir)
        assert tile.energised == True
        assert dirs == exp

    @pytest.mark.parametrize(
        "coords, dir, exp",
        [
            ((0, 0), "N", None),
            ((0, 0), "E", (0, 1)),
            ((0, 0), "S", (1, 0)),
            ((0, 0), "W", None),

            ((0, 9), "N", None),
            ((0, 9), "E", None),
            ((0, 9), "S", (1, 9)),
            ((0, 9), "W", (0, 8)),

            ((9, 0), "N", (8, 0)),
            ((9, 0), "E", (9, 1)),
            ((9, 0), "S", None),
            ((9, 0), "W", None),

            ((9, 9), "N", (8, 9)),
            ((9, 9), "E", None),
            ((9, 9), "S", None),
            ((9, 9), "W", (9, 8)),

            ((4, 4), "N", (3, 4)),
            ((4, 4), "E", (4, 5)),
            ((4, 4), "S", (5, 4)),
            ((4, 4), "W", (4, 3)),
        ]
    )
    def test_grid_go(self, coords, dir, exp):
        grid = Grid(self.data)
        assert grid.go(coords, dir) == exp


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
