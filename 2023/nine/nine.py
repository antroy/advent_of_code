#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys


def do_the_thing(seq):
    if all([n == 0 for n in seq]):
        return 0
    next_seq = [seq[n + 1] - seq[n] for n in range(0, len(seq) - 1)]

    return seq[-1] + do_the_thing(next_seq)


def parse(data):
    return [[int(x) for x in line.split()] for line in data]


def answer_part_one(data):
    count = 0
    for seq in parse(data):
        count += do_the_thing(seq)

    return count


def answer_part_two(data):

    return ""


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="nine.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
        "0 3 6 9 12 15",
        "1 3 6 10 15 21",
        "10 13 16 21 30 45",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 114

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == ""


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
