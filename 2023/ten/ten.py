#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys


class Field:
    def __init__(self, data):
        self.tiles = {
                "|": (self.n, self.s), "-": (self.e, self.w),
                "L": (self.n, self.e), "J": (self.n, self.w),
                "7": (self.w, self.s), "F": (self.s, self.e)
                }
        self.grid = {}
        for x, line in enumerate(data):
            self.grid[x] = {}
            for y, c in enumerate(line):
                self.grid[x][y] = c
                if c == "S":
                    self.start = (x, y)

        # start_connectors = [fn(*self.start) for fn in [self.n, self.e, self.s, self.w]]
        # start_connectors = [(c, self.at(*c))  for c in start_connectors if c]
        # start_connectors = [tile for tile in start_connectors if tile[1] in self.tiles]
        # exits = [(tile, [fn(*tile[0]) for fn in self.tiles[tile[1]]]) for tile in start_connectors]
        # candidates = [exit for exit in exits if self.start in exit[1]]
        # points = {[c for c in exit[1] if c != self.start][0] for exit in candidates}
        # for k, v in self.tiles.items():
        #     breakpoint()
        #     coords = {fn(*self.start) for fn in v}
        #     if coords == points:
        #         self.start_symbol = k

        # print(points)
        # print(self.start_symbol)


    def at(self, x, y):
        return self.grid[x][y]

    def n(self, x, y):
        if x == 0:
            return None
        return x - 1, y

    def e(self, x, y):
        if y >= (len(self.grid[0]) - 1):
            return None
        return x, y + 1

    def s(self, x, y):
        if x >= (len(self.grid) - 1):
            return None
        return x + 1, y

    def w(self, x, y):
        if y == 0:
            return None
        return x, y - 1

    def pipe_length(self):
        self.pipe_tiles = set()
        current = self.start
        self.pipe_tiles.add(current)
        steps = 0

        first_step_options = [self.n(*current), self.e(*current), self.s(*current)]
        first_step = None
        for point in first_step_options:
            if point and self.at(*point) in self.tiles:
                first_step = point

        previous = current
        current = first_step
        self.pipe_tiles.add(current)
        steps += 1

        while current != self.start:
            tile = self.at(*current)
            next_previous = current
            options = [direction(*current) for direction in self.tiles[tile]
                       if direction(*current) != previous]
            current = options[0]
            self.pipe_tiles.add(current)
            previous = next_previous
            steps += 1

        return steps

    def interior(self, tile_under_test):
        line_count = 0
        l_open = False
        j_open = False

        for i in range(tile_under_test[0], -1, -1):
            if (i, tile_under_test[1]) not in self.pipe_tiles:
                continue
            tile = self.at(i, tile_under_test[1])
            if l_open:
                if tile == "|":
                    continue
                if tile == "F":
                    l_open = False
                    continue
                if tile == "7":
                    l_open = False
                    line_count += 1
                    continue
            elif j_open:
                if tile == "|":
                    continue
                if tile == "F":
                    j_open = False
                    line_count += 1
                    continue
                if tile == "7":
                    j_open = False
                    continue
            elif tile == "-":
                line_count += 1
            elif tile == "L":
                l_open = True
            elif tile == "J":
                j_open = True

        return line_count % 2 == 1

    def interior_tile_count(self):
        candidates = []
        for x in range(len(self.grid)):
            for y in range(len(self.grid[0])):
                candidate = (x, y)
                if candidate not in self.pipe_tiles:
                    candidates.append(candidate)

        interiors = []
        for candidate in candidates:
            if self.interior(candidate):
                interiors.append(candidate)

        self.print_field(interiors)
        return len(interiors)

    def print_field(self, interiors):
        for x in range(len(self.grid)):
            line = []
            for y in range(len(self.grid[0])):
                if (x, y) in interiors:
                    line.append("I")
                else:
                    line.append(self.at(x, y))
            print("".join(line))


def answer_part_one(data):
    field = Field(data)

    return field.pipe_length() // 2


def answer_part_two(data):
    field = Field(data)
    field.pipe_length()

    return field.interior_tile_count()


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="ten.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
            "..F7.",
            ".FJ|.",
            "SJ.L7",
            "|F--J",
            "LJ...",
            ]

    def test_grid(self):
        field = Field(self.data)

        assert field.grid[0][0] == "."
        assert field.grid[0][2] == "F"
        assert field.grid[2][3] == "L"
        assert field.grid[3][0] == "|"
        assert field.grid[4][4] == "."
        assert field.start == (2, 0)
        assert field.grid[field.start[0]][field.start[1]] == "S"

    def test_grid_n(self):
        field = Field(self.data)
        assert field.n(0, 0) is None
        assert field.n(0, 2) is None
        assert field.n(1, 0) == (0, 0)
        assert field.n(4, 3) == (3, 3)

    def test_grid_e(self):
        field = Field(self.data)
        assert field.e(0, 4) is None
        assert field.e(2, 4) is None
        assert field.e(1, 0) == (1, 1)
        assert field.e(4, 3) == (4, 4)
        assert field.e(4, 0) == (4, 1)

    def test_grid_s(self):
        field = Field(self.data)
        assert field.s(4, 0) is None
        assert field.s(4, 2) is None
        assert field.s(1, 0) == (2, 0)
        assert field.s(3, 3) == (4, 3)

    def test_grid_w(self):
        field = Field(self.data)
        assert field.w(0, 0) is None
        assert field.w(3, 0) is None
        assert field.w(1, 1) == (1, 0)
        assert field.w(4, 4) == (4, 3)

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 8

    def test_answer_part_two_a(self):
        data = """...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........""".split("\n")
        answer = answer_part_two(data)
        assert answer == 4

    def test_answer_part_two_b(self):
        data = """.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...""".split("\n")
        answer = answer_part_two(data)
        assert answer == 8

    def test_interior(self):
        data = """.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...""".split("\n")
        field = Field(data)
        field.pipe_length()
        interior = field.interior((5, 1))

        assert not interior

    def test_answer_part_two_c(self):
        data = """FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L""".split("\n")
        answer = answer_part_two(data)
        assert answer == 10


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
