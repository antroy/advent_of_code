#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys


class Pattern:
    def __init__(self, data):
        self.rows = [list(line) for line in data]
        self.columns = [[self.rows[i][j] for i in range(len(self.rows))] for j in range(len(self.rows[0]))]


def find_mirror(pattern, find_in_rows):
    to_check = pattern.rows if find_in_rows else pattern.columns

    for i, line in enumerate(to_check[0:-1]):
        current_index, next_index = i, i + 1
        this_line = line
        is_mirror = True
        while this_line == to_check[next_index]:
            current_index, next_index = current_index - 1, next_index + 1
            if current_index < 0 or next_index >= len(to_check):
                if is_mirror:
                    return i + 1
            this_line = to_check[current_index]

    return 0


def get_scores(pattern):
    row_score = find_mirror(pattern, True)
    if row_score:
        return row_score * 100
    return find_mirror(pattern, False)


def parse(data):
    out = []
    rows = []
    data.append("") # Ensure we have a final blank line.

    for line in data:
        row = line.strip()
        if row:
            rows.append(row)
        elif rows:
            out.append(Pattern(rows))
            rows = []

    return out


def answer_part_one(data):
    patterns = parse(data)
    scores = [get_scores(pattern) for pattern in patterns]

    return sum(scores)


def answer_part_two(data):

    return ""


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="thirteen.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = """
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
""".split("\n")

    def test_parse(self):
        result = parse(self.data)
        assert len(result) == 2
        assert len(result[0].rows) == 7
        assert len(result[0].columns) == 9

    def test_find_mirror_rows(self):
        result = parse(self.data)
        assert find_mirror(result[0], True) == 0
        assert find_mirror(result[1], True) == 4
        assert len(result) == 2

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 405

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == ""


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
