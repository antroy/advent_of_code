#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys, re
from collections import defaultdict


def parse_line(line):
    card, numbers = [x.strip() for x in line.split(":")]
    card_no = int(re.search(r"\d+", card).group(0))
    w, y = numbers.split("|")
    winning = set(w.strip().split())
    yours = set(y.strip().split())
    return card_no, winning, yours


def score(card_no, winning, yours):
    c = count(winning, yours)
    if not c:
        return 0

    return 2 ** (c - 1)


def count(winning, yours):
    return len(winning.intersection(yours))


def accumulate_cards(cards, results):
    new_results = {**results}
    if not cards:
        return new_results

    rest, last = cards[:-1], cards[-1]
    no, winning, yours = last
    wins = count(winning, yours)
    new_results[no] = 1

    for x in range(wins):
        next_card = no + x + 1
        new_results[no] += results.get(next_card, 0)

    return accumulate_cards(rest, new_results)


def answer_part_one(data):
    cards = [parse_line(line) for line in data]
    scores = [score(*c) for c in cards]

    return sum(scores)


def answer_part_two(data):
    cards = [parse_line(line) for line in data]

    accumulation = accumulate_cards(cards, {})

    return sum(accumulation.values())
    

def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="four.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
"Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
"Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
"Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
"Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
"Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
    ]

    def test_score(self):
        assert score(*parse_line(self.data[0])) == 8
        assert score(*parse_line(self.data[1])) == 2
        assert score(*parse_line(self.data[4])) == 0

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 13

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 30

    def test_accumulate_cards(self):
        cards = [parse_line(line) for line in self.data[-2:-1]]
        assert accumulate_cards(cards, {}) == {5: 1}

    def test_accumulate_cards_2(self):
        cards = [parse_line(line) for line in self.data[-3:-1]]
        assert accumulate_cards(cards, {}) == {4: 2, 5: 1}

    def test_accumulate_cards_3(self):
        cards = [parse_line(line) for line in self.data[-4:-1]]
        assert accumulate_cards(cards, {}) == {3: 4, 4: 2, 5: 1}

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
