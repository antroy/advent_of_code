#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import math, sys

def parse(data):
    times = data[0].split(":")[1].strip().split()
    records = data[1].split(":")[1].strip().split()
    times = [int(t) for t in times]
    records = [int(r) for r in records]
    return zip(times, records)


def is_winner(pause, duration, record):
    remainder = duration - pause
    speed = pause
    distance = speed * remainder

    return distance > record


def get_win_count(duration, record):
    better_distances = 0
    for pause in range(duration):
        if is_winner(pause, duration, record):
            better_distances += 1
    return better_distances


def answer_part_one(data):
    races = parse(data)

    return math.prod(
        [get_win_count(duration, record) for duration, record in races]
    )


def answer_part_two(duration, record):

    return get_win_count(duration, record)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="six.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
        "Time:      7  15   30",
        "Distance:  9  40  200"
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 288

    def test_answer_part_two(self):
        answer = answer_part_two(71530, 940200)
        assert answer == 71503


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        duration = 48876981
        record = 255128811171623
        answer = answer_part_two(duration, record)
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
