#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import re, string, sys


def answer_part_one(data):
    digits = [[d for d in line if d in string.digits] for line in data]
    sums = [int(f"{digit[0]}{digit[-1]}") for digit in digits]

    return sum(sums)


def answer_part_two(data):
    number_dict = {
        "one": "1", "two": "2", "three": "3", "four": "4", "five": "5", 
        "six": "6", "seven": "7", "eight": "8", "nine": "9"
        }
    pattern = f"^{'|'.join(number_dict.keys())}"

    def replacement(line):
        if not line:
            return ""
        m = re.match(pattern, line)
        if m:
            num = number_dict[m.group(0)]
            rest = line[len(m.group(0)) - 1:]
            return num + replacement(rest)
        return line[0] + replacement(line[1:])

    normalised = [replacement(line) for line in data]

    return answer_part_one(normalised)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="one.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


class TestExampleData():
    data1 = [
            "1abc2",
            "pqr3stu8vwx",
            "a1b2c3d4e5f",
            "treb7uchet",
    ]

    data2 = [
            "two1nine",
            "eightwothree",
            "abcone2threexyz",
            "xtwone3four",
            "4nineeightseven2",
            "zoneight234",
            "7pqrstsixteen",
            ]
    def test_answer_part_one(self):
        answer = answer_part_one(self.data1)
        assert answer == 142

    def test_answer_part_two(self):
        answer = answer_part_two(self.data2)
        assert answer == 281

    def test_answer_part_two_parse_bug(self):
        answer = answer_part_two(["eightwo"])
        assert answer == 82

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
