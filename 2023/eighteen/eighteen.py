#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys
import re
import pytest


class Instruction:
    def __init__(self, line):
        pattern = r"([LRUD]) (\d+) \(#([0-9a-f]{6})\)"
        m = re.match(pattern, line)
        self.dir = m.group(1)
        self.count = int(m.group(2))
        self.colour = m.group(3)


def dimensions(grid):
    min_x, max_x, min_y, max_y = 0, 0, 0, 0
    for k in grid.keys():
        min_x = min(min_x, k[0])
        max_x = max(max_x, k[0])
        min_y = min(min_y, k[1])
        max_y = max(max_y, k[1])

    return min_x, max_x, min_y, max_y


def is_inside(grid, node):
    if node in grid:
        return True
    min_x, max_x, min_y, max_y = dimensions(grid)

    diagonal = [(node[0] + i, node[1] + i) for i in range(max(max_x, max_y))]

    crossings = [d for d in diagonal if d in grid]
    return len(crossings) % 2 == 0


def dig(data):
    instructons = [Instruction(line) for line in data]
    current = (0, 0)
    grid = []

    for i in instructons:
        for j in range(i.count):
            if i.dir == "R":
                node = (current[0], current[1] + (j + 1))
            if i.dir == "L":
                node = (current[0], current[1] - (j + 1))
            if i.dir == "D":
                node = (current[0] + (j + 1), current[1])
            if i.dir == "U":
                node = (current[0] - (j + 1), current[1])
            grid.append(node)

            current = node

    return grid


def shoelace_area(points):
    left_lace = 0
    right_lace = 0

    for i in range(len(points)):
        left = points[i][0] * points[(i + 1) % len(points)][1]
        right = points[(i + 1) % len(points)][0] * points[i][1]

        left_lace += left
        right_lace += right

    return abs(left_lace - right_lace) // 2


# picks theorem: A = i + b/2 + 1
# We know A from shoelace
# So i = A - 1 - b/2
# But we want the interior + boundary
# So: total = A - 1 - b/2 + b = A -1 + b/2

def point_count(area, boundary):
    return area - 1 + (boundary // 2)




def answer_part_one(data):
    grid = dig(data)
    return shoelace_area(grid)


def answer_part_two(data):

    return ""


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="eighteen.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
        "R 6 (#70c710)",
        "D 5 (#0dc571)",
        "L 2 (#5713f0)",
        "D 2 (#d2c081)",
        "R 2 (#59c680)",
        "D 2 (#411b91)",
        "L 5 (#8ceee2)",
        "U 2 (#caa173)",
        "L 1 (#1b58a2)",
        "U 2 (#caa171)",
        "R 2 (#7807d2)",
        "U 3 (#a77fa3)",
        "L 2 (#015232)",
        "U 2 (#7a21e3)",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 62

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == ""


def test_dimensions():
    grid = {(-3, 8): None, (-7, -3): None, (4, 0): None, (-7, -5): None}
    assert dimensions(grid) == (-7, 4, -5, 8)


@pytest.mark.parametrize("grid, exp",[
    ([(0, 0), (0, 1), (1, 1), (1, 0)], 4),
    ([(0, 0), (1, 0), (1, 1), (0, 1)], 4),
])
def test_point_count(grid, exp):
    area = shoelace_area(grid)
    assert point_count(area, len(grid)) == exp


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
