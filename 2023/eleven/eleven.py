#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys


class Universe:
    def __init__(self, data, expansion=2):
        grid = []
        last_galaxy = 0
        self.row_weights = []
        self.col_weights = []

        for line in data:
            if not line.strip():
                continue
            grid.append([])
            for c in line:
                if c == ".":
                    grid[-1].append("")
                if c == "#":
                    last_galaxy += 1
                    grid[-1].append(str(last_galaxy))
            self.row_weights.append(1 if "".join(grid[-1]) else expansion)

        out = []
        columns = [[line[x] for line in grid] for x in range(len(grid[0]))]
        self.col_weights = [(1 if "".join(col) else expansion) for col in columns]

        self.grid = grid

    def coords(self):
        out = []
        for x, line in enumerate(self.grid):
            for y, c in enumerate(line):
                if c:
                    out.append((c, (x, y)))

        return out


    def dist(self, a, b):
        ax, bx = sorted([a[1][0], b[1][0]])
        ay, by = sorted([a[1][1], b[1][1]])
        
        return sum(self.row_weights[ax:bx]) + sum(self.col_weights[ay:by])


    def coord_distances(self, coords):
        if len(coords) <= 1:
            return []
        first, rest = coords[0], coords[1:]
        distances = [self.dist(first, coord) for coord in rest]
        distances.extend(self.coord_distances(rest))
        return distances


def answer_part_one(data):
    universe = Universe(data)
    coords = universe.coords()
    distances = universe.coord_distances(coords)

    return sum(distances)


def answer_part_two(data, expansion=1000000):
    universe = Universe(data, expansion)
    coords = universe.coords()
    distances = universe.coord_distances(coords)

    return sum(distances)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="eleven.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = """...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....""".split("\n")

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 374

    def test_answer_part_two_10(self):
        answer = answer_part_two(self.data, expansion=10)
        assert answer == 1030

    def test_answer_part_two_100(self):
        answer = answer_part_two(self.data, expansion=100)
        assert answer == 8410

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
