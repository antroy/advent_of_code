#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import re, sys


def parse_data(data):
    chain = []
    out = {"chain": chain}
    current_ranges = None
    dest = None

    for line in [l for l in data if l.strip()]:
        if line.startswith("seeds:"):
            seeds = [int(s) for s in line.split(":")[1].strip().split()]
            out["seeds"] = seeds
            out["seed_ranges"] = [(seeds[i], seeds[i] + seeds[i+1] - 1) for i in range(0, len(seeds), 2)]
            continue
        map_name_match = re.match(r"\s*(\w+)-to-(\w+) map:\s*", line)
        if map_name_match:
            if current_ranges:
                current_ranges.sort()
            current_ranges = []
            source, dest = map_name_match.groups()
            chain.append((source, dest))
            out[(source, dest)] = current_ranges
            continue
        dest_start, source_start, length = [int(part) for part in line.split()]
        current_ranges.append((source_start, dest_start, length))
    current_ranges.sort()

    return out


def get_value(thing, mappings):
    for mapping in mappings:
        source, dest, r = mapping
        if thing >= source and thing <= source + r:
            return thing - source + dest
    return thing


def get_locations(almanac):
    out = []
    for seed in almanac["seeds"]:
        thing = seed
        for element in almanac["chain"]:
            mappings = almanac[element]
            thing = get_value(thing, mappings)
        out.append(thing)
    return out


def next_ranges(thing_ranges, mappings):
    out = []

    for thing_range in thing_ranges:
        t_min, t_max = thing_range

        for mapping in mappings:
            s_min, s_max, offset = mapping
            if t_max < s_min: # thing is entirely before the range
                out.append((t_min, t_max))
                break
            elif t_min < s_min and t_max <= s_max: # thing overlaps mapping to the left
                out.append((t_min, s_min - 1))
                out.append((s_min - offset, t_max - offset))
                break
            elif t_min >= s_min and t_max <= s_max: # mapping range contains thing range
                out.append((t_min - offset, t_max - offset))
                break
            elif t_min < s_min and t_max > s_max: # mapping range contained by thing range
                out.append((t_min, s_min - 1))
                out.append((s_min - offset, s_max - offset))
                t_min = s_max + 1
            elif t_min > s_max:  # thing entirely to the right of mapping
                continue
            elif t_min >= s_min and t_max > s_max:  # thing overlaps mapping to the right
                out.append((t_min - offset, s_max - offset))
                t_min = s_max + 1
            else:
                print("Should not have got here!!")
                sys.exit(1)

        else:
            # Append any part of the initial range that is left over.
            out.append((t_min, t_max))

    return out


def transform(start, dest, number):
    offset = start - dest
    return (start, start + number -1, offset)


def transform_mapping(mapping):
    return [transform(start, dest, number) for start, dest, number in mapping]


def lowest_location_for_range(almanac):
    thing_range = almanac["seed_ranges"]
    for element in almanac["chain"]:
        mappings = transform_mapping(almanac[element])
        thing_range = next_ranges(thing_range, mappings)

    return thing_range


def answer_part_one(data):
    almanac = parse_data(data)
    locations = get_locations(almanac)

    return min(locations)


def answer_part_two(data):
    almanac = parse_data(data)
    locations = lowest_location_for_range(almanac)

    return min([loc[0] for loc in locations])


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="five.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = """
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
""".split("\n")

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 35

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 46

    def test_get_almanac(self):
        almanac = parse_data(self.data)
        assert almanac["seeds"] == [79, 14, 55, 13]
        assert almanac["seed_ranges"] == [(79, 92), (55, 67)]
        assert ("seed", "soil") in almanac["chain"]
        assert ("water", "light") in almanac["chain"]
        assert ("humidity", "location") in almanac["chain"]
        assert almanac[("seed", "soil")] == [(50, 52, 48), (98, 50, 2)]

    def test_get_value(self):
        mappings = [(98, 50, 2), (50, 52, 48)]
        assert get_value(79, mappings) == 81
        assert get_value(14, mappings) == 14
        assert get_value(55, mappings) == 57
        assert get_value(13, mappings) == 13

    def test_next_ranges_no_overlap(self):
        thing = [(3, 5)]
        ranges = [(6, 7, -15), (12, 14, -1)]

        assert next_ranges(thing, ranges) == [(3, 5)]

    def test_next_ranges_some_overlap(self):
        thing = [(3, 7)]
        ranges = [(6, 8, -15), (12, 14, -1)]

        assert next_ranges(thing, ranges) == [(3, 5), (21, 22)]

    def test_next_ranges_double_overlap(self):
        thing = [(8, 17)]
        ranges = [(6, 21, 4), (12, 13, 3)]
        ranges = [(6, 9, -15), (12, 14, -1)]

        assert next_ranges(thing, ranges) == [(23, 24), (10, 11), (13, 15), (15, 17)]

    def test_next_ranges_thing_contained_in_range(self):
        thing = [(8, 9)]
        ranges = [(6, 9, -15), (12, 14, -1)]

        assert next_ranges(thing, ranges) == [(23, 24)]


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
