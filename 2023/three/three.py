# !/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys


def parsed_data(data):
    numbers = []
    symbols = set()

    for i, line in enumerate(data):
        result = parse_line(line, i)
        numbers.extend(result["numbers"])
        symbols.update(result["symbols"])

    return numbers, symbols



def answer_part_one(data):
    numbers, symbols = parsed_data(data)

    parts = []
    for number in numbers:
        if adjacent(number, symbols):
            parts.append(number.number)

    return sum(parts)


def answer_part_two(data):
    numbers, symbols = parsed_data(data)

    gear_ratios = [gear_ratio(s, numbers) for s in symbols]

    return sum(g for g in gear_ratios if g)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="three.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


def parse_line(line, line_no):
    out = {"numbers": [], "symbols": []}
    number = []
    coord = None

    for i, c in enumerate(line):
        if c.isdigit():
            if not number:
                coord = (line_no, i)
            number.append(c)
        else:
            if number:
                out["numbers"].append(Num(int("".join(number)), coord))
                number = []
                coord = None
            if c == ".":
                continue
            else:
                out["symbols"].append((c, (line_no, i)))
    if number:
        out["numbers"].append(Num(int("".join(number)), coord))
        number = []
        coord = None

    return out


class Num:
    def __init__(self, number, coords):
        self.number, self.coords = number, coords

    def __eq__(self, other):
        return self.number == other.number and self.coords == other.coords

    def __str__(self):
        return f"{self.number}: {self.coords}"

    def __repr__(self):
        return self.__str__()


def gear_ratio(symbol, numbers):
    if symbol[0] != "*":
        return None
    parts_adjacent = [n for n in numbers if adjacent(n, {symbol})]

    if len(parts_adjacent) == 2:
        return parts_adjacent[0].number * parts_adjacent[1].number

    return None


def adjacent(num, symbols):
    coords = set()
    for i in range(len(str(num.number))):
        x, y = num.coords
        # in line
        coords.add((x, y + i))
        if y - 1 >= 0:
            coords.add((x, y - 1))
        coords.add((x, y + i + 1))

        # above
        if x - 1 >= 0:
            coords.add((x - 1, y + i))
            coords.add((x - 1, y - 1))
            coords.add((x - 1, y + i + 1))

        # below
        coords.add((x + 1, y + i))
        if y - 1 >= 0:
            coords.add((x + 1, y - 1))
        coords.add((x + 1, y + i + 1))

    return coords.intersection({s[1] for s in symbols})


class TestExampleData():
    data = [
        "467..114..",
        "...*......",
        "..35..633.",
        "......#...",
        "617*......",
        ".....+.58.",
        "..592.....",
        "......755.",
        "...$.*....",
        ".664.598..",
    ]

    def test_adjacent(self):
        assert adjacent(Num(467, (0, 0)), {("*", (1, 3))})
        assert adjacent(Num(35, (2, 2)), {("*", (1, 3))})

    def test_parse_line_gets_numbers(self):
        result = parse_line(self.data[0], 0)
        assert result["numbers"] == [
            Num(467, (0, 0)),
            Num(114, (0, 5)),
        ]
        assert result["symbols"] == []
        assert parse_line("..4", 0)["numbers"] == [Num(4, (0, 2))]

    def test_parse_line_gets_symbols(self):
        result = parse_line(self.data[1], 1)
        assert result["numbers"] == []
        assert result["symbols"] == [("*", (1, 3))]

        assert parse_line("..*", 0)["symbols"] == [("*", (0, 2))]


    def test_parse_line_gets_both(self):
        result = parse_line(self.data[4], 4)
        assert result["numbers"] == [
            Num(617, (4, 0)),
        ]
        assert result["symbols"] == [("*", (4, 3))]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 4361

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 467835


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
