#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
from collections import defaultdict
import sys
import pytest


class Hand:
    def __init__(self, input):
        self.cards, self.bid = input.split()
        self.bid = int(self.bid)

    def __str__(self):
        return f"{self.cards}: {self.bid}"

    def __repr__(self):
        return self.__str__()


def card_to_hex(cards):
    trans_table = str.maketrans("AKQJT", "edcba")
    return int(cards.translate(trans_table), 16)


def hand_score(hand):
    card_bag = defaultdict(lambda: 0)
    for c in hand:
        card_bag[c] += 1

    values = set(card_bag.values())

    if len(card_bag) == 1:
        # Five of a kind
        return 7

    if 4 in values:
        # Four of a kind
        return 6

    if 3 in values:
        if 2 in values:
            # Full house
            return 5
        else:
            # 3 of a kind
            return 4

    if 2 in values:
        if len(card_bag) == 3:
            # 2 pair
            return 3
        else:
            # pair
            return 2
    return 1


def card_to_hex2(cards):
    trans_table = str.maketrans("AKQJT", "edc1a")
    return int(cards.translate(trans_table), 16)


def hand_score2(hand):
    card_bag = defaultdict(lambda: 0)

    hand = hand.replace("J", "")
    joker_count = 5 - len(hand)

    if joker_count == 0:
        return hand_score(hand)

    for c in hand:
        card_bag[c] += 1

    values = set(card_bag.values())

    if len(card_bag) <= 1:
        # Five of a kind as we can add 3 jokers
        return 7

    if len(card_bag) == 2:
        if 1 in values:
            # Jokers can match the non 1 card to make 4
            return 6
        else:
            # we must have 2 pairs, so go for full house
            return 5
    if len(card_bag) == 3:
        # we have 4 cards with a pair, so add one to the pair
        # or we have 3 unique cards, so add 2 to one of them
        return 4

    if len(card_bag) == 4:
        # We have already dealt with 5 cards, so these must all be unique
        # Add a card for a pair
        return 2



def hand_key(hand):
    base_value = card_to_hex(hand.cards)
    hand_value = hand_score(hand.cards)

    return hand_value, base_value


def hand_key2(hand):
    base_value = card_to_hex2(hand.cards)
    hand_value = hand_score2(hand.cards)

    return hand_value, base_value


def result(data, sort_key):
    hands = [Hand(line) for line in data]
    ranked_hands = sorted(hands, key=sort_key)
    return sum([(i + 1) * hand.bid for i, hand in enumerate(ranked_hands)])


def answer_part_one(data):
    return result(data, hand_key)


def answer_part_two(data):
    return result(data, hand_key2)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="seven.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
        "32T3K 765",
        "T55J5 684",
        "KK677 28",
        "KTJJT 220",
        "QQQJA 483",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 6440

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 5905


def test_hand_initialization():
    h = Hand("T55J5 684")
    assert h.cards == "T55J5"
    assert h.bid == 684


@pytest.mark.parametrize(
    "hand, exp", [
        ("55555", (7, 0x55555)),
        ("AKAAA", (6, 0xedeee)),
        ("5J5J5", (5, 0x5b5b5)),
        ("95Q55", (4, 0x95c55)),
        ("553KK", (3, 0x553dd)),
        ("5434Q", (2, 0x5434c)),
        ("872K1", (1, 0x872d1)),
    ]
)
def test_hand_key(hand, exp):
    assert hand_key(Hand(f"{hand} 1")) == exp


@pytest.mark.parametrize(
    "card, exp",
    [("A", 14), ("Q", 12), ("T", 10), ("AQT6K", 0xeca6d)]
)
def test_card_to_hex(card, exp):
    assert card_to_hex(card) == exp


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
