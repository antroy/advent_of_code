#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys


def tilt(col):
    # "O..#.O#O...O..O."
    parts = col.split("#")
    out = []
    for part in parts:
        length = len(part)
        new_part = part.replace(".", "")
        removed = "." * (length - len(new_part))
        out.append(removed + new_part)

    return "#".join(out)


def col_weight(col):
    return sum([i + 1 for i, c in enumerate(col) if c == "O"])


def tilted(cols):
    return [tilt(col) for col in cols]


def weight(tilted_cols):
    return sum(col_weight(col) for col in tilted_cols)


def tilted_weight(cols):
    return weight(tilt(col) for col in cols)


def rotate(rows):
    return ["".join(reversed([rows[i][j] for i in range(len(rows))])) for j in range(len(rows[0]))]


def spin(cols):
    next_lines = cols
    for i in range(4):
        tilted_lines = tilted(next_lines)
        next_lines = rotate(tilted_lines)

    return next_lines


def answer_part_one(data):
    return tilted_weight(rotate(data))


def answer_part_two(data):
    #  data = [
            #  "O....#....",
            #  "O.OO#....#",
            #  ".....##...",
            #  "OO.#O....O",
            #  ".O.....O#.",
            #  "O.#..O.#.#",
            #  "..O..#O..O",
            #  ".......O..",
            #  "#....###..",
            #  "#OO..#....",
    #  ]
    this_cols = rotate(data)
    prev_cols = None
    wts = []

    for i in range(1000000000):
        prev_cols = this_cols
        this_cols = spin(prev_cols)
        if i > 1400:
            wt = weight(this_cols)
            wts.append(wt)
        if i > 1500:
            breakpoint()

        if set(this_cols) == set(prev_cols):
            break
    
    return weight(this_cols)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="fourteen.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
            "O....#....",
            "O.OO#....#",
            ".....##...",
            "OO.#O....O",
            ".O.....O#.",
            "O.#..O.#.#",
            "..O..#O..O",
            ".......O..",
            "#....###..",
            "#OO..#....",
    ]

    def test_tilt(self):
        col = "O..#.O#O...O..O."
        assert tilt(col) == "..O#.O#......OOO"

    def test_weight(self):
        col = "..O#.O#......OOO"
        assert col_weight(col) == 3 + 6 + 14 + 15 + 16

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 136

    def test_rotate_once(self):
        small_data = ["O..", "..#", "OOO"]
        cols = rotate(small_data)
        assert cols == ["O.O", "O..", "O#."]

    def test_rotate_all_the_way(self):
        """
        .#O
        O.O
        """
        cols = ["O.", ".#", "#O"]
        rotate_west = rotate(cols)
        assert set(rotate_west) == {"#.O", "O#."}

        rotate_south = rotate(rotate_west)
        assert set(rotate_south) == {".O", "#.", "O#"}

        rotate_east = rotate(rotate_south)
        assert set(rotate_east) == {"O.#", ".#O"}

        rotate_north = rotate(rotate_east)
        assert rotate_north == cols

    def test_spin(self):
        cols = ["O..", ".O#", "#O."]
        spun = spin(cols)
        assert set(spun) == {"...", "OO#", "#O."}

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 64


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
