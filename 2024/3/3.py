#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import re
import sys

MUL_PATTERN = r"mul\(([0-9]{1,3}),([0-9]{1,3})\)"

def answer_part_one(data):

    matches = re.findall(MUL_PATTERN, data)

    result = sum([int(m[0]) * int(m[1]) for m in matches])

    return result


def answer_part_two(data):
    switched = r"(do\(\)|don't\(\)|" + MUL_PATTERN + ")"
    matches = re.findall(switched, data)
    on = True
    result = 0

    for match in matches:
        if match[0] == "don't()":
            on = False
        if match[0] == "do()":
            on = True
        else:
            if on:
                result += int(match[1]) * int(match[2])
    return result


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="3.txt"):
    with open(filename) as fh:
        return "".join([line.strip() for line in fh])


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():

    def test_answer_part_one(self):
        data = "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5)))"
        answer = answer_part_one(data)
        assert answer == 161

    def test_answer_part_two(self):
        data = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5)))"
        answer = answer_part_two(data)
        assert answer == 48


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
