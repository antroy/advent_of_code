#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import re
import platform
import sys

def parse_data(data):
    out = []
    for line in data:
        m = re.match(r"(\d+): ([0-9 ]+)", line)
        equation = {"tot": int(m[1]), "nums": [int(d) for d in m[2].split()]}
        out.append(equation)

    return out
        
def mul(x, y):
    return x * y

def add(x, y):
    return x + y

def concat(x, y):
    return int(str(x) + str(y))

combo_cache = {}

def combos(count, operators):
    op_key = str(operators)
    if count in combo_cache.get(op_key, {}):
        return combo_cache[op_key][count]
    out = set()
    if count == 1:
        for op in operators:
            out.add((op, ))
    if count > 1:
        previous = combos(count - 1, operators)
        for combo in previous:
            for op in operators:
                out.add(combo + (op,))

    if not op_key in combo_cache:
        combo_cache[op_key] = {}

    combo_cache[op_key][count] = out

    return out


def solvable(total, numbers, operators):
    for ops in combos(len(numbers) - 1, operators):
        result = None
        for i, op in enumerate(ops):
            if not result:
                result = op(numbers[i], numbers[i+1])
            else:
                result = op(result, numbers[i+1])

        if total == result:
            return True
    return False
        

def answer_part_one(data):
    equations = parse_data(data)

    return sum([
        eq["tot"] 
        for eq in equations 
        if solvable(eq["tot"], eq["nums"], [add, mul])
        ])


def answer_part_two(data):
    equations = parse_data(data)

    return sum([
        eq["tot"] 
        for eq in equations 
        if solvable(eq["tot"], eq["nums"], [add, mul, concat])
        ])


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="7.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    if platform.system() == "Linux":
        with Popen(["xclip", "-selection", "clipboard"],
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()
    if platform.system() == "Darwin":
        with Popen("pbcopy",
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()


data = [
        "190: 10 19",
        "3267: 81 40 27",
        "83: 17 5",
        "156: 15 6",
        "7290: 6 8 6 15",
        "161011: 16 10 13",
        "192: 17 8 14",
        "21037: 9 7 18 13",
        "292: 11 6 16 20",
]

def test_answer_part_one():
    answer = answer_part_one(data)
    assert answer == 3749

def test_answer_part_two():
    answer = answer_part_two(data)
    assert answer == 11387

def test_parse_data():
    res = parse_data(data[0:2])

    assert res[0]["tot"] == 190
    assert res[0]["nums"] == [10, 19]
    assert res[1]["tot"] == 3267
    assert res[1]["nums"] == [81, 40, 27]

def test_solvable():
    assert solvable(190, [10, 19], [add, mul])
    assert solvable(3267, [81, 40, 27], [add, mul])

    assert not solvable(21037, [9, 7, 18, 13], [add, mul])
    assert not solvable(83, [17, 5], [add, mul])

def test_combos():
    res0 = combos(0, [add, mul])
    res1 = combos(1, [add, mul])
    res2 = combos(2, [add, mul])

    assert res0 == set()
    assert res1 == {(add,), (mul,)}
    assert res2 == {(add,add), (mul,mul), (add, mul), (mul,add)}


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
