#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys


up = (0, -1)
down = (0, 1)
left = (-1, 0)
right = (1, 0)
ul = (-1, -1)
ur = (1, -1)
dl = (-1, 1)
dr = (1, 1)


def answer_part_one(data):
    grid = Grid(data)

    counts = [count_xmas_for(cell) for cell in grid.grid.values()]

    return sum(counts)

def count_xmas_for(cell):
    directions = [up, down, left, right, ul, ur, dl, dr]

    has_xmas = [xmas_in_dir(cell, d) for d in directions]
    return len([b for b in has_xmas if b])

def answer_part_two(data):
    grid = Grid(data)

    counts = [1 for cell in grid.grid.values() if x_mas_cell(cell)]

    return sum(counts)

def xmas_in_dir(cell, direction):
    if cell.data != "X":
        return False

    grid = cell.grid
    current_cell = cell 

    for letter in "MAS":
        current_cell = grid.at(current_cell.x, current_cell.y, direction)
        if not current_cell:
            return False
        if current_cell.data != letter:
            return False

    return True

def x_mas_cell(cell):
    if cell.data != "A":
        return False

    grid = cell.grid
    x, y = cell.x, cell.y

    diag1 = {c.data for c in [grid.at(x, y, ul), grid.at(x, y, dr)] if c}
    diag2 = {c.data for c in [grid.at(x, y, ur), grid.at(x, y, dl)] if c}

    return (diag1 & diag2) == {"M", "S"}


def find_word(word, cells):
    if not word:
        return len(cells)

    first, rest = word[0], word[1:]
    cells_with_first = [cell for cell in cells if cell.data == first]
    
    count = 0
    for cell in cells_with_first:
        count += find_word(rest, cell.surrounding())

    return count


class Cell:
    def __init__(self, x, y, data, grid):
        self.x = x
        self.y = y
        self.data = data
        self.grid = grid

    def surrounding(self):
        x = self.x
        y = self.y

        return [self.grid.at(*coord) for coord in [
                (x-1, y-1), (x, y-1), (x+1, y-1),
                (x-1, y), (x+1, y),
                (x-1, y+1), (x, y+1), (x+1, y+1),
                ]
            if coord in self.grid.grid]

    def __str__(self):
        return f"{self.x}, {self.y}: {self.data}"

    def __repr__(self):
        return self.__str__()

class Grid:
    def __init__(self, rows):
        self.grid = {}
        for y, row in enumerate(rows):
            for x, data in enumerate(row):
                self.grid[(x, y)] = Cell(x, y, data, self)

    def at(self, x, y, direction=(0, 0)):
        return self.grid.get((x + direction[0], y + direction[1]))

def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="4.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
            "MMMSXXMASM",
            "MSAMXMSMSA",
            "AMXSXMAAMM",
            "MSAMASMSMX",
            "XMASAMXAMM",
            "XXAMMXXAMA",
            "SMSMSASXSS",
            "SAXAMASAAA",
            "MAMMMXMMMM",
            "MXMXAXMASX",
            ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 18

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 9

    def test_grid(self):
        grid = Grid([
            "12345",
            "67890",
            "abcde",
            "fghij",
            "klmno",
            ])

        cell = grid.at(1, 2)

        assert cell.x == 1
        assert cell.y == 2
        assert cell.data == "b"
        assert set([c.data for c in cell.surrounding()]) == {"6", "7", "8", "a", "c", "f", "g", "h"}
    
        cell = grid.at(4, 0)

        assert cell.x == 4
        assert cell.y == 0
        assert cell.data == "5"
        assert set([c.data for c in cell.surrounding()]) == {"4", "9", "0"}

        cell = grid.at(3, 3, ul)

        assert cell.x == 2
        assert cell.y == 2
        assert cell.data == "c"

        cell = grid.at(3, 3, right)

        assert cell.x == 4
        assert cell.y == 3
        assert cell.data == "j"

    def test_xmas_in_dir(self):
        grid = Grid([
            "SQQSQQS",
            "QAQAQAQ",
            "QQMMMQQ",
            "SAMXMAS",
            "QQMMMQQ",
            "QAQAQAQ",
            "SQQSQQS",
            ])

        assert xmas_in_dir(grid.at(3, 3), (1, 0)) 
        assert xmas_in_dir(grid.at(3, 3), (-1, 0))
        assert xmas_in_dir(grid.at(3, 3), (0, 1))
        assert xmas_in_dir(grid.at(3, 3), (0, -1))
        assert xmas_in_dir(grid.at(3, 3), (1, 1))
        assert xmas_in_dir(grid.at(3, 3), (-1, 1))
        assert xmas_in_dir(grid.at(3, 3), (1, -1))
        assert xmas_in_dir(grid.at(3, 3), (-1, -1))

    def test_xmas_not_in_dir(self):
        gridx = Grid(["X"])
        gridq = Grid(["Q"])

        assert not xmas_in_dir(gridx.at(0, 0), (1, 0))
        assert not xmas_in_dir(gridx.at(0, 0), (-1, 0))
        assert not xmas_in_dir(gridx.at(0, 0), (0, 1))
        assert not xmas_in_dir(gridx.at(0, 0), (0, -1))
        assert not xmas_in_dir(gridx.at(0, 0), (1, 1))
        assert not xmas_in_dir(gridx.at(0, 0), (-1, 1))
        assert not xmas_in_dir(gridx.at(0, 0), (1, -1))
        assert not xmas_in_dir(gridx.at(0, 0), (-1, -1))

        assert not xmas_in_dir(gridq.at(0, 0), (1, 0))
        assert not xmas_in_dir(gridq.at(0, 0), (-1, 0))
        assert not xmas_in_dir(gridq.at(0, 0), (0, 1))
        assert not xmas_in_dir(gridq.at(0, 0), (0, -1))
        assert not xmas_in_dir(gridq.at(0, 0), (1, 1))
        assert not xmas_in_dir(gridq.at(0, 0), (-1, 1))
        assert not xmas_in_dir(gridq.at(0, 0), (1, -1))
        assert not xmas_in_dir(gridq.at(0, 0), (-1, -1))

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
