#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys
import pytest


def answer_part_one(data):

    return len([1 for line in data if is_safe(line)])


def answer_part_two(data):
    safe_count = 0

    for line in data:
        for i in range(len(line)):
            if is_safe(line[0:i] + line[i+1:]):
                safe_count += 1
                break
    return safe_count


def is_safe(line):
    sorted_line = sorted(line)
    reverse_line = sorted(line, reverse=True)

    if sorted_line != line and reverse_line != line:
        return False

    pairs = zip(line[0:-1], line[1:])

    for first, second in pairs:
        if first == second:
            return False
        if abs(first - second) > 3:
            return False

    return True


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="2.txt"):
    with open(filename) as fh:
        return [[int(v.strip()) for v in line.strip().split()] for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    data = [
            [7, 6, 4, 2, 1],
            [1, 2, 7, 8, 9],
            [9, 7, 6, 2, 1],
            [1, 3, 2, 4, 5],
            [8, 6, 4, 4, 1],
            [1, 3, 6, 7, 9],
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 2

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 4

    @pytest.mark.parametrize("line", [
        [7, 6, 4, 2, 1],
        [1, 3, 6, 7, 9],
    ])
    def test_is_safe(self, line):
        assert is_safe(line)

    @pytest.mark.parametrize("line", [
        [1, 2, 7, 8, 9],
        [9, 7, 6, 2, 1],
        [1, 3, 2, 4, 5],
        [8, 6, 4, 4, 1],
    ])
    def test_not_is_safe(self, line):
        assert not is_safe(line)


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
