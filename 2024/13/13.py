#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import platform
import re
import sys

class Machine:
    def __init__(self):
        self.ax = None
        self.ay = None
        self.bx = None
        self.by = None
        self.prizex = None
        self.prizey = None


def parse(data):
    machine = None
    machines = []
    ax, ay, bx, by, prx, pry = None, None, None, None, None, None

    for line in data:
        if not line:
            machine = None
            continue
        if not machine:
            machine = Machine()
            machines.append(machine)

        am = re.match(r"Button A: X\+(\d+), Y\+(\d+)", line)
        bm = re.match(r"Button B: X\+(\d+), Y\+(\d+)", line)
        pm = re.match(r"Prize: X=(\d+), Y=(\d+)", line)

        if am:
            machine.ax = int(am[1])
            machine.ay = int(am[2])
        if bm:
            machine.bx = int(bm[1])
            machine.by = int(bm[2])
        if pm:
            machine.prizex = int(pm[1])
            machine.prizey = int(pm[2])

    return machines

def answer_part_one(data):

    return ""


def answer_part_two(data):

    return ""


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="13.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    if platform.system() == "Linux":
        with Popen(["xclip", "-selection", "clipboard"],
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()
    if platform.system() == "Darwin":
        with Popen("pbcopy",
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()


data = [
        "Button A: X+94, Y+34",
        "Button B: X+22, Y+67",
        "Prize: X=8400, Y=5400",
        "",
        "Button A: X+26, Y+66",
        "Button B: X+67, Y+21",
        "Prize: X=12748, Y=12176",
        "",
        "Button A: X+17, Y+86",
        "Button B: X+84, Y+37",
        "Prize: X=7870, Y=6450",
        "",
        "Button A: X+69, Y+23",
        "Button B: X+27, Y+71",
        "Prize: X=18641, Y=10279",
        ]

def test_parse():
    machines = parse(data)

    assert len(machines) == 4

    assert machines[0].ax == 94
    assert machines[0].ay == 34
    assert machines[0].bx == 22
    assert machines[0].by == 67
    assert machines[0].prizex == 8400
    assert machines[0].prizey == 5400

    assert machines[-1].ax == 69
    assert machines[-1].ay == 23
    assert machines[-1].bx == 27
    assert machines[-1].by == 71
    assert machines[-1].prizex == 18641
    assert machines[-1].prizey == 10279

def test_answer_part_one():
    answer = answer_part_one(data)
    assert answer == ""

def test_answer_part_two():
    answer = answer_part_two(data)
    assert answer == ""


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
