#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import pytest
import platform
import sys

class Cell:
    def __init__(self, x, y, data, grid):
        self.x = x
        self.y = y
        self.data = data
        self.grid = grid

    def __str__(self):
        return f"{self.x}, {self.y}: {self.data}"

    def __repr__(self):
        return self.__str__()

class Grid:
    up = (0, -1)
    down = (0, 1)
    left = (-1, 0)
    right = (1, 0)
    directions = {
            "^": {"turn_right": ">", "dir": up},
            ">": {"turn_right": "v", "dir": right},
            "v": {"turn_right": "<", "dir": down},
            "<": {"turn_right": "^", "dir": left},
            }

    def __init__(self, rows):
        self.grid = {}
        self.start = None

        self.max_x = 0
        self.max_y = 0

        for y, row in enumerate(rows):
            self.max_y = y
            for x, data in enumerate(row):
                self.max_x = x
                cell = Cell(x, y, data, self)
                if data in self.directions:
                    self.start = cell
                self.grid[(x, y)] = cell

    def cells(self):
        return self.grid.values()

    def __str__(self):
        out = ""

        for y in range(self.max_y + 1):
            for x in range(self.max_x + 1):
                out += self.at(x, y).data
            out += "\n"

        return out

    def at(self, x, y, direction=(0, 0)):
        return self.grid.get((x + direction[0], y + direction[1]))

    def walk(self):
        guard = self.start
        current_dir = guard.data

        yield guard
        moves = set()

        while guard:
            next_cell = self.at(guard.x, guard.y, self.directions[current_dir]["dir"])

            turns = 0

            while next_cell and next_cell.data == "#" and turns < 4:
                turns += 1
                current_dir = self.directions[current_dir]["turn_right"]
                next_cell = self.at(guard.x, guard.y, self.directions[current_dir]["dir"])

            if turns == 4:
                break

            if next_cell:
                move_from = guard
                move_to = next_cell
                move = ((move_from.x, move_from.y), (move_to.x, move_to.y))

                if move in moves:
                    raise Exception("Stuck in a loop")

                moves.add(move)

            guard = next_cell

            if guard:
                yield guard


def answer_part_one(data):
    grid = Grid(data)
    steps = {(c.x, c.y) for c in grid.walk()}
    
    return len(steps)


def answer_part_two(data):
    grid = Grid(data)

    count = 0

    for cell in grid.cells():
        if cell.data == ".":
            cell.data = "#"
            try:
                list(grid.walk())
            except:
                count += 1
                print(f"Loop {count} found at {cell}!")

            cell.data = "."

    return count


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="6.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    if platform.system() == "Linux":
        with Popen(["xclip", "-selection", "clipboard"],
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()
    if platform.system() == "Darwin":
        with Popen("pbcopy",
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()


data = [
        "....#.....",
        ".........#",
        "..........",
        "..#.......",
        ".......#..",
        "..........",
        ".#..^.....",
        "........#.",
        "#.........",
        "......#...",
]
# (6, 7) is a problem...

def test_answer_part_one():
    answer = answer_part_one(data)
    assert answer == 41

def test_answer_part_two():
    answer = answer_part_two(data)
    assert answer == 6

def test_walk():
    spiral = [
        "..#..",
        "....#",
        "..^..",
        "#....",
        "...#.",
        ]
    grid = Grid(spiral)

    steps = list(grid.walk())

    act = [(s.x, s.y) for s in steps] 
    exp = [
        (2, 2),
        (2, 1),
        (3, 1),
        (3, 2),
        (3, 3),
        (2, 3),
        (1, 3),
        (1, 2),
        (1, 1),
        (1, 0),
        ]

    assert act == exp

def test_exception_if_in_loop():
    loop = [
        ".#..",
        "...#",
        "#^..",
        "..#.",
        ]
    grid = Grid(loop)

    with pytest.raises(Exception) as ex:
        list(grid.walk())

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
