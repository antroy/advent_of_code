#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import platform
import sys
from itertools import combinations


class Cell:
    def __init__(self, x, y, data, grid):
        self.x = x
        self.y = y
        self.data = data
        self.grid = grid

    def __str__(self):
        return f"{self.x}, {self.y}: {self.data}"

    def __repr__(self):
        return self.__str__()

class Grid:
    def __init__(self, rows):
        self.grid = {}
        self.types = set()

        self.max_x = 0
        self.max_y = 0

        for y, row in enumerate(rows):
            self.max_y = y
            for x, data in enumerate(row):
                if data != ".":
                    self.types.add(data)
                self.max_x = x
                cell = Cell(x, y, data, self)
                self.grid[(x, y)] = cell

    def cells(self):
        return self.grid.values()

    def __str__(self):
        out = ""

        for y in range(self.max_y + 1):
            for x in range(self.max_x + 1):
                out += self.at(x, y).data
            out += "\n"

        return out

    def at(self, x, y, direction=(0, 0)):
        return self.grid.get((x + direction[0], y + direction[1]))

    def antinodes_for(self, c1, c2):
        dx = c1.x - c2.x
        dy = c1.y - c2.y

        a = self.at(c1.x - dx, c1.y - dy)
        b = self.at(c1.x + dx, c1.y + dy)
        c = self.at(c2.x - dx, c2.y - dy)
        d = self.at(c2.x + dx, c2.y + dy)

        out = {x for x in [a, b, c, d] if x} - {c1, c2}

        return out

    def antinodes_for_b(self, c1, c2):
        dx = c1.x - c2.x
        dy = c1.y - c2.y

        out = set()
        current = c1

        while current:
            out.add(current)
            current = self.at(current.x, current.y, (0 - dx, 0 - dy))

        current = c1

        while current:
            out.add(current)
            current = self.at(current.x, current.y, (dx, dy))

        out.update({c1, c2})

        return out

    def antinodes(self, version=1):
        out = set()
        fn = self.antinodes_for if version == 1 else self.antinodes_for_b
        for type in self.types:
            nodes = [cell for cell in self.cells() if cell.data == type]
            pairs = combinations(nodes, 2)

            for pair in pairs:
                out.update(fn(*pair))
            
        return out


def answer_part_one(data):
    grid = Grid(data)

    return len(grid.antinodes())


def answer_part_two(data):
    grid = Grid(data)

    return len(grid.antinodes(2))


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="8.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    if platform.system() == "Linux":
        with Popen(["xclip", "-selection", "clipboard"],
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()
    if platform.system() == "Darwin":
        with Popen("pbcopy",
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()


data = [
        "............",
        "........0...",
        ".....0......",
        ".......0....",
        "....0.......",
        "......A.....",
        "............",
        "............",
        "........A...",
        ".........A..",
        "............",
        "............",
]

data_a = [
        "..........",
        "..........",
        "..........",
        "....a.....",
        "..........",
        ".....a....",
        "..........",
        "..........",
        "..........",
        "..........",
        ]

data_b = [
    "..........",
    "..........",
    "..........",
    "....a.....",
    "........a.",
    ".....a....",
    "..........",
    "..........",
    "..........",
    "..........",
    ]

def test_answer_part_one():
    answer = answer_part_one(data)
    assert answer == 14

def test_answer_part_two():
    answer = answer_part_two(data)
    assert answer == 34

def test_antinodes_for():

    grid = Grid(data_a)
    exp = {grid.at(3,1),grid.at(6,7)}

    act = grid.antinodes_for(grid.at(4, 3), grid.at(5, 5))

    assert act == exp

def test_antinodes():
    grid = Grid(data_a)
    exp = {grid.at(3,1),grid.at(6,7)}

    act = grid.antinodes()

    assert act == exp

def test_antinodes_b():
    grid = Grid(data_b)
    exp = {
            grid.at(3,1), 
            grid.at(0,2), 
            grid.at(2,6), 
            grid.at(6,7)
            }

    act = grid.antinodes()

    assert act == exp

data_z = [
        "T.........",
        "...T......",
        ".T........",
        "..........",
        "..........",
        "..........",
        "..........",
        "..........",
        "..........",
        "..........",
        ]

#  def test_antinodes_for_b():
    #  grid = Grid(data_z)
#
    #  act1 = grid.antinodes_for_b(grid.at(0, 0), grid.at(3, 1))
    #  act2 = grid.antinodes_for_b(grid.at(1, 2), grid.at(3, 1))
    #  act3 = grid.antinodes_for_b(grid.at(0, 0), grid.at(1, 2))
#
    #  assert act == exp

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
