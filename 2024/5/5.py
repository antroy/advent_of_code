#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import functools
from subprocess import PIPE, Popen
import sys

def sort_page(page, rules):
    def sort(x, y):
        if f"{x}|{y}" in rules:
            return -1
        if f"{y}|{x}" in rules:
            return 1
        return 0

    return sorted(page, key=functools.cmp_to_key(sort))


def is_valid(page, rules):
    return page == sort_page(page, rules)


def invalid(page, rules):
    return page != sort_page(page, rules)


def answer_part_one(data):
    rules, pages = data

    valid_pages = [p for p in pages if is_valid(p, rules)]

    return sum([int(p[len(p) // 2]) for p in valid_pages])


def answer_part_two(data):
    rules, pages = data

    invalid_pages = [p for p in pages if invalid(p, rules)]
    sorted_pages = [sort_page(p, rules) for p in invalid_pages]

    return sum([int(p[len(p) // 2]) for p in sorted_pages])


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def parse_data(data):
    lines_section = False
    rules, pages = [], []

    for line in data.split("\n"):
        line = line.strip()
        if not line:
            lines_section = True
            continue
        if lines_section:
            pages.append(line.split(","))
        else:
            rules.append(line)

    return rules, pages
    


def get_data(filename="5.txt"):
    with open(filename) as fh:
        return parse_data(fh.read())


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()

data = """47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47
"""

def test_parse_data():
    rules, pages = parse_data(data)

    assert rules[0] == "47|53"
    assert rules[7] == "29|13"
    assert rules[20] == "53|13"

    assert pages[0] == ["75", "47", "61", "53", "29"]
    assert pages[5] == ["97", "13", "75", "29", "47"]

def test_answer_part_one():
    answer = answer_part_one(parse_data(data))
    assert answer == 143

def test_answer_part_two():
    answer = answer_part_two(parse_data(data))
    assert answer == 123

def test_line_valid():
    rules, pages = parse_data(data)

    assert is_valid(pages[0], rules)
    assert is_valid(pages[1], rules)
    assert is_valid(pages[2], rules)
    assert not is_valid(pages[3], rules)
    assert not is_valid(pages[4], rules)
    assert not is_valid(pages[5], rules)
    

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
