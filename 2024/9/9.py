#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import platform
import pytest
import sys


def expand(data):
    fs = [int(d) for d in data]
    out = []
    id = 0
    space = False

    for f in fs:
        if space:
            out.extend(["."] * f)
            space = False
            continue
        out.extend([id] * f)
        id += 1
        space = True

    return out


def compact(data, dots=None):
    if dots is None:
        dots = len([x for x in data if x == "."])

    while True:
        if len([d for d in data[0 - dots:] if d == "."]) == dots:
            return data

        first_dot = None
        digit = None
        pos = None

        for i, d in enumerate(data):
            if d == ".":
                if first_dot is None:
                    first_dot = i
            else:
                digit = d
                pos = i

        data[first_dot] = digit
        data[pos] = "."

    return data


def partition(data):
    out = []
    current = None
    currentIndex = None
    count = 0

    for i, d in enumerate(data):
        if d == current:
            count += 1
        else:
            if current is not None:
                out.append((current, currentIndex, count))
            current = d
            currentIndex = i
            count = 1
    out.append((current, currentIndex, count))

    return out

def compact2(data, dots=None):
    files = partition(data)
    reversed_ids = reversed([f for f in files if f[0] != "."])

    for r_id, r_start, r_length in reversed_ids:
        for i, (l_id, l_start, l_length) in enumerate(files):
            if l_id != ".":
                continue
            if l_start > r_start:
                continue
            if l_length >= r_length:
                init_seg = data[0:l_start]
                replacement = data[r_start: r_start + r_length]
                rem_dots = ["."] * (l_length - r_length)
                middle = data[l_start+l_length: r_start]
                gap = ["."] * r_length 
                rest = data[r_start + r_length:]

                new_data = []
                new_data.extend(init_seg)
                new_data.extend(replacement)
                new_data.extend(rem_dots)
                new_data.extend(middle)
                new_data.extend(gap)
                new_data.extend(rest)

                data = new_data
                files = partition(data)
                break

    return data


def checksum(data):
    return sum([0 if d == "." else i * d for i, d in enumerate(data)])

def answer_part_one(data):

    return checksum(compact(expand(data)))


def answer_part_two(data):

    return checksum(compact2(expand(data)))


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="9.txt"):
    with open(filename) as fh:
        return "".join([line.strip() for line in fh])


def clip(data):
    print("Storing Code")
    if platform.system() == "Linux":
        with Popen(["xclip", "-selection", "clipboard"],
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()
    if platform.system() == "Darwin":
        with Popen("pbcopy",
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()


data1 = "12345"
data2 = "2333133121414131402"


@pytest.mark.parametrize(
        "data, exp", [
            (data1, [0,".",".",1,1,1,".",".",".",".",2,2,2,2,2]),
            (data2, [0,0,".",".",".",1,1,1,".",".",".",2,".",".",".",3,3,3,".",4,4,".",5,5,5,5,".",6,6,6,6,".",7,7,7,".",8,8,8,8,9,9]),
        ])
def test_expand(data, exp):
    act = expand(data)

    assert act == exp

@pytest.mark.parametrize(
        "data, exp", [
            (data1, "022111222......"),
            (data2, "0099811188827773336446555566.............."),
        ])
def test_compact(data, exp):
    act = "".join([str(x) for x in compact(expand(data))])

    assert act == exp

@pytest.mark.parametrize(
        "data, exp", [
            (data1, "0..111....22222"),
            (data2, "00992111777.44.333....5555.6666.....8888.."),
        ])
def test_compact2(data, exp):
    act = "".join([str(x) for x in compact2(expand(data))])

    assert act == exp

def test_answer_part_one():
    answer = answer_part_one(data2)
    assert answer == 1928

def test_answer_part_two():
    answer = answer_part_two(data2)
    assert answer == 2858


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
