#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import platform
import sys

up = (0, -1)
down = (0, 1)
left = (-1, 0)
right = (1, 0)


class Cell:
    def __init__(self, x, y, data, grid):
        self.x = x
        self.y = y
        self.data = int(data) if not data == "." else None
        self.grid = grid
        self.trails = None

    def exits(self):
        x = self.x
        y = self.y
        out = [self.grid.at(x, y, d) for d in [up, down, left, right]]
        out = [c for c in out if c and c.data is not None and c.data == self.data + 1]

        return out

    def paths(self):
        if self.trails is not None:
            return self.trails

        exits = self.exits()
        if not exits:
            self.trails = [[self]]
            return self.trails

        out = []

        for e in exits:
            for p in e.paths():
                paths = [self]
                paths.extend(p)
                out.append(paths)

        self.trails = out
        return self.trails

    def score(self):
        summmits_reachable = {t[-1] for t in self.trails if t[-1].data == 9}
        return len(list(summmits_reachable))

    def rating(self):
        return len([t for t in self.trails if len(t) == 10])

    def __str__(self):
        return f"({self.x}, {self.y}: {self.data})"

    def __repr__(self):
        return self.__str__()

class Grid:
    def __init__(self, rows):
        self.grid = {}
        self.trailheads = []

        for y, row in enumerate(rows):
            for x, data in enumerate(row):
                cell = Cell(x, y, data, self)
                self.grid[(x, y)] = cell
                if cell.data == 0:
                    self.trailheads.append(cell)

        self._paths()

    def at(self, x, y, direction=(0, 0)):
        return self.grid.get((x + direction[0], y + direction[1]))

    def _paths(self):
        out = {}

        for cell in self.trailheads:
            trails = [p for p in cell.paths()]
            trails = [p for p in trails if len(p) == 10]
            out[cell] = trails

        return out

    def score(self):
        return sum([c.score() for c in self.trailheads])

    def rating_score(self):
        ratings = [c.rating() for c in self.trailheads]
        return sum(ratings)


def answer_part_one(data):
    grid = Grid(data)

    return grid.score()


def answer_part_two(data):
    grid = Grid(data)

    return grid.rating_score()


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="10.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    if platform.system() == "Linux":
        with Popen(["xclip", "-selection", "clipboard"],
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()
    if platform.system() == "Darwin":
        with Popen("pbcopy",
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()

ex1 = [
    "0123",
    "1234",
    "8765",
    "9876",
]

ex2 = [
        "..90..9",
        "...1.98",
        "...2..7",
        "6543456",
        "765.987",
        "876....",
        "987....",
        ]

data = [
    "89010123",
    "78121874",
    "87430965",
    "96549874",
    "45678903",
    "32019012",
    "01329801",
    "10456732",
]

def test_ex1():
    grid = Grid(ex1)

    assert grid.score() == 1

def test_ex2():
    grid = Grid(ex2)

    assert grid.score() == 4
    
def test_answer_part_one():
    answer = answer_part_one(data)
    assert answer == 36

def test_answer_part_two():
    answer = answer_part_two(data)
    assert answer == 81


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
