#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import re
import sys


def answer_part_one(data):
    xs, ys = data
    xs.sort()
    ys.sort()
    sorted_pairs = zip(xs, ys)

    return sum([abs(p[0] - p[1]) for p in sorted_pairs])


def answer_part_two(data):
    xs, ys = data
    counts = {x: 0 for x in xs}
    for y in ys:
        if y in counts:
            counts[y] += 1

    return sum(x * counts[x] for x in xs)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="1.txt"):
    with open(filename) as fh:
        pairs = [re.split(r"\s+", line.strip()) for line in fh]
        return [int(p[0]) for p in pairs], [int(p[1]) for p in pairs]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    xs = [3, 4, 2, 1, 3, 3]
    ys = [4, 3, 5, 3, 9, 3]

    def test_answer_part_one(self):
        answer = answer_part_one((self.xs, self.ys))
        assert answer == 11

    def test_answer_part_two(self):
        answer = answer_part_two((self.xs, self.ys))
        assert answer == 31


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
