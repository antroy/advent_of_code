#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import platform
import sys
import pytest

up = (0, -1)
down = (0, 1)
left = (-1, 0)
right = (1, 0)

dirs = {
        "u": up,
        "d": down,
        "r": right,
        "l": left
 }

class Cell:
    def __init__(self, x, y, data, grid):
        self.x = x
        self.y = y
        self.data = data
        self.grid = grid

    def surrounding(self):
        x = self.x
        y = self.y

        out = [self.grid.at(x, y, coord) for coord in [up, down, left, right]]
        return [cell for cell in out if cell]

    def adjacent_to(self, other):
        if self.data != other.data:
            return False
        return other in self.surrounding()

    def __str__(self):
        return f"{self.x}, {self.y}: {self.data}"

    def __repr__(self):
        return self.__str__()

class Grid:
    def __init__(self, rows):
        self.grid = {}
        for y, row in enumerate(rows):
            for x, data in enumerate(row):
                self.grid[(x, y)] = Cell(x, y, data, self)

    def at(self, x, y, direction=(0, 0)):
        return self.grid.get((x + direction[0], y + direction[1]))

    def region_at(self, cell, visited):
        visited.add(cell)
        adjacent = [c for c in cell.surrounding() if cell.data == c.data]
        out = {cell}
        for adj in adjacent:
            if not adj in visited:
                out.update(self.region_at(adj, visited))

        return out

    def regions(self):
        visited = set()
        regions = []

        for cell in self.grid.values():
            if cell in visited:
                continue
            regions.append(self.region_at(cell, visited))

        return regions

    def fences(self, region, orientation):
        fence_list = []

        for cell in region:
            boundary = self.at(cell.x, cell.y, dirs[orientation])
            if boundary not in region:
                fence_list.append(((cell.x, cell.y), orientation))

        # Need to sort differently deplending on direction.
        # Or something
        return sorted(fence_list)

def edges(fences):
    edges = 0
    current = None

    ## Or here. Keep track of the elements we care about.
    # So for a fence, check for all the cells in line. If it is in the region
    # add to the ignore list
    for fence in fences:
        inline = []
        while next_fence == None:
            # etc

        if not current:
            edges += 1
        else:
            cx, cy = current[0]
            fx, fy = fence[0]
            orientation = fence[1]

            if orientation in ["u", "d"]:
                if cy != fy or abs(fx - cx) != 1:
                    edges += 1
            if orientation in ["r", "l"]:
                if cx != fx or abs(fy - cy) != 1:
                    edges += 1
        current = fence

    return edges


def perimeter(region):
    count = 0
    for cell in region:
        internal = 0
        for sur in cell.surrounding():
            if cell.data == sur.data:
                internal += 1

        count += (4 - internal)
    return count

def area(region):
    return len(region)

def answer_part_one(data):
    grid = Grid(data)
    regions = grid.regions()
    prices = [area(r) * perimeter(r) for r in regions]

    return sum(prices)


def answer_part_two(data):
    grid = Grid(data)
    regions = grid.regions()
    
    price = 0

    for region in regions:
        r_count = 0
        for di in dirs:
            fs = grid.fences(region, di)
            ed = edges(fs)
            r_count += ed
            breakpoint()
        r_price = r_count * area(region)
        price += r_price

    return price


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="12.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    if platform.system() == "Linux":
        with Popen(["xclip", "-selection", "clipboard"],
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()
    if platform.system() == "Darwin":
        with Popen("pbcopy",
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()


ex1 = [
        "AAAA",
        "BBCD",
        "BBCC",
        "EEEC",
]

ex2 = [
        "OOOOO",
        "OXOXO",
        "OOOOO",
        "OXOXO",
        "OOOOO",
]

data = [
        "RRRRIICCFF",
        "RRRRIICCCF",
        "VVRRRCCFFF",
        "VVRCCCJFFF",
        "VVVVCJJCFE",
        "VVIVCCJJEE",
        "VVIIICJJEE",
        "MIIIIIJJEE",
        "MIIISIJEEE",
        "MMMISSJEEE",
        ]

def test_regions_1():
    grid = Grid(ex1)
    rs = grid.regions()

    assert len(rs) == 5
    assert {grid.at(0,0), grid.at(1,0), grid.at(2,0), grid.at(3,0)} in rs

def test_regions_2():
    grid = Grid(ex2)
    rs = grid.regions()
    assert len(rs) == 5

def test_area():
    grid = Grid(ex1)
    rs = grid.regions()

    for r in rs:
        if list(r)[0].data == "A":
            assert area(r) == 4
        if list(r)[0].data == "B":
            assert area(r) == 4
        if list(r)[0].data == "C":
            assert area(r) == 4
        if list(r)[0].data == "D":
            assert area(r) == 1
        if list(r)[0].data == "E":
            assert area(r) == 3

def test_perimeter():
    grid = Grid(ex1)
    rs = grid.regions()

    for r in rs:
        if list(r)[0].data == "A":
            assert perimeter(r) == 10
        if list(r)[0].data == "B":
            assert perimeter(r) == 8
        if list(r)[0].data == "C":
            assert perimeter(r) == 10
        if list(r)[0].data == "D":
            assert perimeter(r) == 4
        if list(r)[0].data == "E":
            assert perimeter(r) == 8

def test_fences():
    grid = Grid(ex1)

    [
    "AAAA",
    "BBCD",
    "BBCC",
    "EEEC",]

    for region in grid.regions():
        if grid.at(0, 0) in region:
            assert grid.fences(region, "u") == [((0, 0), "u"), ((1, 0), "u"), ((2, 0), "u"), ((3, 0), "u"),]
            assert grid.fences(region, "d") == [((0, 0), "d"), ((1, 0), "d"), ((2, 0), "d"), ((3, 0), "d"),]
            assert grid.fences(region, "l") == [((0, 0), "l")]
            assert grid.fences(region, "r") == [((3, 0), "r")]
        if grid.at(2, 1) in region:
            assert grid.fences(region, "u") == [((2, 1), "u"), ((3, 2), "u")]
            assert grid.fences(region, "d") == [((2, 2), "d"), ((3, 3), "d")]
            assert grid.fences(region, "l") == [((2, 1), "l"), ((2, 2), "l"), ((3, 3), "l")]
            assert grid.fences(region, "r") == [((2, 1), "r"), ((3, 2), "r"), ((3, 3), "r")]


@pytest.mark.parametrize(
        "fences, exp", [
            ( [((0, 0), "u"), ((1, 0), "u"), ((2, 0), "u"), ((3, 0), "u")], 1),
            ( [((2, 1), "r"), ((3, 2), "r"), ((3, 3), "r")], 2)
            ]
        )
def test_edges(fences, exp):
    act = edges(fences)

    assert act == exp

def test_answer_part_two_ex1():
    answer = answer_part_two(ex1)
    assert answer == 80

def test_answer_part_two_ex2():
    answer = answer_part_two(ex2)
    assert answer == 436

def test_answer_part_one():
    answer = answer_part_one(data)
    assert answer == 1930

def test_answer_part_two():
    answer = answer_part_two(data)
    assert answer == 1206


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
