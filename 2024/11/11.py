#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import platform
import sys
from collections import Counter

def parse(data):
    return [int(d.strip()) for d in data[0].split(" ")]
 
def parse2(data):
    return Counter(parse(data))
 
def expand(stones, count):
    if count == 0:
        return stones
    out = []

    for stone in stones:
        if stone == 0:
            out.append(1)
        elif len(str(stone)) % 2 == 0:
            st_st = str(stone)
            left = int(st_st[0:len(st_st) // 2])
            right = int(st_st[len(st_st) // 2:])
            out.extend([left, right])
        else:
            out.append(stone * 2024)

    return expand(out, count - 1)

def expand2(stones, iterations):
    if iterations == 0:
        return stones
    out = Counter()

    for stone, count in stones.items():
        if stone == 0:
            out[1] += count
        elif len(str(stone)) % 2 == 0:
            st_st = str(stone)
            left = int(st_st[0:len(st_st) // 2])
            right = int(st_st[len(st_st) // 2:])
            
            out[left] += count
            out[right] += count
        else:
            out[stone * 2024] += count

    return expand2(out, iterations - 1)


def answer_part_one(data):

    return sum(expand2(parse2(data), 25).values())


def answer_part_two(data):
    stones = expand2(parse2(data), 75)

    return sum(stones.values())


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="11.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    if platform.system() == "Linux":
        with Popen(["xclip", "-selection", "clipboard"],
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()
    if platform.system() == "Darwin":
        with Popen("pbcopy",
                   universal_newlines=True, stdin=PIPE) as fh:
            fh.stdin.write(str(data))
            fh.communicate()


data = ["125 17"]

def test_answer_part_one():
    answer = answer_part_one(data)
    assert answer == 55312

def test_expand():
    act = expand(parse(data), 2)
    exp = [253, 0, 2024, 14168]

    assert act == exp

def test_expand_2():
    act = expand2(parse2(data), 2)
    exp = Counter([253, 0, 2024, 14168])

    assert act == exp

def test_expand_3():
    act = expand2(parse2(data), 3)
    exp = Counter([512072, 1, 20, 24, 28676032])

    assert act == exp

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    else:
        expand([2024], 14)
        
