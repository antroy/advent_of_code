#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import unittest
import sys

ROCK = 1
PAPER = 2
SCISSORS = 3

WIN = 6
DRAW = 3
LOSS = 0

CHOICES = {
    "A": ROCK, "B": PAPER, "C": SCISSORS,
    "X": ROCK, "Y": PAPER, "Z": SCISSORS
}

CHOICES_2 = {
    "A": ROCK, "B": PAPER, "C": SCISSORS,
    "X": LOSS, "Y": DRAW, "Z": WIN
}


def score(elf, me):
    circular_diff = (me - elf) % 3

    if circular_diff == 0:
        return me + DRAW
    if circular_diff == 1:
        return me + WIN

    return me


def strategy(data):
    return [line.split() for line in data]


def answer_part_one(data):
    scores = [score(CHOICES[turn[0]], CHOICES[turn[1]])
              for turn in strategy(data)]

    return sum(scores)


def play_for_result(elf, result):
    if result == WIN:
        return (elf % 3) + 1
    if result == DRAW:
        return elf
    return ((elf - 2) % 3) + 1


def answer_part_two(data):
    scores = []

    as_numbers = [(CHOICES_2[turn[0]], CHOICES_2[turn[1]])
                  for turn in strategy(data)]
    scores = [score(choice[0], play_for_result(choice[0], choice[1]))
              for choice in as_numbers]


    return sum(scores)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="two.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


class TestExampleData(unittest.TestCase):
    data = ["A Y", "B X", "C Z"]

    def test_score(self):
        self.assertEqual(score(ROCK, ROCK), 4)
        self.assertEqual(score(ROCK, PAPER), 8)
        self.assertEqual(score(ROCK, SCISSORS), 3)
        self.assertEqual(score(PAPER, ROCK), 1)
        self.assertEqual(score(PAPER, PAPER), 5)
        self.assertEqual(score(PAPER, SCISSORS), 9)
        self.assertEqual(score(SCISSORS, ROCK), 7)
        self.assertEqual(score(SCISSORS, PAPER), 2)
        self.assertEqual(score(SCISSORS, SCISSORS), 6)

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        self.assertEqual(answer, 15)

    def test_play_for_result(self):
        self.assertEqual(play_for_result(ROCK, WIN), PAPER)
        self.assertEqual(play_for_result(ROCK, DRAW), ROCK)
        self.assertEqual(play_for_result(ROCK, LOSS), SCISSORS)
        self.assertEqual(play_for_result(PAPER, WIN), SCISSORS)
        self.assertEqual(play_for_result(PAPER, DRAW), PAPER)
        self.assertEqual(play_for_result(PAPER, LOSS), ROCK)
        self.assertEqual(play_for_result(SCISSORS, WIN), ROCK)
        self.assertEqual(play_for_result(SCISSORS, DRAW), SCISSORS)
        self.assertEqual(play_for_result(SCISSORS, LOSS), PAPER)

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        self.assertEqual(answer, 12)


unittest.main()
