#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys

import pytest


class Node:
    def __init__(self, name, parent=None, size=None):
        self.name = name
        self._size = size
        self.parent = parent
        self.children = {} if size is None else None

    def add(self, name, size=None):
        node = Node(name, self, size)
        self[name] = node
        return node

    def is_dir(self):
        return self.children is not None

    def size(self):
        if not self.is_dir():
            return self._size
        return sum([c.size() for c in self.children.values()])

    def __getitem__(self, key):
        return self.children[key] if self.children is not None else None

    def __setitem__(self, key, value):
        if self.children is not None:
            self.children[key] = value

    def __eq__(self, other):
        if isinstance(other, Node):
            return self.name == other.name and self.parent == other.parent
        return False

    def __str__(self):
        return "%s (%s, %s)" % (
            self.name,
        self.parent.name if self.parent else "",
        [child for child in self.children] if self.children is not None else ".",
        )

    def visualize(self, curdir=None, depth=0):
        print(
            "%s- %s (%s%s) %s"
            % (
                "  " * depth,
                self.name,
                "file" if self.children is None else "dir",
                ", %s" % self.size(),
                "**" if self == curdir else "",
            )
        )
        if self.children is None:
            return

        for child in self.children.values():
            child.visualize(curdir, depth + 1)

    def _dirs_satisfying(self, filter):
        if self.is_dir():
            descendants = [
                item
                for n in self.children.values()
                for item in n._dirs_satisfying(filter)
            ]
            if filter(self):
                return [self] + descendants
            else:
                return descendants
        else:
            return []

    def get_dirs_smaller_than(self, max_size):
        return self._dirs_satisfying(lambda node: node.size() <= max_size)

    def get_dirs_bigger_than(self, min_size):
        return self._dirs_satisfying(lambda node: node.size() >= min_size)


class Tree:
    def __init__(self):
        self.root = Node("/")
        self.curdir = self.root

    def _up(self):
        self.curdir = self.curdir.parent
        return self.curdir

    def _root(self):
        self.curdir = self.root
        return self.curdir

    def cd(self, d):
        if d == "..":
            return self._up()

        if d == "/":
            return self._root()

        self.curdir = self.curdir[d]
        return self.curdir

    def add(self, file_or_dir):
        size, name = file_or_dir.split(" ")
        if size == "dir":
            self.curdir.add(name)
        else:
            self.curdir.add(name, int(size))


def build_tree(data):
    tree = Tree()

    for line in data:
        if line.startswith("$ cd"):
            tree.cd(line.split(" ")[2])
        elif line.startswith("$"):
            continue
        else:
            tree.add(line)

    tree.root.visualize()
    return tree.root


def to_free(root):
    free_space = 70000000 - root.size()
    update_space = 30000000
    return update_space - free_space


def answer_part_one(data):
    return sum(node.size() for node in build_tree(data).get_dirs_smaller_than(100000))


def answer_part_two(data):
    root = build_tree(data)
    space_reqd = to_free(root)
    dirs = root.get_dirs_bigger_than(space_reqd)
    ordered_asc = sorted(dirs, key=lambda d: d.size())

    return ordered_asc[0].size()


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="seven.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestNode:
    @pytest.fixture
    def root(self):
        root = Node("/")
        a = root.add("a")
        root.add("b.txt", 345)
        a.add("f", 678)
        return root

    def test_build_tree(self, root):
        assert root["a"].name == "a"
        assert root["b.txt"].name == "b.txt"
        assert root["a"]["f"].name == "f"

    def test_is_dir(self, root):
        assert root.is_dir()
        assert root["a"].is_dir()
        assert not root["b.txt"].is_dir()
        assert not root["a"]["f"].is_dir()

    def test_size(self, root):
        assert root["a"]["f"].size() == 678
        assert root["b.txt"].size() == 345
        assert root["a"].size() == 678
        assert root.size() == 1023


class TestTree:
    def test_add(self):
        tree = Tree()
        tree.add("dir one")
        tree.add("2345 thing")
        tree.add("dir two")
        tree.add("12 other")

        assert tree.root["one"].name == "one"
        assert tree.root["thing"].name == "thing"
        assert tree.root["thing"].size() == 2345
        assert tree.root["two"].name == "two"
        assert tree.root["other"].name == "other"
        assert tree.root["other"].size() == 12

    def test_cd(self):
        tree = Tree()
        tree.cd("/")
        tree.add("dir a")

        assert tree.root.name == "/"
        assert tree.curdir.name == "/"

        d = tree.cd("a")
        assert tree.curdir == d
        assert tree.curdir.name == "a"
        tree.add("dir b")
        d = tree.cd("b")
        assert tree.curdir == d
        assert tree.curdir.name == "b"
        d = tree.cd("..")
        assert tree.curdir == d
        assert tree.curdir.name == "a"
        tree.cd("/")
        assert tree.curdir == tree.root
        assert tree.curdir.name == "/"
        d = tree.cd("a")
        assert tree.curdir == d
        assert tree.curdir.name == "a"
        tree.root.visualize()


class TestExampleData:
    data = [
        "$ cd /",
        "$ ls",
        "dir a",
        "14848514 b.txt",
        "8504156 c.dat",
        "dir d",
        "$ cd a",
        "$ ls",
        "dir e",
        "29116 f",
        "2557 g",
        "62596 h.lst",
        "$ cd e",
        "$ ls",
        "584 i",
        "$ cd ..",
        "$ cd ..",
        "$ cd d",
        "$ ls",
        "4060174 j",
        "8033020 d.log",
        "5626152 d.ext",
        "7214296 k",
    ]

    def test_to_free(self):
        root = build_tree(self.data)
        assert to_free(root) == 8381165

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 95437

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 24933642


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
