#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys


def answer_part_one(data):
    calorie_counts = []

    current_elf = []
    first, rest = None, [int(food) if food else None for food in data]
    while rest:
        first, rest = rest[0], rest[1:]
        if not first:
            calorie_counts.append(current_elf)
            current_elf = []
        else:
            current_elf.append(first)

    calorie_counts.append(current_elf)
    calorie_totals = [sum(foods) for foods in calorie_counts]
    return max(calorie_totals)


def answer_part_two(data):
    calorie_counts = []

    current_elf = []
    first, rest = None, [int(food) if food else None for food in data]
    while rest:
        first, rest = rest[0], rest[1:]
        if not first:
            calorie_counts.append(current_elf)
            current_elf = []
        else:
            current_elf.append(first)

    calorie_counts.append(current_elf)
    calorie_totals = [sum(foods) for foods in calorie_counts]
    print(calorie_totals)
    top3 = sorted(calorie_totals, reverse=True)[0:3]
    print(top3)
    return sum(top3)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="one.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        data = [
            "1000", "2000", "3000", "",
            "4000", "",
            "5000", "6000", "",
            "7000", "8000", "9000", "",
            "10000"
        ]
        answer = answer_part_one(data)
        self.assertEqual(answer, 24000)


    def test_answer_part_two(self):
        data = [
            "1000", "2000", "3000", "",
            "4000", "",
            "5000", "6000", "",
            "7000", "8000", "9000", "",
            "10000"
        ]
        answer = answer_part_two(data)
        self.assertEqual(answer, 45000)

unittest.main()
