#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import math
from subprocess import PIPE, Popen
import sys

import pytest


def load_grid(data):
    inverted_grid = [[int(i) for i in list(line)] for line in data]
    out = [list(cols) for cols in zip(*inverted_grid)]
    return out


def row(y, grid):
    return [col[y] for col in grid]


def each_dir(tree_loc, trees):
    return list(reversed(trees[0:tree_loc])), trees[tree_loc + 1 :]


def in_each_direction_do(x, y, grid, compare_fn, agg_fn):
    tree = grid[x][y]
    tree_row = row(y, grid)
    tree_col = grid[x]
    left, right = each_dir(x, tree_row)
    up, down = each_dir(y, tree_col)

    results = [compare_fn(tree, x) for x in [left, right, up, down]]

    return agg_fn(results)


def visible_in_dir(tree, trees_in_dir):
    return not trees_in_dir or max(trees_in_dir) < tree


def visible(x, y, grid):
    return in_each_direction_do(x, y, grid, visible_in_dir, any)


def can_see(tree, trees_in_a_direction):
    trees_visible = 0
    for t in trees_in_a_direction:
        trees_visible += 1
        if t >= tree:
            break

    return trees_visible


def scenic_score(x, y, grid):
    return in_each_direction_do(x, y, grid, can_see, math.prod)


def answer_part_one(data):
    grid = load_grid(data)
    total_visible = 0

    for x in range(len(grid)):
        for y in range(len(grid[x])):
            if visible(x, y, grid):
                total_visible += 1

    return total_visible


def answer_part_two(data):
    grid = load_grid(data)
    max_scenic_score = 0

    for x in range(len(grid)):
        for y in range(len(grid[x])):
            max_scenic_score = max(max_scenic_score, scenic_score(x, y, grid))
    return max_scenic_score


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="eight.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData:
    @pytest.fixture
    def data(self):
        return ["30373", "25512", "65332", "33549", "35390"]

    @pytest.fixture
    def grid(self, data):
        return load_grid(data)

    @pytest.fixture
    def grid2(self):
        return load_grid(
            [
                "3037",
                "6552",
                "6533",
                "3614",
                "1111",
            ]
        )

    def test_load_grid(self, grid):
        assert grid[0][0] == 3
        assert grid[0][1] == 2
        assert grid[1][1] == 5
        assert grid[2][4] == 3
        assert grid[3][4] == 9
        assert grid[4][3] == 9
        assert grid[4][4] == 0

    def test_row(self, grid):
        assert row(0, grid) == [3, 0, 3, 7, 3]
        assert row(2, grid) == [6, 5, 3, 3, 2]

    def test_visible(self, grid):
        assert visible(0, 2, grid)
        assert visible(1, 1, grid)
        assert visible(2, 1, grid)
        assert not visible(2, 2, grid)
        assert not visible(3, 1, grid)

    def test_visible_u_d(self, grid2):
        assert visible(1, 1, grid2)
        assert visible(2, 2, grid2)

    def test_can_see(self):
        assert can_see(3, [1, 2, 3, 2]) == 3
        assert can_see(3, [1, 4, 3, 2]) == 2
        assert can_see(3, [6, 4, 3, 2]) == 1
        assert can_see(3, [1, 1, 1, 2]) == 4

    def test_scenic_score(self, grid):
        assert scenic_score(2, 1, grid) == 4
        assert scenic_score(2, 3, grid) == 8

    def test_answer_part_one(self, data):
        answer = answer_part_one(data)
        assert answer == 21

    def test_answer_part_two(self, data):
        answer = answer_part_two(data)
        assert answer == 8


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
