#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys

import pytest


class CPU:
    def __init__(self, x=1, cycle=0):
        self.x = x
        self.cycle = cycle
        self.debug = {"op_no": 0}
        self.x_during = 1

    def parse(self, instruction):
        def _addx():
            self.x += int(instruction.split()[1])

        def _noop():
            pass

        if instruction.startswith("noop"):
            return 1, _noop
        if instruction.startswith("addx"):
            return 2, _addx

    def run(self, instruction_stack, callback):
        while instruction_stack:
            delay, op = self.parse(instruction_stack.pop())
            self.debug["op_no"] += 1
            while delay > 0:
                self.cycle += 1
                self.x_during = self.x
                delay -= 1
                if delay == 0:
                    op()
                callback(self)


def answer_part_one(data):
    instruction_stack = list(reversed(data))
    cpu = CPU()
    signal_strengths = {}

    def callback(cpu):
        strength = cpu.cycle * cpu.x_during
        signal_strengths[cpu.cycle] = (cpu.x, strength, cpu.debug["op_no"])

    cpu.run(instruction_stack, callback)

    signals = [signal_strengths[c][1] for c in [20, 60, 100, 140, 180, 220]]
    return sum(signals)


def answer_part_two(data):
    instruction_stack = list(reversed(data))
    cpu = CPU()
    crt = []

    def callback(cpu):
        pix = [cpu.x_during - 1, cpu.x_during, cpu.x_during + 1]
        crt.append(pix)

    cpu.run(instruction_stack, callback)

    def draw(pix, posn):
        return "#" if (posn % 40) in pix else "."

    pixels = [draw(p, i) for i, p in enumerate(crt)]
    print("".join(pixels[0:40]))
    print("".join(pixels[40:80]))
    print("".join(pixels[80:120]))
    print("".join(pixels[120:160]))
    print("".join(pixels[160:200]))
    print("".join(pixels[200:240]))


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="ten.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestCPU:
    @pytest.mark.parametrize(
        "example",
        [
            (1, "noop", 1, 1),
            (1, "addx 11", 12, 2),
            (1, "addx -7", -6, 2),
            (-31, "addx 5", -26, 2),
            (16, "addx -7", 9, 2),
        ],
    )
    def test_parse(self, example):
        start_x, instruction, exp_x, exp_delay = example
        cpu = CPU(x=start_x)
        delay, op = cpu.parse(instruction)
        op()
        assert cpu.x == exp_x
        assert delay == exp_delay

    @pytest.mark.parametrize(
        "example",
        [
            (["addx -5", "addx 3", "noop"], [(1, 1), (2, 1), (3, 4), (4, 4), (5, -1)]),
            (
                ["addx 20", "noop", "addx -1", "addx 7"],
                [(1, 1), (2, 8), (3, 8), (4, 7), (5, 7), (6, 7), (7, 27)],
            ),
        ],
    )
    def test_run(self, example):
        instructions, exp = example
        actual = []

        def callback(cpu):
            actual.append((cpu.cycle, cpu.x))

        cpu = CPU()
        cpu.run(instructions, callback)
        assert actual == exp


class TestExampleData:
    def test_answer_part_one(self):
        answer = answer_part_one(get_data("ten_ex.txt"))
        assert answer == 13140

    def test_answer_part_two(self):
        answer_part_two(get_data("ten_ex.txt"))


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
