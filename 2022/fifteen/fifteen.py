#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import re
import sys
from datetime import datetime as dt
from subprocess import PIPE, Popen

import pytest

PATTERN = r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)"


def diff(a, b):
    d = a - b
    if d < 0:
        return -d
    return d


class Sensor:
    def __init__(self, line):
        m = re.match(PATTERN, line)
        self.x = int(m.group(1))
        self.y = int(m.group(2))
        self.beacon = (int(m.group(3)), int(m.group(4)))
        self.max_dist = self.dist(self.beacon)

    def dist(self, point):
        xs = diff(self.x, point[0])
        ys = diff(self.y, point[1])
        return xs + ys

    def can_see(self, point):
        dist = self.dist(point)
        return self.max_dist >= dist

    def range(self, y):
        dy = diff(self.y, y)
        dx = self.max_dist - dy
        return (self.x - dx, self.x + dx)


def answer_part_one(data, row=2000000):
    then = dt.now()
    sensors = [Sensor(line) for line in data]
    beacons = set([s.beacon for s in sensors])
    min_x = min([s.x - s.max_dist for s in sensors])
    max_x = max([s.x + s.max_dist for s in sensors])
    can_see = set()
    print("Min: %s; Max: %s" % (min_x, max_x))

    now = dt.now()
    print("Preamble:", str(now - then))
    then = now

    for x in range(min_x, max_x + 1):
        covered = False
        for s in sensors:
            if s.can_see((x, row)):
                covered = True
                break
        if covered:
            can_see.add((x, row))
        #now = dt.now()
        #print("Scan:", x,  str(now - then))
        #then = now

    now = dt.now()
    print("Scanning:", str(now - then))
    then = now
    not_beacons = can_see - beacons

    now = dt.now()
    print("Diff:", str(now - then))
    then = now
    return len(not_beacons)


def has_gaps(ranges):
    ranges.sort()

    print("ROW:", ranges)
    current_rightmost = ranges[0][1]
    for rl, rr in ranges[1:]:
        if rl <= current_rightmost + 1:
            current_rightmost = rr
        else:
            print("CR: ", current_rightmost)
            print("RRR", rl, rr)
            print("XXX", current_rightmost + 1)
            return current_rightmost + 1

    return False


def answer_part_two(data, max_x_y=4000000):
    then = dt.now()
    sensors = [Sensor(line) for line in data]
    beacons = set([s.beacon for s in sensors])
    min_x = min([s.x - s.max_dist for s in sensors] + [0])
    max_x = max([s.x + s.max_dist for s in sensors] + [max_x_y])
    min_y = min([s.y - s.max_dist for s in sensors] + [0])
    max_y = max([s.y + s.max_dist for s in sensors] + [max_x_y])
    print("Min: %s; Max: %s" % (min_x, max_x))

    ranges = []
    for y in range(min_y, max_y + 1):
        ranges.append([s.range(y) for s in sensors])

        for r in ranges:
            gap = has_gaps(r)
            if gap:
                print(gap, y)
                return gap * 4000000 + y

    return 12


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="fifteen.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


example_data = [
    "Sensor at x=2, y=18: closest beacon is at x=-2, y=15",
    "Sensor at x=9, y=16: closest beacon is at x=10, y=16",
    "Sensor at x=13, y=2: closest beacon is at x=15, y=3",
    "Sensor at x=12, y=14: closest beacon is at x=10, y=16",
    "Sensor at x=10, y=20: closest beacon is at x=10, y=16",
    "Sensor at x=14, y=17: closest beacon is at x=10, y=16",
    "Sensor at x=8, y=7: closest beacon is at x=2, y=10",
    "Sensor at x=2, y=0: closest beacon is at x=2, y=10",
    "Sensor at x=0, y=11: closest beacon is at x=2, y=10",
    "Sensor at x=20, y=14: closest beacon is at x=25, y=17",
    "Sensor at x=17, y=20: closest beacon is at x=21, y=22",
    "Sensor at x=16, y=7: closest beacon is at x=15, y=3",
    "Sensor at x=14, y=3: closest beacon is at x=15, y=3",
    "Sensor at x=20, y=1: closest beacon is at x=15, y=3",
]


class TestSensor:
    @pytest.mark.parametrize(
        "example",
        [
            (example_data[0], (2, 18), (-2, 15), 7),
            (example_data[5], (14, 17), (10, 16), 5),
            (example_data[9], (20, 14), (25, 17), 8),
        ],
    )
    def test_init(self, example):
        line, coords, beacon, dist = example
        sensor = Sensor(line)
        assert (sensor.x, sensor.y) == coords
        assert sensor.beacon == beacon
        assert sensor.max_dist == dist

    @pytest.mark.parametrize(
        "ex",
        [
            ((8, 7), True),
            ((2, 10), True),
            ((14, 3), False),
            ((11, 1), True),
        ],
    )
    def test_can_see(self, ex):
        point, expected = ex
        sensor = Sensor(example_data[6])
        assert sensor.can_see(point) == expected


class TestExampleData:
    def test_has_gap(self):
        r = [((19, -10), (19, -10)), ((20, -10), (20, -10)), ((26, -10), (26, -10)), ((26, -10), (26, -10)), ((24, -10), (24, -10)), ((29, -10), (29, -10)), ((27, -10), (27, -10)), ((22, -10), (22, -10)), ((13, -10), (13, -10
)), ((38, -10), (38, -10)), ((33, -10), (33, -10)), ((31, -10), (31, -10)), ((25, -10), (25, -10)), ((37, -10), (37, -10))]

        assert has_gaps(r)

    def test_answer_part_one(self):
        answer = answer_part_one(example_data, row=10)
        assert answer == 26

    def test_answer_part_two(self):
        answer = answer_part_two(example_data, 20)
        assert answer == 56000011


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
