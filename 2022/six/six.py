#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pytest
from subprocess import PIPE, Popen
import sys


def find_marker(length):
    def marker_fn(data):
        for i in range(len(data)):
            chars = data[i:i+length]
            if len(set(chars)) == length:
                return i+length

        return None
    return marker_fn


def answer_part_one(data):
    return find_marker(4)(data)


def answer_part_two(data):
    return find_marker(14)(data)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="six.txt"):
    with open(filename) as fh:
        return fh.read()


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData():
    @pytest.mark.parametrize('example', [
        ("bvwbjplbgvbhsrlpgdmjqwftvncz", 5),
        ("nppdvjthqldpwncqszvftbrmjlhg", 6),
        ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10),
        ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11)
    ])
    def test_answer_part_one(self, example):
        data, expected = example
        answer = answer_part_one(data)
        assert answer == expected

    @pytest.mark.parametrize('example', [
        ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19),
        ("bvwbjplbgvbhsrlpgdmjqwftvncz", 23),
        ("nppdvjthqldpwncqszvftbrmjlhg", 23),
        ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29),
        ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26)
    ])
    def test_answer_part_two(self, example):
        data, expected = example
        answer = answer_part_two(data)
        assert answer == expected


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
