#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import math
import re
from datetime import datetime as dt
from subprocess import PIPE, Popen

import pytest


class Monkey:
    def __init__(self):
        self.items = []
        self.op = None
        self.div_by = None
        self.true_throw = None
        self.false_throw = None
        self.inspected = 0
        self.relax = True

    def __str__(self):
        return "%s [%s]" % (self.items, self.inspected)

    def next_monkey(self, worry):
        return self.true_throw if worry % self.div_by == 0 else self.false_throw

    def turn(self, monkeys):
        while self.items:
            item = self.items.pop(0)
            self.inspected += 1
            then = dt.now()
            new_item = self.op(item)
            time = (dt.now() - then).seconds
            if time > 2:
                print(time)
                print(str(self))
            if self.relax:
                new_item = new_item // 3
            monkey = self.next_monkey(new_item)
            monkeys[monkey].items.append(new_item)


def get_op(op, by):
    def _op(old):
        if op == "+":
            return old + (old if by == "old" else int(by))
        if op == "*":
            return old * (old if by == "old" else int(by))

    return _op


def parse(data, relax=True):
    monkeys = []
    current = None
    for line in data:
        line = line.strip()
        m = re.match(r"Monkey (\d+):", line)
        if m:
            current = int(m.group(1))
            monkeys.append(Monkey())
            monkeys[current].relax = relax
        m = re.match(r"Starting items: (.+)", line)
        if m:
            monkeys[current].items.extend(
                [int(item) for item in m.group(1).split(", ")]
            )
        m = re.match(r"Operation: new = old (.) (.+)", line)
        if m:
            op = m.group(1)
            by = m.group(2)
            monkeys[current].op = get_op(op, by)
        m = re.match(r"Test: divisible by (\d+)", line)
        if m:
            monkeys[current].div_by = int(m.group(1))

        m = re.match(r"If (true|false): throw to monkey (\d+)", line)
        if m:
            if m.group(1) == "true":
                monkeys[current].true_throw = int(m.group(2))
            else:
                monkeys[current].false_throw = int(m.group(2))
    return monkeys


def round(monkeys):
    for monkey in monkeys:
        monkey.turn(monkeys)


def do_rounds(monkeys, number_of_rounds):
    then = dt.now()
    for i in range(number_of_rounds):
        print("Round %s" % (i + 1))
        round(monkeys)
        # for i, m in enumerate(monkeys):
        #     print("Monkey %s: %s" % (i, m))

    in_order = list(sorted(monkeys, key=lambda x: x.inspected))
    return math.prod(m.inspected for m in in_order[-2:])


def answer_part_one(data):
    monkeys = parse(data)
    return do_rounds(monkeys, 20)


def answer_part_two(data):
    monkeys = parse(data, False)
    return do_rounds(monkeys, 600)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="eleven.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData:
    @pytest.fixture
    def data(self):
        return """
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
""".split(
            "\n"
        )

    @pytest.fixture
    def monkeys(self, data):
        return parse(data)

    def test_parse(self, monkeys):
        assert monkeys[0].items == [79, 98]
        assert monkeys[0].op(2) == 38
        assert monkeys[0].next_monkey(23) == 2
        assert monkeys[0].next_monkey(13) == 3
        assert monkeys[0].true_throw == 2
        assert monkeys[0].false_throw == 3

        assert monkeys[2].op(3) == 9

    def test_Monkey_turn(self, monkeys):
        monkeys[0].turn(monkeys)
        assert monkeys[3].items[-2] == 500
        assert monkeys[3].items[-1] == 620

    def test_round(self, monkeys):
        round(monkeys)
        assert monkeys[0].items == [20, 23, 27, 26]
        assert monkeys[1].items == [2080, 25, 167, 207, 401, 1046]
        assert monkeys[2].items == []
        assert monkeys[3].items == []

    def test_answer_part_one(self, data):
        answer = answer_part_one(data)
        assert answer == 10605

    def test_answer_part_two(self, data):
        answer = answer_part_two(data)
        assert answer == 2713310158


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
