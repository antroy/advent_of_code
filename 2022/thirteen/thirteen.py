#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import json
import sys
from functools import cmp_to_key
from subprocess import PIPE, Popen

import pytest


def answer_part_one(data):
    pairs = parse(data)
    sum_of_correct = 0
    for i, pair in enumerate(pairs):
        status = in_correct_order(*pair)
        if status is None:
            print("None:", i + 1)
        elif status:
            print("OK:", i + 1)
            sum_of_correct += i + 1
        else:
            print("BAD:", i + 1)

    return sum_of_correct


def with_dividers(packets):
    return packets + [[[2]], [[6]]]


def answer_part_two(data):
    packets = with_dividers(parse_packets(data))
    ordered_packets = sorted(packets, key=cmp_to_key(packet_cmp))
    i2 = ordered_packets.index([[2]]) + 1
    i6 = ordered_packets.index([[6]]) + 1
    return i2 * i6


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="thirteen.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def in_correct_order(left, right):
    if isinstance(left, int) and isinstance(right, int):
        if left == right:
            return None
        return left < right
    if isinstance(left, list) and isinstance(right, list):
        if not left and not right:
            return None
        if not left:
            return True
        if not right:
            return False
        rest_left, rest_right = list(left), list(right)
        check_first = in_correct_order(rest_left.pop(0), rest_right.pop(0))
        if check_first is None:
            return in_correct_order(rest_left, rest_right)
        return check_first

    if isinstance(left, list):
        return in_correct_order(left, [right])
    if isinstance(right, list):
        return in_correct_order([left], right)


def packet_cmp(left, right):
    order = in_correct_order(left, right)
    if order is None:
        return 0
    if order:
        return -1
    return 1


def parse(data):
    pairs = []
    current = []
    for line in data:
        if not line:
            continue
        current.append(json.loads(line))
        if len(current) == 2:
            pairs.append(tuple(current))
            current = []

    return pairs


def parse_packets(data):
    packets = []
    for line in data:
        if not line:
            continue
        packets.append(json.loads(line))

    return packets


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData:
    data = [
        "[1,1,3,1,1]",
        "[1,1,5,1,1]",
        "",
        "[[1],[2,3,4]]",
        "[[1],4]",
        "",
        "[9]",
        "[[8,7,6]]",
        "",
        "[[4,4],4,4]",
        "[[4,4],4,4,4]",
        "",
        "[7,7,7,7]",
        "[7,7,7]",
        "",
        "[]",
        "[3]",
        "",
        "[[[]]]",
        "[[]]",
        "",
        "[1,[2,[3,[4,[5,6,7]]]],8,9]",
        "[1,[2,[3,[4,[5,6,0]]]],8,9]",
    ]

    @pytest.fixture
    def pairs(self):
        return parse(self.data)

    def test_parse(self):
        pairs = parse(self.data)

        assert pairs[0][1] == [1, 1, 5, 1, 1]
        assert pairs[5][0] == []
        assert pairs[6][1] == [[]]

    def test_in_correct_order(self, pairs):
        assert in_correct_order(*pairs[0])
        assert in_correct_order(*pairs[1])
        assert in_correct_order(*pairs[3])
        assert in_correct_order(*pairs[5])

        assert not in_correct_order(*pairs[2])
        assert not in_correct_order(*pairs[4])
        assert not in_correct_order(*pairs[6])
        assert not in_correct_order(*pairs[7])

    def test_not_in_correct_order(self, pairs):
        left = [
            [[], [9, [1, 8, 2, 0], 9, [4]], 7, 1, []],
            [
                [3, 2, 7, 5],
                [[0, 3]],
                [10, [4, 7, 6], [10]],
                [10, [2, 4, 0, 3, 8], [9]],
                [[3, 3], 4, [], 5, 1],
            ],
            [[1, 7, 7], 0, [1, 5], []],
        ]
        right = [
            [[], [], [[], [4, 1, 7, 7], 2, 2, 5], 1, 9],
            [8, 7, 10, [[1, 9, 9, 7], 2]],
        ]

        assert not in_correct_order(left, right)

    def test_packet_cmp(self):
        packets = with_dividers(parse_packets(self.data))
        actual = sorted(packets, key=cmp_to_key(packet_cmp))
        assert actual[0] == []
        assert actual[5] == [[1], [2, 3, 4]]
        assert actual[9] == [[2]]
        assert actual[13] == [[6]]
        assert actual[15] == [7, 7, 7, 7]
        assert actual[17] == [9]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 13

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 140


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
