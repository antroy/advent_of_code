#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from datetime import datetime as dt
from subprocess import PIPE, Popen
import sys
import string

HEIGHT_MAP = {x: string.ascii_lowercase.index(x) for x in string.ascii_lowercase}


class Node:
    def __init__(self, x, y, height):
        self.x, self.y = x, y
        self.height = height
        self.start = False
        self.end = False
        if height == "S":
            self.height = "a"
            self.start = True
        elif height == "E":
            self.height = "z"
            self.end = True

        self.height_i = HEIGHT_MAP[self.height]
        self.distance = 1000000
        self.neighbours = []
        self.previous = None
        self.visited = False

    def __str__(self):
        return "(%s, %s) H: %s, D: %s" % (self.x, self.y, self.height_i, self.distance)

    def coords(self):
        return self.x, self.y

    def dist_to_node(self, n):
        if n.height_i <= (self.height_i + 1):
            return self.distance + 1
        return 1000000

    def route_here(self):
        current = self
        chain = []
        while current.previous:
            chain.insert(0, current)
            current = current.previous
        return chain


def load_grid(data):
    inverted_grid = [[i for i in list(line)] for line in data]
    grid = [list(cols) for cols in zip(*inverted_grid)]

    out = {}
    for x in range(len(grid)):
        for y in range(len(grid[0])):
            out[(x, y)] = Node(x, y, grid[x][y])

    for node in out.values():
        for c in [
            (node.x + 1, node.y),
            (node.x - 1, node.y),
            (node.x, node.y + 1),
            (node.x, node.y - 1),
        ]:
            if c in out:
                node.neighbours.append(out[c])
    return out


def process_node(node):
    for n in node.neighbours:
        if n.visited:
            continue
        dist = node.dist_to_node(n)
        if dist < n.distance:
            n.distance = dist
            n.previous = node
    node.visited = True


def print_shortest_path(end_node):
    chain = end_node.route_here()
    print("Shortest Path:")
    print(" -> ".join(str(n) for n in chain))


def get_shortest_path(nodes, start_coords, coord_shortest_dist_map=None):
    start = nodes[start_coords]
    start.distance = 0
    unvisited = set(nodes.values())
    dest = [n for n in unvisited if n.end][0]

    while unvisited:
        smallest = sorted(list(unvisited), key=lambda n: n.distance)[0]
        process_node(smallest)
        unvisited.remove(smallest)
        if smallest == dest:
            break

    return dest


def row(y, grid):
    return [col[y] for col in grid]


def answer_part_one(data):
    nodes = load_grid(data)
    start = [n for n in nodes.values() if n.start][0]
    return get_shortest_path(nodes, start.coords()).distance


def answer_part_two(data):
    nodes = load_grid(data)
    start_points = [n for n in nodes.values() if n.height == "a"]
    paths = []
    shortest_path = 1000000
    then = dt.now()

    for i, start in enumerate(start_points):
        print("Analysing paths for %s  [%s/%s]" % (start, i + 1, len(start_points)))
        path = get_shortest_path(load_grid(data), start.coords())
        paths.append(path)
        if path:
            shortest_path = min(shortest_path, path.distance)
        now = dt.now()
        diff = now - then
        print("    %s" % diff.total_seconds())
        then = now

    sorted_paths = sorted([p for p in paths if p], key=lambda n: n.distance)

    return sorted_paths[0].distance


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="twelve.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData:
    data = [
        "Sabqponm",
        "abcryxxl",
        "accszExk",
        "acctuvwj",
        "abdefghi",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 31

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 29


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
