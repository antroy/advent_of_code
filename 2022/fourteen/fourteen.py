#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from subprocess import PIPE, Popen
import sys


def get_path(line):
    pairs = line.split(" -> ")
    return [tuple(map(int, p.split(","))) for p in pairs]


def process_path(caves, path):
    for i in range(len(path[:-1])):
        from_c, to_c = path[i : i + 2]
        fx, fy = from_c
        tx, ty = to_c
        if fx == tx:
            ys = sorted([fy, ty])
            for y in range(ys[0], ys[1] + 1):
                caves[(fx, y)] = "#"
        if fy == ty:
            xs = sorted([fx, tx])
            for x in range(xs[0], xs[1] + 1):
                caves[(x, fy)] = "#"


def fill_rock(caves, paths):
    for path in paths:
        process_path(caves, path)


def drop_and_count_sand(caves, part_one):
    depth = max([y for x, y in caves])
    entry_point = (500, 0)
    sand = entry_point
    while True:
        sx, sy = sand
        if part_one and (sy > depth):
            break
        resting_spots = [
            coord
            for coord in [(sx, sy + 1), (sx - 1, sy + 1), (sx + 1, sy + 1)]
            if coord not in caves and coord[1] != (depth + 2)
        ]
        if resting_spots:
            sand = resting_spots[0]
        else:
            caves[sand] = "O"
            if (not part_one) and (sand == entry_point):
                break
            sand = entry_point

    return len([v for v in caves.values() if v == "O"])


def answer_part_one(data):
    paths = [get_path(line) for line in data]
    caves = {}
    fill_rock(caves, paths)

    return drop_and_count_sand(caves, True)


def answer_part_two(data):
    paths = [get_path(line) for line in data]
    caves = {}
    fill_rock(caves, paths)

    return drop_and_count_sand(caves, False)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="fourteen.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestExampleData:
    data = [
        "498,4 -> 498,6 -> 496,6",
        "503,4 -> 502,4 -> 502,9 -> 494,9",
    ]

    def test_get_path(self):
        line = self.data[1]
        actual = get_path(line)
        assert actual == [(503, 4), (502, 4), (502, 9), (494, 9)]

    def test_process_path(self):
        paths = get_path(self.data[0])
        actual = {}
        process_path(actual, paths)
        assert len(actual) == 5

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 24

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 93


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
