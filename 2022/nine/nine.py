#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import sys
from subprocess import PIPE, Popen

import pytest

DIR_ADJUSTMENTS = {
    "L": (-1, 0),
    "R": (1, 0),
    "U": (0, 1),
    "D": (0, -1),
}


def sign(n):
    return -1 if n < 0 else 1


class Knot:
    def __init__(self, coords=(0, 0), tail=None):
        self.x, self.y = coords
        self.tail = tail
        self.history = [coords]

    def __str__(self):
        return "%s" % self.coords()

    def _move_to(self, coord):
        self.x, self.y = coord
        self.history.append(coord)
        if self.tail:
            self.tail.follow(self)

    def coords(self):
        return self.x, self.y

    def follow(self, head):
        dx = head.x - self.x
        dy = head.y - self.y

        if abs(dx) < 2 and abs(dy) < 2:
            return
        if dy == 0:
            self._move_to((self.x + sign(dx), self.y))
            return
        if dx == 0:
            self._move_to((self.x, self.y + sign(dy)))
            return

        self._move_to((self.x + sign(dx), (self.y + sign(dy))))

    def move(self, motion):
        direction, count = motion.split(" ")
        dx, dy = DIR_ADJUSTMENTS[direction]

        for i in range(int(count)):
            self._move_to((self.x + dx, self.y + dy))

        return self.x, self.y

    def visited_count(self):
        return len(list(set(self.history)))


def answer_part_one(data):
    tail = Knot()
    head = Knot(tail=tail)

    for motion in data:
        head.move(motion)

    return tail.visited_count()


def answer_part_two(data):
    knots = []
    for i in range(10):
        previous = knots[-1] if knots else None
        k = Knot(tail=previous)
        knots.append(k)

    head = knots[-1]
    tail = knots[0]

    for motion in data:
        head.move(motion)

    return tail.visited_count()


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--part-one",
        help="Run solution part one",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-b",
        "--part-two",
        help="Run solution part two",
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def get_data(filename="nine.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(
        ["xclip", "-selection", "clipboard"], universal_newlines=True, stdin=PIPE
    ) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


class TestKnot:
    @pytest.mark.parametrize(
        "example",
        [
            ("R 1", (1, 0)),
            ("L 1", (-1, 0)),
            ("U 1", (0, 1)),
            ("D 1", (0, -1)),
            ("R 3", (3, 0)),
            ("L 5", (-5, 0)),
            ("U 2", (0, 2)),
            ("D 11", (0, -11)),
        ],
    )
    def test_move(self, example):
        move, exp = example
        k = Knot()
        coord = k.move(move)
        assert coord == exp

    @pytest.mark.parametrize(
        "example",
        [
            #  Should not move as they are adjacent
            ((1, 0), (0, 0), (0, 0)),
            ((0, 1), (0, 0), (0, 0)),
            ((-1, 0), (0, 0), (0, 0)),
            ((0, -1), (0, 0), (0, 0)),
            ((1, 1), (0, 0), (0, 0)),
            ((-1, 1), (0, 0), (0, 0)),
            ((1, -1), (0, 0), (0, 0)),
            ((-1, -1), (0, 0), (0, 0)),
            #  Should move by one in the same direction
            ((2, 0), (0, 0), (1, 0)),
            ((-2, 0), (0, 0), (-1, 0)),
            ((0, 2), (0, 0), (0, 1)),
            ((0, -2), (0, 0), (0, -1)),
            # Should move diagonally
            ((2, 1), (0, 0), (1, 1)),
            ((2, -1), (0, 0), (1, -1)),
            ((-2, 1), (0, 0), (-1, 1)),
            ((-2, -1), (0, 0), (-1, -1)),
        ],
    )
    def test_follow(self, example):
        head_coords, tail_coords, exp_tail = example
        t = Knot(tail_coords)
        h = Knot(head_coords, t)
        t.follow(h)

        assert t.coords() == exp_tail


class TestExampleData:
    data = [
        "R 4",
        "U 4",
        "L 3",
        "D 1",
        "R 4",
        "D 1",
        "L 5",
        "R 2",
    ]
    data2 = [
        "R 5",
        "U 8",
        "L 8",
        "D 3",
        "R 17",
        "D 10",
        "L 25",
        "U 20",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        assert answer == 13

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        assert answer == 1

    def test_answer_part_two_longer(self):
        answer = answer_part_two(self.data2)
        assert answer == 36


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
