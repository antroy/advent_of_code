#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import unittest
import string
import sys

PRIORITIES = {letter: index + 1
              for index, letter in enumerate(string.ascii_letters)}


def halve(line):
    middle = len(line) // 2
    return set(line[0:middle]), set(line[middle:])


def answer_part_one(data):
    sacks = [halve(line) for line in data]
    duplicates = [left.intersection(right).pop() for left, right in sacks]
    priorities = [PRIORITIES[item] for item in duplicates]

    return sum(priorities)


def triples(bags):
    if len(bags) < 3:
        return bags
    return [bags[0:3]] + triples(bags[3:])


def priority_of_badge(group):
    badge = group[0].intersection(group[1], group[2])

    return PRIORITIES[badge.pop()]


def answer_part_two(data):
    sacks = [set(line) for line in data]
    groups = triples(sacks)
    priorities = [priority_of_badge(group) for group in groups]

    return sum(priorities)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="three.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


class TestExampleData(unittest.TestCase):
    data = [
        "vJrwpWtwJgWrhcsFMMfFFhFp",
        "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
        "PmmdzqPrVvPwwTWBwg",
        "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
        "ttgJtRGJQctTZtZT",
        "CrZsJsPPZsGzwwsLwLmpwMDw"
    ]

    def test_priorities(self):
        self.assertEqual(PRIORITIES['a'], 1)
        self.assertEqual(PRIORITIES['z'], 26)
        self.assertEqual(PRIORITIES['A'], 27)
        self.assertEqual(PRIORITIES['Z'], 52)

    def test_halve(self):
        self.assertEqual(halve("ab"), (set("a"), set("b")))
        self.assertEqual(halve("abcdef"), (set("abc"), set("def")))
        self.assertEqual(halve("abcdefghijkl"), (set("abcdef"), set("ghijkl")))

    def test_triples(self):
        self.assertEqual(triples([]), [])
        self.assertEqual(triples(list("ab")), ["a", "b"])
        self.assertEqual(triples(list("abcdefghijkl")),
                         [list("abc"), list("def"), list("ghi"), list("jkl")])

    def test_badge_priority(self):
        group1 = [set(bag) for bag in self.data[0:3]]
        group2 = [set(bag) for bag in self.data[3:]]
        self.assertEqual(priority_of_badge(group1), 18)
        self.assertEqual(priority_of_badge(group2), 52)

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        self.assertEqual(answer, 157)

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        self.assertEqual(answer, 70)


unittest.main()
