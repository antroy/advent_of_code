#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import unittest
from subprocess import PIPE, Popen
import sys


def parse_pairs(line):
    one, two = line.split(",")
    range_one = tuple(int(x) for x in one.split("-"))
    range_two = tuple(int(x) for x in two.split("-"))
    return range_one, range_two


def full_containment(one, two):
    def a_contains_b(a, b):
        return a[0] <= b[0] and a[1] >= b[1]

    return a_contains_b(one, two) or a_contains_b(two, one)


def overlap(one, two):
    def a_overlaps_b(a, b):
        return b[0] <= a[0] <= b[1] or b[0] <= a[1] <= b[1]

    return a_overlaps_b(one, two) or a_overlaps_b(two, one)


def answer_part_one(data):
    pairs = [parse_pairs(line) for line in data]
    full_overlaps = [pair for pair in pairs
                     if full_containment(*pair)]

    return len(full_overlaps)


def answer_part_two(data):
    pairs = [parse_pairs(line) for line in data]
    full_overlaps = [pair for pair in pairs
                     if overlap(*pair)]

    return len(full_overlaps)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="four.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)


class TestExampleData(unittest.TestCase):
    # Added the reverse of 4th pair to the sample data
    data = [
        "2-4,6-8",
        "2-3,4-5",
        "5-7,7-9",
        "2-8,3-7",
        "3-7,2-8",
        "6-6,4-6",
        "2-6,4-8"
    ]

    def test_overlap(self):
        self.assertTrue(overlap((1, 10), (3, 6)))
        self.assertTrue(overlap((1, 4), (3, 4)))
        self.assertTrue(overlap((1, 4), (1, 2)))
        self.assertTrue(overlap((1, 4), (1, 4)))
        self.assertTrue(overlap((5, 10), (3, 6)))
        self.assertTrue(overlap((5, 10), (7, 12)))
        self.assertTrue(overlap((3, 6), (5, 10)))
        self.assertTrue(overlap((7, 12), (5, 10)))
        self.assertTrue(overlap((7, 12), (5, 7)))

        self.assertFalse(overlap((5, 10), (1, 2)))
        self.assertFalse(overlap((5, 10), (11, 22)))

    def test_full_containment(self):
        self.assertTrue(full_containment((1, 10), (3, 6)))
        self.assertTrue(full_containment((1, 4), (3, 4)))
        self.assertTrue(full_containment((1, 4), (1, 2)))
        self.assertTrue(full_containment((1, 4), (1, 4)))

        self.assertFalse(full_containment((5, 10), (3, 6)))
        self.assertFalse(full_containment((5, 10), (7, 12)))
        self.assertFalse(full_containment((5, 10), (1, 2)))
        self.assertFalse(full_containment((5, 10), (11, 22)))

    def test_parse_pairs(self):
        self.assertEqual(
            parse_pairs(self.data[0]),
            ((2, 4), (6, 8))
        )

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        self.assertEqual(answer, 3)

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        self.assertEqual(answer, 5)


unittest.main()
