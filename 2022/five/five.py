#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import copy
import unittest
import re
from subprocess import PIPE, Popen
import sys

MOVE_PATTERN = r"move (\d+) from (\d+) to (\d+)"


def process_instruction(line):
    m = re.match(MOVE_PATTERN, line)
    out = [int(row) for row in m.groups()]
    out[1] -= 1
    out[2] -= 1

    return tuple(out)


def process_stacks(lines):
    rows = [tuple(row[1::4]) for row in reversed(lines)]

    header = rows.pop(0)
    stacks = [[] for i in header]  # Initialize from header
    for row in rows:
        for i, item in enumerate(row):
            if item.strip():
                stacks[i].append(item)
    return stacks


def move_9000(instruction, stacks):
    count, from_s, to_s = instruction
    if count == 0:
        return stacks

    out = copy.deepcopy(stacks)
    out[to_s].append(out[from_s].pop())

    return move_9000((count - 1, from_s, to_s), out)


def move_9001(instruction, stacks):
    count, from_s, to_s = instruction
    if count == 0:
        return stacks

    out = copy.deepcopy(stacks)
    out[to_s].extend(out[from_s][-count:])
    out[from_s][-count:] = []

    return out


def stacks_and_instructions(data):
    raw_stacks = [line for line in data if line and "move" not in line]
    instructions = [process_instruction(line)
                    for line in data if "move" in line]

    stacks = process_stacks(raw_stacks)
    return stacks, instructions


def move_and_get_top(data, move_fn):
    stacks, instructions = stacks_and_instructions(data)

    for instruction in instructions:
        stacks = move_fn(instruction, stacks)

    return "".join(stack[-1] for stack in stacks
                   if stack and stack[-1].strip())


def answer_part_one(data):
    return move_and_get_top(data, move_9000)


def answer_part_two(data):
    return move_and_get_top(data, move_9001)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one",
                        action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two",
                        action="store_true", default=False)

    return parser.parse_args()


def get_data(filename="five.txt"):
    with open(filename) as fh:
        return [line.rstrip() for line in fh]


def clip(data):
    print("Storing Code")
    with Popen(["xclip", "-selection", "clipboard"],
               universal_newlines=True, stdin=PIPE) as fh:
        fh.stdin.write(str(data))
        fh.communicate()


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        clip(answer)
        sys.exit(0)


class TestExampleData(unittest.TestCase):
    data = [
        "    [D]    ",
        "[N] [C]    ",
        "[Z] [M] [P]",
        " 1   2   3 ",
        "",
        "move 1 from 2 to 1",
        "move 3 from 1 to 3",
        "move 2 from 2 to 1",
        "move 1 from 1 to 2"
    ]

    def test_stacks(self):
        stacks, instructions = stacks_and_instructions(self.data)

        exp_stacks = [["Z", "N"], ["M", "C", "D"], ["P"]]

        self.assertEqual(stacks, exp_stacks)

    def test_instructions(self):
        stacks, instructions = stacks_and_instructions(self.data)

        exp_instructions = [(1, 1, 0), (3, 0, 2), (2, 1, 0), (1, 0, 1)]

        self.assertEqual(instructions, exp_instructions)

    def test_move_9000_1(self):
        instruction = (1, 1, 0)
        stacks = [["Z", "N"], ["M", "C", "D"], ["P"]]
        new_stacks = move_9000(instruction, stacks)
        exp_new_stacks = [["Z", "N", "D"], ["M", "C"], ["P"]]
        self.assertEqual(new_stacks, exp_new_stacks)

    def test_move_9000_2(self):
        instruction = (3, 0, 2)
        stacks = [["Z", "N", "D"], ["M", "C"], ["P"]]
        new_stacks = move_9000(instruction, stacks)
        exp_new_stacks = [[], ["M", "C"], ["P", "D", "N", "Z"]]
        self.assertEqual(new_stacks, exp_new_stacks)

    def test_move_9001_1(self):
        instruction = (1, 1, 0)
        stacks = [["Z", "N"], ["M", "C", "D"], ["P"]]
        new_stacks = move_9001(instruction, stacks)
        exp_new_stacks = [["Z", "N", "D"], ["M", "C"], ["P"]]
        self.assertEqual(new_stacks, exp_new_stacks)

    def test_move_9001_2(self):
        instruction = (3, 0, 2)
        stacks = [["Z", "N", "D"], ["M", "C"], ["P"]]
        new_stacks = move_9001(instruction, stacks)
        exp_new_stacks = [[], ["M", "C"], ["P", "Z", "N", "D"]]
        self.assertEqual(new_stacks, exp_new_stacks)

    def test_answer_part_one(self):
        answer = answer_part_one(self.data)
        self.assertEqual(answer, "CMZ")

    def test_answer_part_two(self):
        answer = answer_part_two(self.data)
        self.assertEqual(answer, "MCD")


unittest.main()
