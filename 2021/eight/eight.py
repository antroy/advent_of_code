#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys

class Display:
    def __init__(self, data_line):
        signals, output = data_line.split("|")
        self.signals = signals.strip().split()
        self.output = output.strip().split()

    def known_digits_in_output(self):
        out = 0
        for digit in self.output:
            length = len(digit)
            if length in [2, 3, 4, 7]:
                out += 1

        return out

    def is_in(self, contained, container):
        return len(set(contained) - set(container)) == 0


    def map_digits(self):
        out = {}
        out2 = {}

        useful_order = sorted(self.signals, key=lambda x: [2,3,4,7,6,5].index(len(x)))

        for digit in ["".join(sorted(s)) for s in useful_order]:
            length = len(digit)
            if length == 2:
                out[digit] = 1
                out2[1] = digit
            elif length == 3:
                out[digit] = 7
                out2[7] = digit
            elif length == 4:
                out[digit] = 4
                out2[4] = digit
            elif length == 7:
                out[digit] = 8
                out2[8] = digit
            elif length == 6:
                # 9, 6 or 0
                if self.is_in(out2[4], digit):
                    out[digit] = 9
                    out2[9] = digit
                elif self.is_in(out2[1], digit):
                    out[digit] = 0
                    out2[0] = digit
                else:
                    out[digit] = 6
                    out2[6] = digit
            elif length == 5:
                # 5, 2 or 3
                if self.is_in(out2[1], digit):
                    out[digit] = 3
                elif self.is_in(digit, out2[6]):
                    out[digit] = 5
                else:
                    out[digit] = 2
            else:
                out[digit] = -1

        return out

    def get_output_value(self):
        digit_map = self.map_digits()

        sorted_digits = ["".join(sorted(item)) for item in self.output]
        numbers = [digit_map[digit] for digit in sorted_digits]
        return int("%s%s%s%s" % tuple(numbers))

class DigitSolver:
    def __init__(self, data):
        self.displays = [Display(line) for line in data]

    def count_easy_digits(self):
        return sum([d.known_digits_in_output() for d in self.displays])

    def add_output_values(self):
        return sum([display.get_output_value() for display in self.displays])

def answer_part_one(data):

    return DigitSolver(data).count_easy_digits()


def answer_part_two(data):

    return DigitSolver(data).add_output_values()


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data(filename):
    with open(filename) as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data("eight.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data("eight.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = [
    "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe",
    "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc",
    "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg",
    "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb",
    "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea",
    "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb",
    "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe",
    "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef",
    "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb",
    "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"
]

class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 26)


    def test_answer_part_two(self):
        answer = answer_part_two(TEST_DATA)
        self.assertEqual(answer, 61229)


class TestDigitSolver(unittest.TestCase):
    def test_count_known_digits_in_output(self):
        solver = DigitSolver(TEST_DATA)
        self.assertEqual(solver.count_easy_digits(), 26)

    def test_add_output_values(self):
        solver = DigitSolver(TEST_DATA)
        self.assertEqual(solver.add_output_values(), 61229)


class TestDisplay(unittest.TestCase):
    def test_init(self):
        data = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
        display = Display(data)
        self.assertEqual(display.signals, ["acedgfb", "cdfbe", "gcdfa", "fbcad", "dab", "cefabd", "cdfgeb", "eafb", "cagedb", "ab"])
        self.assertEqual(display.output, ["cdfeb", "fcadb", "cdfeb", "cdbaf"])

    def test_is_in(self):
        display = Display("abc | abc")

        self.assertTrue(display.is_in("abef", "abcefg"))
        self.assertTrue(display.is_in("bf", "abcefg"))
        self.assertFalse(display.is_in("abef", "abceg"))
        self.assertFalse(display.is_in("bf", "acefg"))

    def test_map_digits(self):
        data = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
        display = Display(data)
        mapped_digits = display.map_digits()
        self.assertEqual(0, mapped_digits["abcdeg"])
        self.assertEqual(1, mapped_digits["ab"])
        self.assertEqual(2, mapped_digits["acdfg"])
        self.assertEqual(3, mapped_digits["abcdf"])
        self.assertEqual(4, mapped_digits["abef"])
        self.assertEqual(5, mapped_digits["bcdef"])
        self.assertEqual(6, mapped_digits["bcdefg"])
        self.assertEqual(7, mapped_digits["abd"])
        self.assertEqual(8, mapped_digits["abcdefg"])
        self.assertEqual(9, mapped_digits["abcdef"])

    def test_get_output_value(self):
        data = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
        display = Display(data)
        self.assertEqual(display.get_output_value(),  5353)
unittest.main()
