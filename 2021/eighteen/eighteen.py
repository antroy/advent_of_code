#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, json, re, sys
from copy import deepcopy

def unpack(string):
    return [int(c) if c.isdigit() else c for c in string if c != ","] 

def pack(array):
    out = []
    for e in array:
        if type(e) == int:
            pass
            #  if out[-1].isdigit() :
                #  out.append(",")

def add_numbers(a, b):
    return [a, b]

def reduce(n, element, depth):
    if depth == 5:
        pass

def explode(n):
    locations = annotate(n)
    out = deepcopy(n)

    for loc in locations:
        number, index = loc
        if len(index) > 4:
            if 1 in index:
                previous_index = list(reversed(loc[1])).index(1) - 1
                print(previous_index)

                
            if 0 in index:
                next_index = list(reversed(loc[1])).index(1) - 1
                print(next_index)
                out

def add_to_item_at(n, index):
    if not index:
        return n
    
    return item_at(n[index[0]], index[1:])
            

def split(n):
    pass

def annotate(n, loc=None):
    if not loc:
        loc = []

    left = n[0]
    right = n[1]

    out_l = None
    out_r = None
    
    if type(left) == int:
        out_l = [(left, loc + [0])]
    else:
        out_l = annotate(left, loc + [0])

    if type(right) == int:
        out_r = [(right, loc + [1])]
    else:
        out_r = annotate(right, loc + [1])

    return out_l + out_r


def answer_part_one(data):

    return None


def answer_part_two(data):

    return None


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def parse_numbers(data):
    return [json.loads(line) for line in data]

def get_data():
    with open("eighteen.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

class TestExampleData(unittest.TestCase):
    def test_unpack(self):
        self.assertEqual(unpack("[[3,[4,5]],[1,2]]"), ["[","[",3,"[",4,5,"]","]","[",1,2,"]","]"])

    def xtest_pack(self):
        self.assertEqual(pack(["[","[",3,"[",4,5,"]","]","[",1,2,"]","]"]), "[[3,[4,5]],[1,2]]")

    def test_annotate(self):
        number = [[1,[2,[3,4]]],[5,6]]

        ann = annotate(number)

        print(ann)

    def test_parse_numbers(self):
        data = [
            "[1,1]",
            "[2,2]",
            "[3,3]",
            "[4,4]"
        ]

        numbers = parse_numbers(data)

        self.assertEqual(numbers[1], [2,2])
        self.assertEqual(numbers[3], [4,4])

    def test_explode(self):
        num = parse_numbers(["[[[[[3,3],0],0],0],0]"])[0]
        self.assertEqual(explode(num), "[[[[0,3],0],0],0]")

    def test_add_numbers(self):
        self.assertEqual(add_numbers([1,1], [2,2]), [[1,1],[2,2]])

    def test_answer_part_one(self):
        data = [
        ]
        answer = answer_part_one(data)
        self.assertEqual(answer, None)


    def test_answer_part_two(self):
        data = [
        ]
        answer = answer_part_two(data)
        self.assertEqual(answer, None)

unittest.main()
