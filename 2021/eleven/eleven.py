#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys

class Cave:
    def __init__(self, data):
        self.rows = [[int(number) for number in line] for line in data]
        self.flash_count = 0
        self.step_count = 0
        self.first_simul_flash = None

    def __str__(self):
        return "\n".join(self.cave_map())

    def print_flashes(self):
        out = ["|".join(["%-2s" % col for col in row]) for row in self.rows]
        width = len(out[0])
        out = "\n".join(out)
        out = re.sub("(0)", r"\033[1;33m\1\033[0m", out)
        print("-" * width)
        print(out)
        print("-" * width)

    def cave_map(self):
        return ["".join([str(col) for col in row]) for row in self.rows]

    def inside(self, octopus):
        max_x = len(self.rows[0])
        max_y = len(self.rows)

        if octopus[0] < 0:
            return False
        if octopus[0] >= max_x:
            return False
        if octopus[1] < 0:
            return False
        if octopus[1] >= max_y:
            return False
        return True

    def brightness(self, octopus):
        return self.rows[octopus[1]][octopus[0]]

    def increase_brightness(self, octopus):
        self.rows[octopus[1]][octopus[0]] += 1

    def neighbours(self, octopus):
        x, y = octopus
        up = (x, y-1)
        left = (x-1, y)
        below = (x, y+1)
        right = (x+1, y)
        up_left = (x-1, y-1)
        below_left = (x-1, y+1)
        up_right = (x+1, y-1)
        below_right = (x+1, y+1)
        neighbours = [up, left, right, below, up_left, up_right, below_left, below_right]

        return [neighbour for neighbour in neighbours if self.inside(neighbour)]

    def octopi(self):
        return [(x, y) for y in range(len(self.rows)) for x in range(len(self.rows[0]))]

    def increase_levels(self):
        for octopus in self.octopi():
            self.increase_brightness(octopus)

    def flash(self, octopus):
        for n in self.neighbours(octopus):
            self.increase_brightness(n)

    def run_flashes(self, flashed=set()):
        ready_to_flash = set([p for p in self.octopi() if self.brightness(p) > 9])
        flashes = ready_to_flash - flashed

        if not flashes:
            return

        flashes_with_brightnesss = [(p, self.brightness(p)) for p in flashes]

        for octopus, brightness in flashes_with_brightnesss:
            if brightness > 9:
                self.flash(octopus)
        all_flashers = flashed.union(flashes)
        self.run_flashes(all_flashers)

    def reset(self):
        flash_count = 0
        for octopus in self.octopi():
            if self.brightness(octopus) > 9:
                flash_count += 1
                self.rows[octopus[1]][octopus[0]] = 0
        self.flash_count += flash_count

        if self.step_count in [193, 194, 195]:
            self.print_flashes()

        if self.first_simul_flash == None and flash_count == len(self.octopi()):
            self.first_simul_flash = self.step_count

    def step(self):
        self.step_count += 1
        self.increase_levels()
        self.run_flashes()
        self.reset()

    def run_x_steps(self, number_of_steps):
        for i in range(number_of_steps):
            self.step()

def answer_part_one(data):
    cave = Cave(data)
    cave.run_x_steps(100)
    return cave.flash_count

def answer_part_two(data):
    cave = Cave(data)
    cave.run_x_steps(200)

    while cave.first_simul_flash == None:
        cave.run_x_steps(10)

    return cave.first_simul_flash


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("eleven.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = [
    "5483143223",
    "2745854711",
    "5264556173",
    "6141336146",
    "6357385478",
    "4167524645",
    "2176841721",
    "6882881134",
    "4846848554",
    "5283751526"
]

class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        result = answer_part_one(TEST_DATA)
        self.assertEqual(result, 1656)

    def test_answer_part_two(self):
        result = answer_part_two(TEST_DATA)
        self.assertEqual(result, 195)

class TestCave(unittest.TestCase):
    def test_init(self):
        cave = Cave(TEST_DATA)

        self.assertEqual(cave.rows[1], [2,7,4,5,8,5,4,7,1,1])
        self.assertEqual(cave.rows[4], [6,3,5,7,3,8,5,4,7,8])

    def test_inside(self):
        cave = Cave(TEST_DATA)

        self.assertTrue(cave.inside((0, 0)))
        self.assertTrue(cave.inside((0, 4)))
        self.assertTrue(cave.inside((9, 0)))
        self.assertTrue(cave.inside((9, 4)))
        self.assertTrue(cave.inside((5, 2)))
        self.assertTrue(cave.inside((6, 5)))
        self.assertFalse(cave.inside((-1, 2)))
        self.assertFalse(cave.inside((6, -1)))
        self.assertFalse(cave.inside((10, 2)))

    def test_increase_levels(self):
        data = ["01", "67"]
        cave = Cave(data)
        cave.increase_levels()
        self.assertEqual(cave.brightness((0,0)), 1)
        self.assertEqual(cave.brightness((1,0)), 2)
        self.assertEqual(cave.brightness((0,1)), 7)
        self.assertEqual(cave.brightness((1,1)), 8)

        data = ["91", "97"]
        cave = Cave(data)
        cave.increase_levels()
        self.assertEqual(cave.brightness((0,0)), 10)
        self.assertEqual(cave.brightness((1,0)), 2)
        self.assertEqual(cave.brightness((0,1)), 10)
        self.assertEqual(cave.brightness((1,1)), 8)

    def test_step(self):
        data = [
            "11111",
            "19991",
            "19191",
            "19991",
            "11111"
        ]
        cave = Cave(data)
        cave.step()

        expected = [
            "34543",
            "40004",
            "50005",
            "40004",
            "34543"
        ]
        self.assertEqual(cave.cave_map(), expected)

    def test_run_10_steps(self):
        cave = Cave(TEST_DATA)
        cave.run_x_steps(1)
        self.assertEqual(cave.flash_count, 0)

        cave.run_x_steps(1)
        self.assertEqual(cave.flash_count, 35)

        cave.run_x_steps(8)
        self.assertEqual(cave.flash_count, 204)


unittest.main()
