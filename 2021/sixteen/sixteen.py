#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys

class Packet:
    def __init__(self, version, id, data):
        self.version, self.id, self.data = version, id, data

    def __str__(self):
        return "(%s, %s): %s" % (self.version, self.id, self.data)

def hex2bin(hex_digits):
    return bin(int(hex_digits, 16))[2:].zfill(len(hex_digits) * 4)

def parse_packet(packet_hex):
    packet = hex2bin(packet_hex)
    version = int(packet[0:3], 2)
    type_id = int(packet[3:6], 2)
    data = packet[6:]
    return version, type_id, data

def literal_parser(packet, value=""):
    used = packet[0:6]
    data = packet[6:]

    while True:
        if not data:
            return int(value, 2), data

        value += data[1:5]
        data_out = data[5:]
        used += data[0:5]

        if data[0] == "0":
            value_len = len(value)
            parts = value_len // 4
            version_type_bits = 6
            parsed_length = value_len + parts + version_type_bits
            bit_offset = (4 - parsed_length) % 4
            remainder = data_out #[bit_offset:]

            return int(value, 2), remainder

        data = data_out

def op_0_parser(version, type_id, packet):
    print("OP0")
    data = packet[7:]
    sub_packets_length = int(data[0:15], 2)
    remainder = data[15:]
    sub_packets = remainder[0:sub_packets_length]

    out = []
    while len(sub_packets) > 6:
        packet, sub_packets = process_packet(sub_packets)
        if packet:
            out.append(packet)

    return (version, type_id, out), remainder[sub_packets_length:]

def op_1_parser(version, type_id, packet):
    print("OP1")
    data = packet[7:]
    no_subs_bin = data[0:11]
    no_sub_packets = int(no_subs_bin, 2)
    remainder = data[11:]

    print("Should be %s subpackets" % no_sub_packets)

    sub_packets = []
    for i in range(no_sub_packets):
        if not remainder:
            break
        packet, remainder = process_packet(remainder)
        if packet:
            sub_packets.append(packet)

    return (version, type_id, sub_packets), "" # Ignore any trailing chars

PARSERS = {"0": op_0_parser, "1": op_1_parser}

def process_packet(packet):
    packets = []

    if not packet or re.match(r"^0+$", packet):
        print("??? : %s" % packet)
        return None, packet

    version = int(packet[0:3], 2)
    type_id = int(packet[3:6], 2)
    
    print("V: %s; T: %s" % (version, type_id))
    data = None

    if type_id == 4:
        number, remainder = literal_parser(packet)
        return (version, type_id, number), remainder
    else:
        length_type = packet[6]

        return PARSERS[length_type](version, type_id, packet)
    
def version_sum(packet):
    version, type_id, data = packet

    if type_id == 4:
        return version

    total = version

    for packet in data:
        total += version_sum(packet)

    return total

def answer_part_one(data):
    packet = hex2bin(data[0])
    print("Orig:" + packet[0:100])
    res, _ = process_packet(packet)
    print("ONE: %s" % (res,))
    return version_sum(res)


def answer_part_two(data):

    return None


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("sixteen.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

class TestExampleData(unittest.TestCase):
    def test_hex2bin(self):
        self.assertEqual(hex2bin("5A"), "01011010")
        self.assertEqual(hex2bin("2E7"), "001011100111")
        self.assertEqual(hex2bin("1F"), "00011111")
        self.assertEqual(hex2bin("B33CA"), "10110011001111001010")
        self.assertEqual(hex2bin("5D"), "01011101")

    def test_parse_packet(self):
        packet = "D2FE28"
        version, type_id, data = parse_packet(packet)
        self.assertEqual(version, 6)
        self.assertEqual(type_id, 4)
        self.assertEqual(data, "101111111000101000")

    def test_parse_one_literal_message(self):
        packet = hex2bin("D2FE28")
        # Binary: 110 100 101111111000101000

        literal, remainder = process_packet(packet)
        version, id, number = literal
        
        self.assertEqual(version, 6)
        self.assertEqual(id, 4)
        self.assertEqual(number, 2021)

    def test_parse_operator_0_message(self):
        packet = hex2bin("38006F45291200")
        # Binary: 001 110 0 0000000000110111101000101001010010001001000000000

        data, _ = process_packet(packet)

        first = data[2][0]
        number_1 = first[2]
        second = data[2][1]
        number_2 = second[2]
        self.assertEqual(number_1, 10)
        self.assertEqual(number_2, 20)

    def test_parse_operator_1_message(self):
        packet = hex2bin("EE00D40C823060")
        # Binary: 111 011 1 0000000001101010000001100100000100011000001100000
        data, _ = process_packet(packet)
        
        self.assertEqual(len(data), 3)

        first = data[2][0]
        number_1 = first[2]
        second = data[2][1]
        number_2 = second[2]
        third = data[2][2]
        number_3 = third[2]
        self.assertEqual(number_1, 1)
        self.assertEqual(number_2, 2)
        self.assertEqual(number_3, 3)

    def test_example_1(self):
        packet = hex2bin("8A004A801A8002F478")
        res, _ = process_packet(packet)
        total = version_sum(res)

        self.assertEqual(total, 16)

    def test_example_2(self):
        packet = hex2bin("620080001611562C8802118E34")
        res, _ = process_packet(packet)
        total = version_sum(res)

        self.assertEqual(total, 12)

    def test_example_3(self):
        packet = hex2bin("C0015000016115A2E0802F182340")
        res, _ = process_packet(packet)
        total = version_sum(res)

        self.assertEqual(total, 23)

    def test_example_4(self):
        packet = hex2bin("A0016C880162017C3686B18A3D4780")
        res, _ = process_packet(packet)
        total = version_sum(res)

        self.assertEqual(total, 31)

    def test_version_sum(self):
        data = (3,4,6)
        self.assertEqual(version_sum(data), 3)

        data = (3, 1, [(1,4,3), (8,4,3)])
        self.assertEqual(version_sum(data), 12)

        data = (3, 1, [(1,4,3), (2, 1, [(8,4,3), (5, 0, [(2, 4, 8)])])])
        self.assertEqual(version_sum(data), 21)

unittest.main()
