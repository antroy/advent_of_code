#!/usr/bin/env python
# -*- coding: utf-8 -*-

with open("one.txt") as fh:
    data = [int(line.strip()) for line in fh]

    previous = None
    count = 0

    for n in data:
        if previous != None:
            if previous < n:
                count += 1
        previous = n

    print("Count: %s" % count)
