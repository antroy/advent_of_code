#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys

def this_is_an_increase(the_sum, previous_sum):
    if previous_sum != None:
        return the_sum > previous_sum
    return False


def count_sliding_window_increases(data):
    a, b = None, None
    previous_sum = None
    count = 0

    for n in data:
        if None not in [a, b]:
            the_sum = a + b + n
            if this_is_an_increase(the_sum, previous_sum):
                count += 1
            previous_sum = the_sum

        a, b = b, n

    return count

def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--test", help="Run tests", action="store_true", default=False)

    return parser.parse_args()


if __name__ == "__main__":
    options = opts()
    
    if not options.test:
        with open("one.txt") as fh:
            data = [int(line.strip()) for line in fh]
        triple_count = count_sliding_window_increases(data)
        print("Count: %s" % triple_count)
        sys.exit(0)

sys.argv[1:] = []

import unittest

class TestExampleData(unittest.TestCase):
    def test_sample(self):
        data = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
        triple_count = count_sliding_window_increases(data)
        self.assertEquals(triple_count, 5)

unittest.main()
