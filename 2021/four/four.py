#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys


def answer_part_one(data):

    return None


def answer_part_two(data):

    return None


class Bingo:
    def __init__(self, numbers, cards_as_number_arrays):
        self.bingo_callouts = numbers
        self.cards = []
        self.winners = []

        for card in cards_as_number_arrays:
            self.cards.append(Card(self._card_from_numbers(card)))

    def _card_from_numbers(self, number_array):
        return [[Entry(number) for number in row] for row in number_array]

    def _check_for_a_winner(self, last_number_read):
        for card in self.cards:
            if card.has_winning_line():
                self.winners.append((card, last_number_read))
                self.cards.remove(card)

    def winners_card(self):
        return self.winners[0][0] if self.winners else None

    def losers_card(self):
        return self.winners[-1][0] if self.winners else None

    def mark(self, number):
        for card in self.cards:
            card.mark(number)
        self._check_for_a_winner(number)

    def losers_score(self):
        self._run_game()
        losing_card, losing_number = self.winners[-1]
        print("%s\nLast Number: %s" % (losing_card, losing_number))
        return losing_card.score(losing_number)

    def winners_score(self):
        self._run_game()
        winning_card, winning_number = self.winners[0]
        return winning_card.score(winning_number)

    def _run_game(self):
        for number in self.bingo_callouts:
            self.mark(number)


class Card:
    def __init__(self, entry_array):
        self.card = entry_array
        self.id = "".join([str(entry.number) for row in self.card for entry in row])[0:10]

    def __str__(self):
        out = []
        for row in self.card:
            out.append(" ".join([str(entry) for entry in row]))
        return "\n".join(out)

    def _rows(self):
        return self.card

    def _columns(self):
        return [*zip(*self.card)]

    def _all_marked(self, line):
        return all([entry.marked for entry in line])

    def _sum_unmarked_numbers(self):
        unmarked = [entry.number for row in self.card for entry in row if not entry.marked]
        return sum(unmarked)

    def mark(self, number):
        for row in self.card:
            for entry in row:
                if entry.number == number:
                    entry.mark()

    def has_winning_line(self):
        for row in self._rows():
            if self._all_marked(row):
                return True

        for column in self._columns():
            if self._all_marked(column):
                return True

        return False

    def score(self, last_called_number):
        if not self.has_winning_line():
            return 0

        return last_called_number * self._sum_unmarked_numbers()



class Entry:
    def __init__(self, number, marked=False):
        self.number = number
        self.marked = marked

    def __str__(self):
        return "%2s%s" % (self.number, "*" if self.marked else " ")

    def mark(self):
        self.marked = True


def line_to_number_list(line, sep):
    return [int(number.strip()) for number in line.split(sep) if number.strip()]


def ingest_data(data):
    numbers = line_to_number_list(data[0], ',')

    cards = []
    card = None

    for line in data[1:]:
        if line:
            if not card:
                card = []
                cards.append(card)
            card.append(line_to_number_list(line, ' '))
        else:
            card = None

    return numbers, cards


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part 2", action="store_true", default=False)

    return parser.parse_args()


def data_from_file(filename):
    with open(filename) as fh:
        return [line.strip() for line in fh]


if __name__ == "__main__":
    options = opts()

    if options.part_one:
        data = data_from_file("four.txt")
        numbers, cards = ingest_data(data)
        bingo = Bingo(numbers, cards)

        score = bingo.winners_score()

        print("Score: %s" % score)

        sys.exit(0)
    elif options.part_two:
        data = data_from_file("four.txt")
        numbers, cards = ingest_data(data)
        bingo = Bingo(numbers, cards)

        score = bingo.losers_score()

        print("Score: %s" % score)

        sys.exit(0)


import unittest

class TestEntry(unittest.TestCase):
    def test_entry_creation(self):
        entry = Entry(7)
        self.assertEqual(entry.number, 7)
        self.assertFalse(entry.marked)

        entry = Entry(8, True)
        self.assertEqual(entry.number, 8)
        self.assertTrue(entry.marked)

    def test_entry_marking(self):
        entry = Entry(9)
        self.assertFalse(entry.marked)
        entry.mark()
        self.assertTrue(entry.marked)


class TestCard(unittest.TestCase):
    def test_card_creation(self):
        entries = [[Entry(1), Entry(3)], [Entry(11), Entry(8)]]
        card = Card(entries)
        self.assertFalse(card.has_winning_line())

    def test_card_with_winning_row(self):
        entries = [[Entry(1, True), Entry(3, True)], [Entry(11), Entry(8)]]
        card = Card(entries)
        self.assertTrue(card.has_winning_line())

    def test_card_with_winning_column(self):
        entries = [[Entry(1), Entry(3, True)], [Entry(11), Entry(8, True)]]
        card = Card(entries)
        self.assertTrue(card.has_winning_line())

    def test_incomplete_card_score(self):
        entries = [[Entry(1), Entry(3)], [Entry(11), Entry(8)]]
        card = Card(entries)
        self.assertEqual(card.score(5), 0)

    def test_complete_card_score(self):
        entries = [[Entry(1, True), Entry(3)], [Entry(11, True), Entry(8)]]
        card = Card(entries)
        self.assertEqual(card.score(11), 121)

        entries = [[Entry(1, True), Entry(3)], [Entry(11, True), Entry(8)]]
        card = Card(entries)
        self.assertEqual(card.score(1), 11)

        entries = [[Entry(1), Entry(3)], [Entry(11, True), Entry(8, True)]]
        card = Card(entries)
        self.assertEqual(card.score(8), 32)


class TestBingo(unittest.TestCase):
    def test_bingo_creation(self):
        data = data_from_file("four_test.txt")
        numbers, cards = ingest_data(data)
        bingo = Bingo(numbers, cards)

        self.assertEqual(bingo.winners_card(), None)
        self.assertEqual(bingo.cards[1].card[2][2].number, 7)

    def test_bingo_row_complete(self):
        data = data_from_file("four_test.txt")
        numbers, cards = ingest_data(data)
        bingo = Bingo(numbers, cards)

        bingo.mark(0)
        bingo.mark(13)
        bingo.mark(7)
        bingo.mark(10)
        bingo.mark(16)

        self.assertEqual(bingo.winners_card().id[0:10], "3150222918")

    def test_game_runs(self):
        data = data_from_file("four_test.txt")
        numbers, cards = ingest_data(data)
        bingo = Bingo(numbers, cards)

        score = bingo.winners_score()

        self.assertEqual(bingo.winners_card().id[0:10], "1421172441")
        self.assertEqual(score, 4512)

    def test_last_winning_board(self):
        data = data_from_file("four_test.txt")
        numbers, cards = ingest_data(data)
        bingo = Bingo(numbers, cards)

        score = bingo.losers_score()

        self.assertEqual(bingo.losers_card().id[0:10], "3150222918")
        self.assertEqual(score, 1924)


class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        data = [
        ]
        answer = answer_part_one(data)
        self.assertEqual(answer, None)


    def test_answer_part_two(self):
        data = [
        ]
        answer = answer_part_two(data)
        self.assertEqual(answer, None)

    def test_ingest_data(self):
        data = data_from_file("four_test.txt")
        numbers, cards = ingest_data(data)

        self.assertEqual(numbers[0:5], [7, 4, 9, 5, 11])
        self.assertEqual(cards[0][0], [22, 13, 17, 11, 0])
        self.assertEqual(cards[1][1], [9, 18, 13, 17, 5])
        self.assertEqual(cards[2][3], [22, 11, 13, 6, 5])

unittest.main()
