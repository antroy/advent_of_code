#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys


def transpose(codes):
    if not codes:
        return []

    out = []

    for i in range(len(codes[0])):
        row = []
        for j in range(len(codes)):
            row.append(codes[j][i])
        out.append("".join(row))

    return out


def to_decimal(binary_string):
    return int(binary_string, 2)


def get_gamma_epsilon(transposed):
    gamma, epsilon = [], []
    for row in transposed:
        zeros = len(row.replace("1", ""))
        ones = len(row) - zeros
        if zeros == ones:
            gamma.append("_")
            epsilon.append("_")
        else:
            gamma.append("0" if zeros > ones else "1")
            epsilon.append("1" if zeros > ones else "0")

    return "".join(gamma), "".join(epsilon)


def get_gamma_epsilon_decimal(transposed):
    gamma, epsilon = get_gamma_epsilon(transposed)

    return to_decimal(gamma), to_decimal(epsilon)


def filter_by_position(data, position, most_common):
    gamma, epsilon = get_gamma_epsilon(transpose(data))

    match_digit = gamma[position] if most_common else epsilon[position]
    if match_digit == "_":
        match_digit = "1" if most_common else "0"

    return [code for code in data if code[position] == match_digit]


def recurse_filter(data, most_common, position=0):
    if not data or len(data) == 1:
        return data

    max_index = len(data[0]) -1

    if position > max_index:
        return data

    new_data = filter_by_position(data, position, most_common)
    
    return recurse_filter(new_data, most_common, position +1)


def get_o2_gen_and_co2_scrub(data):
    o2_gen = to_decimal(recurse_filter(data, True)[0])
    co2_scrub = to_decimal(recurse_filter(data, False)[0])

    return o2_gen, co2_scrub


def answer_part_two(data):
    o2_gen, co2_scrub = get_o2_gen_and_co2_scrub(data)

    return o2_gen * co2_scrub


def answer_part_one(data):
    transposed = transpose(data)
    gamma, epsilon = get_gamma_epsilon(transposed)

    return to_decimal(gamma) * to_decimal(epsilon)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part 2", action="store_true", default=False)

    return parser.parse_args()


if __name__ == "__main__":
    options = opts()
    
    with open("three.txt") as fh:
        data = [line.strip() for line in fh]
        if options.part_one:
            answer = answer_part_one(data)
            print("Answer: %s" % answer)
            sys.exit(0)
        elif options.part_two:
            answer = answer_part_two(data)
            print("Answer: %s" % answer)
            sys.exit(0)


import unittest

class TestExampleData(unittest.TestCase):
    def test_transpose(self):
        data = ["01100", "11001", "10011"]
        transposed = transpose(data)
        self.assertEqual(transposed, ["011", "110", "100", "001", "011"])


    def test_gamma_epsilon(self):
        transposed = ["011", "110", "100", "001", "011"]
        gamma, epsilon = get_gamma_epsilon_decimal(transposed)
        self.assertEqual(gamma, 25)
        self.assertEqual(epsilon, 6)


    def test_filter_by_position_and_most_common(self):
        data = ["01100", "11001", "10011"]
        filtered = filter_by_position(data, 0, True)

        self.assertEqual(filtered, ["11001", "10011"])


    def test_filter_by_position_and_least_common(self):
        data = ["01100", "11001", "10011"]
        filtered = filter_by_position(data, 2, False)

        self.assertEqual(filtered, ["01100"])


    def test_filter_by_position_and_most_common_with_a_tie(self):
        data = ["01100", "11001", "10010", "00001"]
        filtered = filter_by_position(data, 4, True)

        self.assertEqual(filtered, ["11001", "00001"])


    def test_filter_by_position_and_least_common_with_a_tie(self):
        data = ["01100", "11001", "10010", "00001"]
        filtered = filter_by_position(data, 4, False)

        self.assertEqual(filtered, ["01100", "10010"])


    def test_recurse_filter_most_common(self):
        data = ["01100", "11001", "10011", "00000", "10001"]
        # ["11001", "10011", "10001"]
        # ["10011", "10001"]
        # ["10011", "10001"]
        # ["10011", "10001"]
        # ["10011"]
        filtered = recurse_filter(data, True)

        self.assertEqual(filtered, ["10011"])


    def test_recurse_filter_least_common(self):
        data = ["01100", "11001", "10011", "00000", "10001"]
        # ["01100", "11001", "10011", "00000", "10001"]
        # ["01100", "00000"]
        # ["00000"]
        # ["00000"]
        # ["00000"]
        filtered = recurse_filter(data, False)

        self.assertEqual(filtered, ["00000"])


    def test_answer_part_one(self):
        data = [
            "00100",
            "11110",
            "10110",
            "10111",
            "10101",
            "01111",
            "00111",
            "11100",
            "10000",
            "11001",
            "00010",
            "01010"
        ]
        answer = answer_part_one(data)
        self.assertEqual(answer, 198)


    def test_answer_part_two(self):
        data = [
            "00100",
            "11110",
            "10110",
            "10111",
            "10101",
            "01111",
            "00111",
            "11100",
            "10000",
            "11001",
            "00010",
            "01010"
        ]
        answer = answer_part_two(data)
        self.assertEqual(answer, 230)


unittest.main()
