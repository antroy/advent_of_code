#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys
from functools import reduce

def parse_step(line):
    patt = r"(on|off) x=(-?\d+)..(-?\d+),y=(-?\d+)..(-?\d+),z=(-?\d+)..(-?\d+)"
    
    m = re.match(patt, line)

    return (m.group(1) == "on",
            (int(m.group(2)), int(m.group(3))),
            (int(m.group(4)), int(m.group(5))),
            (int(m.group(6)), int(m.group(7)))
            )

def overlap(a, b):
    if a[0] <= b[0] <= a[1]:
        return (True, b[0])

    if a[0] <= b[0] <= a[1]:
        return (True, b[0])


def smash_cuboids(a, b):
    ax, ay, az = a
    bx, by, bz = b

def size_of(cuboid):
    dimensions = [x[1] - x[0] + 1 for x in cuboid]
    return reduce(lambda x, y: x * y, dimensions)

class ReactorB:
    def __init__(self, data):
        self.steps = [parse_step(line) for line in data]
        self.cuboids = []

    def add_cuboid(self, cuboid):
        self.cuboids.append(cuboid)

    def size(self):
        return sum([size_of(c) for c in self.cuboids])


class Reactor:
    def __init__(self, data):
        self.steps = [parse_step(line) for line in data]
        self.cuboids = set()

    def range(self, start, end, initialization):
        if initialization:
            start = max(-50, start)
            end = min(50, end)
        return range(start, end + 1)

    def execute_step(self, step, initialization=True):
        turn_on, xrange, yrange, zrange = step
        for x in self.range(*xrange, initialization):
            for y in self.range(*yrange, initialization):
                for z in self.range(*zrange, initialization):
                    if turn_on:
                        self.cuboids.add((x,y,z))
                    else:
                        self.cuboids.discard((x,y,z))

    def execute_all(self, initialization=True):
        i = 0
        for step in self.steps:
            i += 1
            print(i)
            self.execute_step(step, initialization)

        
def answer_part_one(data):
    reactor = Reactor(data)
    reactor.execute_all()

    return len(reactor.cuboids)

def answer_part_two(data):
    reactor = Reactor(data)
    reactor.execute_all()

    return len(reactor.cuboids)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data(filename="twentytwo.txt"):
    with open(filename) as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest
TEST_DATA = [
    "on x=-20..26,y=-36..17,z=-47..7",
    "on x=-20..33,y=-21..23,z=-26..28",
    "on x=-22..28,y=-29..23,z=-38..16",
    "on x=-46..7,y=-6..46,z=-50..-1",
    "on x=-49..1,y=-3..46,z=-24..28",
    "on x=2..47,y=-22..22,z=-23..27",
    "on x=-27..23,y=-28..26,z=-21..29",
    "on x=-39..5,y=-6..47,z=-3..44",
    "on x=-30..21,y=-8..43,z=-13..34",
    "on x=-22..26,y=-27..20,z=-29..19",
    "off x=-48..-32,y=26..41,z=-47..-37",
    "on x=-12..35,y=6..50,z=-50..-2",
    "off x=-48..-32,y=-32..-16,z=-15..-5",
    "on x=-18..26,y=-33..15,z=-7..46",
    "off x=-40..-22,y=-38..-28,z=23..41",
    "on x=-16..35,y=-41..10,z=-47..6",
    "off x=-32..-23,y=11..30,z=-14..3",
    "on x=-49..-5,y=-3..45,z=-29..18",
    "off x=18..30,y=-20..-8,z=-3..13",
    "on x=-41..9,y=-7..43,z=-33..15",
    "on x=-54112..-39298,y=-85059..-49293,z=-27449..7877",
    "on x=967..23432,y=45373..81175,z=27513..53682",
]


class TestReactorB(unittest.TestCase):
    def test_add_cuboid(self):
        one = ((10,12), (10,12),(10,12))
        r = ReactorB([])
        r.add_cuboid(one)

        self.assertEqual(len(r.cuboids), 1)
        self.assertEqual(r.size(), 27)

        two = ((11,13), (11,13),(11,13))
        r.add_cuboid(two)

        self.assertEqual(r.size(), 27 + 19)
        self.assertEqual(len(r.cuboids), 1)

class TestReactor(unittest.TestCase):
    def test_execute_step(self):
        data = [
            "on x=10..12,y=10..12,z=10..12",
            "on x=11..13,y=11..13,z=11..13",
            "off x=9..11,y=9..11,z=9..11",
            "on x=10..10,y=10..10,z=10..10",
        ]
        r = Reactor(data)

        r.execute_step(r.steps[0])
        self.assertEqual(len(r.cuboids), 27)

        r.execute_step(r.steps[1])
        self.assertEqual(len(r.cuboids), 27 + 19)

        r.execute_step(r.steps[2])
        self.assertEqual(len(r.cuboids), 27 + 19 - 8)

        r.execute_step(r.steps[3])
        self.assertEqual(len(r.cuboids), 39)

    def test_execute_all(self):
        data = [
            "on x=10..12,y=10..12,z=10..12",
            "on x=11..13,y=11..13,z=11..13",
            "off x=9..11,y=9..11,z=9..11",
            "on x=10..10,y=10..10,z=10..10",
        ]
        r = Reactor(data)

        r.execute_all()
        self.assertEqual(len(r.cuboids), 39)

class TestExampleData(unittest.TestCase):
    def test_size_of(self):
        one = ((10,12), (10,12), (10,12))
        self.assertEqual(size_of(one), 27)

    def test_parse_step(self):
        line = TEST_DATA[0]
        step = parse_step(line)

        self.assertEqual(step, (True, (-20, 26), (-36, 17), (-47, 7)))

    def xtest_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 590784)

    def xtest_answer_part_two_small_region(self):
        answer = answer_part_one(get_data("twentytwo_b.txt"))
        self.assertEqual(answer, 474140)

    def xtest_answer_part_two(self):
        answer = answer_part_two(get_data("twentytwo_b.txt"))
        self.assertEqual(answer, 2758514936282235)

unittest.main()
