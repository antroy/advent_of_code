#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys


def parse_line(line):
    command_expr = r"(forward|up|down) (\d+)"
    m = re.search(command_expr, line)
    if not m:
        raise Exception("Line: %s does not match the regex")
    cmd, pos = m.groups()

    return (cmd, int(pos))


def depth_by_position(data):
    commands_with_count = [parse_line(l) for l in data]
    
    aim, depth, position = 0, 0, 0
    for command, n in commands_with_count:
        if command == "forward":
            position += n
            depth += (aim * n)
        elif command == "down":
            aim += n
        elif command == "up":
            aim -= n
            
    return depth * position


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--test", help="Run tests", action="store_true", default=False)

    return parser.parse_args()


if __name__ == "__main__":
    options = opts()
    
    if not options.test:
        with open("two.txt") as fh:
            data = [line.strip() for line in fh]
        answer = depth_by_position(data)
        print("Answer: %s" % answer)
        sys.exit(0)

sys.argv[1:] = []

import unittest

class TestExampleData(unittest.TestCase):
    def test_sample(self):
        data = [
            "forward 5", 
            "down 5", 
            "forward 8", 
            "up 3", 
            "down 8", 
            "forward 2"
        ]
        answer = depth_by_position(data)
        self.assertEquals(answer, 900)

unittest.main()
