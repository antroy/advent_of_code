#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, functools, sys


class Cave:
    def __init__(self, data):
        self.rows = [[int(number) for number in line] for line in data]
        _columns = [*zip(*self.rows)]

    def in_grid(self, point):
        max_x = len(self.rows[0])
        max_y = len(self.rows)

        if point[0] < 0:
            return False
        if point[0] >= max_x:
            return False
        if point[1] < 0:
            return False
        if point[1] >= max_y:
            return False
        return True

    def _value(self, point):
        return self.rows[point[1]][point[0]]

    def is_above(self, point, reference):
        point_value = self._value(point)
        reference_value = self._value(reference)

        return point_value > reference_value
 
    def risk(self, point):
        return self._value(point) + 1

    def _neighbours(self, point):
        x, y = point
        above = (x, y-1)
        left = (x-1, y)
        below = (x, y+1)
        right = (x+1, y)
        neighbours = [above, left, right, below]

        return [neighbour for neighbour in neighbours if self.in_grid(neighbour)]

    def is_low_point(self, point):
        out = [self.is_above(neighbour, point) for neighbour in self._neighbours(point)]

        return all(out)

    def _points(self):
        return [(x, y) for y in range(len(self.rows)) for x in range(len(self.rows[0]))]

    def risk_level(self):
        return sum([self.risk(p) for p in self._points() if self.is_low_point(p)])

    def basin_for_point(self, basins, point):
        for basin in basins:
            if point in basin:
                return basin
        return None

    def _basins_containing_points(self, basins, points):
        basins_containing_neighbours = []
        for n in points:
            basin = self.basin_for_point(basins, n)
            if basin and basin not in basins_containing_neighbours:
                basins_containing_neighbours.append(basin)
        return basins_containing_neighbours

    def basins(self):
        points = self._points()

        basins = []

        for point in points:
            if self._value(point) == 9:
                continue
            basins_containing_neighbours = self._basins_containing_points(basins, self._neighbours(point))

            if basins_containing_neighbours:
                if len(basins_containing_neighbours) == 1:
                    basins_containing_neighbours[0].append(point)
                else:
                    joined_basins = [pt for basin in basins_containing_neighbours for pt in basin]
                    joined_basins.append(point)
                    basins.append(joined_basins)
                    for basin in basins_containing_neighbours:
                        basins.remove(basin)

            if not self.basin_for_point(basins, point):
                basins.append([point])

        return [sorted(basin) for basin in basins]

    def top_three_basins(self):
        basins = self.basins()
        sorted_by_size = list(reversed(sorted(basins, key=lambda x: len(x))))

        return sorted_by_size[0:3]


def answer_part_one(data):
    cave = Cave(data)
    return cave.risk_level()


def answer_part_two(data):
    cave = Cave(data)
    top3 = cave.top_three_basins()
    sizes = [len(b) for b in top3]
    return functools.reduce(lambda x, y: x * y, sizes)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("nine.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = [
    "2199943210",
    "3987894921",
    "9856789892",
    "8767896789",
    "9899965678"
]

class TestCave(unittest.TestCase):
    def test_init(self):
        cave = Cave(TEST_DATA)

        self.assertEqual(cave.rows[1], [3,9,8,7,8,9,4,9,2,1])
        self.assertEqual(cave.rows[4], [9,8,9,9,9,6,5,6,7,8])

    def test_is_low_point(self):
        cave = Cave(TEST_DATA)

        self.assertTrue(cave.is_low_point((1, 0)))
        self.assertTrue(cave.is_low_point((9, 0)))
        self.assertTrue(cave.is_low_point((2, 2)))
        self.assertFalse(cave.is_low_point((2, 4)))
        self.assertFalse(cave.is_low_point((6, 3)))

    def test_in_grid(self):
        cave = Cave(TEST_DATA)

        self.assertTrue(cave.in_grid((0, 0)))
        self.assertTrue(cave.in_grid((0, 4)))
        self.assertTrue(cave.in_grid((9, 0)))
        self.assertTrue(cave.in_grid((9, 4)))
        self.assertTrue(cave.in_grid((5, 2)))
        self.assertFalse(cave.in_grid((-1, 2)))
        self.assertFalse(cave.in_grid((6, -1)))
        self.assertFalse(cave.in_grid((10, 2)))
        self.assertFalse(cave.in_grid((6, 5)))

    def test_is_above(self):
        cave = Cave(TEST_DATA)

        self.assertTrue(cave.is_above((0, 0), (1, 0)))
        self.assertTrue(cave.is_above((0, 3), (4, 2)))
        self.assertFalse(cave.is_above((0, 0), (1, 2)))
        self.assertFalse(cave.is_above((5, 4), (5, 3)))

    def test_risk_level(self):
        cave = Cave(TEST_DATA)

        self.assertEqual(cave.risk_level(), 15)

    def test_basins_for_point(self):
        cave = Cave(TEST_DATA)

        self.assertEqual(cave.basin_for_point([], (0, 0)), None)
        self.assertEqual(cave.basin_for_point([[(0,0),(0,1)]], (0, 0)), [(0,0),(0,1)])
        self.assertEqual(cave.basin_for_point([[(0,0),(0,1)]], (2, 0)), None)
        self.assertEqual(cave.basin_for_point([[(0,0),(0,1)], [(2,1),(2,3)]], (2, 3)), [(2,1),(2,3)])


    def test_basins(self):
        cave = Cave(TEST_DATA)

        basins = cave.basins()

        self.assertTrue([(0,0), (0,1), (1,0)] in basins)
        self.assertTrue([(5,0), (6,0), (6,1), (7,0), (8,0), (8,1), (9,0), (9,1), (9,2)] in basins)

        self.assertEqual(sorted([len(basin) for basin in basins]), [3, 9, 9, 14])

class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 15)


    def test_answer_part_two(self):
        answer = answer_part_two(TEST_DATA)
        self.assertEqual(answer, 1134)

unittest.main()
