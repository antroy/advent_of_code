#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys

class Probe:
    def __init__(self, xvel, yvel, x=0, y=0):
        self.x = x
        self.y = y
        self.high_point = y
        self.xvel = xvel
        self.yvel = yvel

    def step(self):
        self.x = self.x + self.xvel
        self.y = self.y + self.yvel
        self.high_point = max(self.y, self.high_point)
        if self.xvel > 0:
            self.xvel -= 1
        self.yvel -= 1

    def is_in_grid(self, x_range, y_range):
        xmin, xmax = x_range
        ymin, ymax = y_range
        return (xmin <= self.x <= xmax) and (ymin <= self.y <= ymax)

    def is_beyond_grid(self, x_range, y_range):
        xmin, xmax = x_range
        ymin, ymax = y_range

        if self.x > xmax:
            return True

        if self.yvel < 0 and self.y < ymin:
            return True

        return False

    def enters_grid(self, x_range, y_range):
        while not self.is_in_grid(x_range, y_range):
            if self.is_beyond_grid(x_range, y_range):
                return False
            self.step()

        return True

def discover_high_point(x_range, y_range):
    high = 0

    for xvel in range(1, 100):
        for yvel in range(1, 100):
            probe = Probe(xvel, yvel)
            if probe.enters_grid(x_range, y_range):
                high = max(high, probe.high_point)

    return high

def count_distinct_initial_velocities(x_range, y_range):
    initial = []

    for xvel in range(1, 1000):
        for yvel in range(-1000, 1000):
            probe = Probe(xvel, yvel)
            if probe.enters_grid(x_range, y_range):
                initial.append((xvel, yvel))

    return len(initial)

def bounds(data):
    pattern = r"target area: x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)"
    xmin, xmax, ymin, ymax = map(int, re.match(pattern, data[0]).groups())

    return (xmin, xmax), (ymin, ymax)

def answer_part_one(data):
    xbounds, ybounds = bounds(data)

    return discover_high_point(xbounds, ybounds)


def answer_part_two(data):
    xbounds, ybounds = bounds(data)

    return count_distinct_initial_velocities(xbounds, ybounds)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("seventeen.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

class TestProbe(unittest.TestCase):
    def test_step(self):
        p = Probe(2, 2)

        p.step()

        self.assertEqual(p.x, 2)
        self.assertEqual(p.y, 2)
        self.assertEqual(p.xvel, 1)
        self.assertEqual(p.yvel, 1)
        
        p.step()

        self.assertEqual(p.x, 3)
        self.assertEqual(p.y, 3)
        self.assertEqual(p.xvel, 0)
        self.assertEqual(p.yvel, 0)
        
        p.step()

        self.assertEqual(p.x, 3)
        self.assertEqual(p.y, 3)
        self.assertEqual(p.xvel, 0)
        self.assertEqual(p.yvel, -1)
        
        p.step()

        self.assertEqual(p.x, 3)
        self.assertEqual(p.y, 2)
        self.assertEqual(p.xvel, 0)
        self.assertEqual(p.yvel, -2)

    def test_is_in_grid(self):
        p = Probe(0, 0)

        self.assertFalse(p.is_in_grid((10, 20), (10, 15)))
        self.assertTrue(p.is_in_grid((-10, 20), (-10, 15)))

        p = Probe(0, 0, 23, -12)
        self.assertFalse(p.is_in_grid((10, 20), (10, 15)))
        self.assertTrue(p.is_in_grid((10, 30), (-20, 0)))

    def test_is_beyond_grid(self):
        p = Probe(0, 0, 10, 16)

        self.assertFalse(p.is_beyond_grid((10, 20), (10, 15)))
        self.assertFalse(p.is_beyond_grid((-10, 20), (-10, 15)))

        p = Probe(0, -1, 23, -12)
        self.assertTrue(p.is_beyond_grid((10, 20), (10, 15)))
        self.assertTrue(p.is_beyond_grid((10, 30), (-10, 20)))
        self.assertFalse(p.is_beyond_grid((10, 30), (-12, 20)))

    def test_enters_grid(self):
        x_range = (20, 30)
        y_range = (-10, -5)

        self.assertTrue(Probe(7, 2).enters_grid(x_range, y_range))
        self.assertTrue(Probe(6, 3).enters_grid(x_range, y_range))
        self.assertTrue(Probe(9, 0).enters_grid(x_range, y_range))
        self.assertFalse(Probe(17, -4).enters_grid(x_range, y_range))


        probe = Probe(6, 9)
        enters = probe.enters_grid(x_range, y_range)

        self.assertTrue(enters)
        self.assertEqual(probe.high_point, 45)

    def test_discover_high_point(self):
        x_range = (20, 30)
        y_range = (-10, -5)

        hp = discover_high_point(x_range, y_range)

        self.assertEqual(hp, 45)



class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        data = [
            "target area: x=20..30, y=-10..-5"
        ]
        answer = answer_part_one(data)
        self.assertEqual(answer, 45)


    def test_answer_part_two(self):
        data = [
            "target area: x=20..30, y=-10..-5"
        ]
        answer = answer_part_two(data)
        self.assertEqual(answer, 112)

unittest.main()
