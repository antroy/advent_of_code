#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys
from collections import Counter

class DetDie:
    def __init__(self, start=0):
        self.roll_number = start

    def roll(self):
        self.roll_number += 1
        return ((self.roll_number - 1) % 100) + 1


class Game:
    def __init__(self, player_one_start, player_two_start, die):
        self.players = {
            1: {'pos': player_one_start, 'score': 0},
            2: {'pos': player_two_start, 'score': 0}
        }

        self.die = die

        self.next_player = 1

    def move(self, player_no, spaces):
        player = self.players[player_no]
        position = player['pos'] + spaces
        player['pos'] = ((position - 1) % 10) + 1
        player['score'] = player['score'] + player['pos']

        #  print(" and moves to space %s for a total %s" % (player['pos'], player['score']))

        self.players[player_no] =  player


    def play(self):
        while True:
            if self.players[1]['score'] >= 1000:
                return 1, 2
            if self.players[2]['score'] >= 1000:
                return 2, 1

            rolls = [self.die.roll() for i in range(3)]
            roll_total = sum(rolls)
            #  print("Player %s rolls %s" % (self.next_player, "+".join(map(str,rolls))), end="")
            self.move(self.next_player, roll_total)
            self.next_player = 2 if self.next_player == 1 else 1

possible_triple_rolls = [(i, j, k) for i in [1,2,3] for j in [1,2,3] for k in [1,2,3]]
possible_totals = [sum(roll) for roll in possible_triple_rolls]
DIRAC_ROLLS = Counter(possible_totals)

def dirac_rolls():
    return dict(DIRAC_ROLLS)

class GameDirac:
    def __init__(self, p1, p2, turn=1):
        self.p1 = p1
        self.p2 = p2
        self.turn = turn
        self.max_score = 21

    def move(self, spaces):
        player1 = dict(self.p1)
        player2 = dict(self.p2)
        player = player1 if self.turn == 1 else player2
        position = player['pos'] + spaces
        player['pos'] = ((position - 1) % 10) + 1
        player['score'] = player['score'] + player['pos']
        next_player = 2 if self.turn == 1 else 1

        return GameDirac(player1, player2, next_player)

    def winners_in_all_universes(self, depth=0):
        #  print("[%s] Scores: %s, %s" % (depth, self.p1['score'], self.p2['score']))
        if self.p1['score'] >= self.max_score:
            return (1, 0)
        if self.p2['score'] >= self.max_score:
            return (0, 1)

        p1_wins_out = 0
        p2_wins_out = 0
        rolls = dirac_rolls()

        for roll in rolls:
            next_game = self.move(roll)
            p1_wins, p2_wins = next_game.winners_in_all_universes(depth + 1)
            freq = rolls[roll]
            p1_wins_out += p1_wins * freq
            p2_wins_out += p2_wins * freq

        return (p1_wins_out, p2_wins_out)


def starting_positions(data):
    start = {int(m.group(1)): int(m.group(2)) for m in [re.match(r"Player (1|2) starting position: (\d)", line) for line in data]}
    return start[1], start[2]

def answer_part_one(data):
    one, two = starting_positions(data)
    game = Game(one, two, DetDie())
    winner, loser = game.play()

    die_rolls = game.die.roll_number
    loser_score = game.players[loser]['score']

    return die_rolls * loser_score

def answer_part_two(data):
    one, two = starting_positions(data)
    p1 = {'pos': one, 'score': 0, 'wins': 0}
    p2 = {'pos': two, 'score': 0, 'wins': 0}
    game = GameDirac(p1, p2)

    return game.winners_in_all_universes()

def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("twentyone.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: P1: %s; P2: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = ["Player 1 starting position: 4", "Player 2 starting position: 8"]

class TestGame(unittest.TestCase):
    def test_move(self):
        game = Game(2, 5, DetDie())

        game.move(1, 7)

        self.assertEqual(game.players[1]['pos'], 9)
        self.assertEqual(game.players[1]['score'], 9)

        game.move(2, 7)

        self.assertEqual(game.players[2]['pos'], 2)
        self.assertEqual(game.players[2]['score'], 2)

        game.move(1, 5)

        self.assertEqual(game.players[1]['pos'], 4)
        self.assertEqual(game.players[1]['score'], 13)


    def test_play(self):
        print("Game Play")
        game = Game(4, 8, DetDie())

        winner, loser = game.play()

        self.assertEqual(winner, 1)
        self.assertEqual(loser, 2)
        self.assertEqual(game.players[2]['score'], 745)
        self.assertEqual(game.die.roll_number, 993)

class TestDetDie(unittest.TestCase):
    def test_rolls(self):
        die = DetDie()

        actual = [die.roll() for i in range(5)]

        self.assertEqual(actual, [1,2,3,4,5])
        self.assertEqual(die.roll_number, 5)


    def test_rolls_near_100(self):
        die = DetDie(98)

        actual = [die.roll() for i in range(5)]

        self.assertEqual(actual, [99, 100, 1, 2, 3])
        self.assertEqual(die.roll_number, 103)

class TestExampleData(unittest.TestCase):
    def test_starting_positions(self):
        one, two = starting_positions(TEST_DATA)

        self.assertEqual(one, 4)
        self.assertEqual(two, 8)

    def test_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 739785)


    def test_answer_part_two(self):
        answer = answer_part_two(TEST_DATA)
        self.assertEqual(answer, (444356092776315, 341960390180808))

unittest.main()
