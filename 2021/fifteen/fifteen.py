#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys
import heapq

class Node:
    def __init__(self, coords, risk):
        self.coords, self.risk = coords, risk
        self.complete = False
        self.via = None
        self.distance = sys.maxsize

    def __str__(self):
        return "%s: V: %s; D:%s" % (self.coords, self.via, self.distance)

class Edge:
    def __init__(self, end, weight):
        self.end, self.weight = end, weight

    def __str__(self):
        return "E: %s: W: %s;" % (self.end.coords, self.weight)

class Cave:
    def __init__(self, data):
        rows = [[int(number) for number in line] for line in data]
        self.max_x = len(rows[0]) - 1 if rows else 0
        self.max_y = len(rows) - 1
        self.nodes = {}
        for y in range(len(rows)):
            for x in range(len(rows[y])):
                self.nodes[(x, y)] = Node((x, y), rows[y][x])

        self.edges = {}
        for node in self.nodes.values():
            exits = []
            self.edges[node.coords] = exits
            for n in self.neighbours(node.coords):
                neighbour = self.nodes[n]
                exits.append(Edge(neighbour, neighbour.risk))

        self.start = self.nodes[(0,0)]
        self.start.distance = 0
        self.end = self.nodes[(self.max_x,self.max_y)]
        self.todo = [(0, (0, 0))] #[(self.nodes[n].distance, n) for n in self.node]
        print("Initialized")

    def print(self):
        print("NODES:")
        print( "\n  ".join([str(v) for v in self.nodes.values()]))
        print("EDGES:")
        for coord in self.edges:
            print("  %s: %s" % (coord, [str(e) for e in self.edges[coord]]))

    def cave_map(self):
        return ["".join([str(col) for col in row]) for row in self.rows]

    def __str__(self):
        return "\n".join(self.cave_map())

    def inside(self, location):
        if location[0] < 0:
            return False
        if location[0] > self.max_x:
            return False
        if location[1] < 0:
            return False
        if location[1] > self.max_y:
            return False
        return True

    def neighbours(self, location):
        x, y = location
        up = (x, y-1)
        left = (x-1, y)
        below = (x, y+1)
        right = (x+1, y)
        neighbours = [up, left, right, below]

        return [neighbour for neighbour in neighbours if self.inside(neighbour)]

    def update_neighbours(self, node):
        for e in self.edges[node.coords]:
            if not e.end.distance:
                e.end.distance = e.weight
                e.end.via = node.coords
            elif e.end.distance > node.distance + e.weight:
                e.end.distance = node.distance + e.weight
                e.end.via = node.coords
                heapq.heappush(self.todo, (e.end.distance, e.end.coords))

    def calc_shortest_path(self):
        """
        Implements Dijkstrika's Algorithm.
        Note the priority queue (heapq) is essential as without
        it sorting a list each loop is dibilitatingly slow...
        """
        while self.todo:
            risk, coords = heapq.heappop(self.todo)
            if coords == self.end.coords:
                break

            end = self.end

            self.update_neighbours(self.nodes[coords])

    def shortest_path(self):
        path = []
        node = self.end
        path = [node.coords]
        self.calc_shortest_path()

        while True:
            previous = self.nodes[node.via] if node.via else None

            path.insert(0, previous.coords)

            if node.via == (0, 0):
                break

            node = previous

        return path

    def value_of_path(self, path):
        return sum(self.nodes[coord].risk for coord in path[1:])

def inc(n, by):
    out = (n + by) % 9
    return out if out else 9


def increment_row_by(row, by):
    numbers = [inc(int(n), by) for n in row]
    return "".join(str(n) for n in numbers)

def map_times_five(data):
    go_wide = []
    for row in data:
        out_row = []
        for i in range(5):
            out_row.append(increment_row_by(row, i))
        go_wide.append("".join(out_row))

    temp_data = [go_wide] * 5
    go_deep = []

    for i in range(5):
        for row in temp_data[i]:
            go_deep.append(increment_row_by(row, i))

    return go_deep
        
def answer_part_one(data):
    cave = Cave(data)

    path = cave.shortest_path()

    return cave.value_of_path(path)


def answer_part_two(data):
    data = map_times_five(data)

    cave = Cave(data)

    path = cave.shortest_path()

    return cave.value_of_path(path)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("fifteen.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

SMALL_DATA = [
    "316",
    "241",
    "724",
]

class TestCave(unittest.TestCase):
    def test_init(self):
        cave = Cave(SMALL_DATA)
        self.assertEqual(cave.start.coords[0], 0)
        self.assertEqual(cave.start.coords[1], 0)
        self.assertEqual(cave.end.coords[0], 2)
        self.assertEqual(cave.end.coords[1], 2)

    def test_update_neighbours(self):
        cave = Cave(SMALL_DATA)

        self.assertEqual(cave.start.distance, 0)
        self.assertEqual(cave.end.distance, sys.maxsize)
        self.assertEqual(cave.nodes[(0,1)].distance, sys.maxsize)
        self.assertEqual(cave.nodes[(0,1)].via, None)
        self.assertEqual(cave.nodes[(1,0)].distance, sys.maxsize)
        self.assertEqual(cave.nodes[(1,0)].via, None)

        cave.update_neighbours(cave.start)
        
        self.assertEqual(cave.nodes[(0,1)].distance, 2)
        self.assertEqual(cave.nodes[(0,1)].via, (0,0))
        self.assertEqual(cave.nodes[(1,0)].distance, 1)
        self.assertEqual(cave.nodes[(1,0)].via, (0,0))
        

    def test_small_cave(self):
        cave = Cave(SMALL_DATA)
        path = cave.shortest_path()
        self.assertEqual(path, [(0,0),(1,0),(1,1),(2,1),(2,2)])
        self.assertEqual(cave.value_of_path(path), 10)

TEST_DATA = [
"1163751742",
"1381373672",
"2136511328",
"3694931569",
"7463417111",
"1319128137",
"1359912421",
"3125421639",
"1293138521",
"2311944581",
]

class TestExampleData(unittest.TestCase):
    def test_increment_row_by(self):
        row = "14589"

        self.assertEqual(increment_row_by(row, 0), "14589")
        self.assertEqual(increment_row_by(row, 2), "36712")
        self.assertEqual(increment_row_by(row, 3), "47823")
        self.assertEqual(increment_row_by(row, 5), "69145")

    def test_data_times_five(self):
        data = ["17","54"]

        exp = [
            "1728394152",
            "5465768798",
            "2839415263",
            "6576879819",
            "3941526374",
            "7687981921",
            "4152637485",
            "8798192132",
            "5263748596",
            "9819213243"
        ]

        self.assertEqual(map_times_five(data), exp)

    def test_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 40)


    def test_answer_part_two(self):
        answer = answer_part_two(TEST_DATA)
        self.assertEqual(answer, 315)

unittest.main()
