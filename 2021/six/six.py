#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys
from collections import defaultdict

def _data_as_array(data):
    out = []
    for line in data:
        out.extend([int(n) for n in line.split(",") if n])

    return out

def shoal_from_data(data):
    longhand_shoal = _data_as_array(data)
    out = defaultdict(lambda: 0)
    for k in longhand_shoal:
        out[k] += 1

    return out

def update_fish(fish):
    if fish == 0:
        return [6, 8]
    return [fish - 1]

def update_shoal(shoal):
    out = defaultdict(lambda: 0)

    for fish, count in shoal.items():
        for new_fish in update_fish(fish):
            out[new_fish] += count

    return dict(out)

def shoal_after_days(shoal, days):
    if days == 0:
        return shoal
    new_shoal = update_shoal(shoal)

    return shoal_after_days(new_shoal, days - 1)

def size(shoal):
    return sum(shoal.values())

def answer_part_one(data):
    shoal = shoal_after_days(shoal_from_data(data), 80)

    return size(shoal)


def answer_part_two(data):
    shoal = shoal_after_days(shoal_from_data(data), 256)

    return size(shoal)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data(filename):
    with open(filename) as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data("six.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data("six.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = "3,4,3,1,2"

class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 5934)

    def test_answer_part_two(self):
        answer = answer_part_two(TEST_DATA)
        self.assertEqual(answer, 26984457539)

    def test_update_fish(self):
        new_fish1 = update_fish(8)
        new_fish2 = update_fish(5)
        new_fish3 = update_fish(1)
        new_fish4 = update_fish(0)

        self.assertEqual(new_fish1, [7])
        self.assertEqual(new_fish2, [4])
        self.assertEqual(new_fish3, [0])
        self.assertEqual(new_fish4, [6, 8])


class TestFishPerf(unittest.TestCase):
    def test_parse_data(self):
        res = shoal_from_data(TEST_DATA)

        self.assertEqual(res, {1: 1, 2: 1, 3: 2, 4: 1})

    def test_update_shoal(self):
        shoal = shoal_from_data("0,1,0,5,6,7,8")
        new_shoal = update_shoal(shoal)

        self.assertEqual(new_shoal, {0: 1, 4: 1, 5: 1, 6: 3, 7: 1, 8: 2})

    def test_shoal_after_18_days(self):
        shoal = shoal_after_days(shoal_from_data(TEST_DATA), 18)

        self.assertEqual(shoal, {0: 3, 1: 5, 2: 3, 3: 2, 4: 2, 5: 1, 6: 5, 7: 1, 8: 4})

    def test_size(self):
        shoal = {1: 1, 2: 1, 3: 2, 4: 1}
        shoal2 = {0: 3, 1: 5, 2: 3, 3: 2, 4: 2, 5: 1, 6: 5, 7: 1, 8: 4}

        self.assertEqual(size(shoal), 5)
        self.assertEqual(size(shoal2), 26)
        



    

unittest.main()
