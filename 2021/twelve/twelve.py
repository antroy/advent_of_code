#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys
from collections import defaultdict, Counter

START_CAVE = 'start'
END_CAVE = 'end'

def paths_as_string_arr(paths):
    return [",".join(c.name for c in p) for p in paths]

def pp(hint, paths):
    print(hint)
    print("\n".join(paths_as_string_arr(paths)))

def times_visited(cave, path):
    return len([c for c in path if c.name == cave.name])

class Cave:
    def __init__(self, name):
        self.name = name
        self.exits = set()
        self.large = name.upper() == name

    def _is_big_or_unvisited(self, path):
        return self.large or times_visited(self, path) < 1

    def _is_terminus(self):
        return self.name in [START_CAVE, END_CAVE]

    def _there_have_already_been_2_visits_to_a_small_cave(self, path):
        small_cave_visit_counts = Counter([c.name for c in path if not c.large]).values()
        already_visited_twice = any([count for count in small_cave_visit_counts if count > 1])
        return already_visited_twice

    def visitable(self, path, allow_revisit):
        if self._is_big_or_unvisited(path):
            return True
        if self._is_terminus():
            return False
        if allow_revisit:
            return not self._there_have_already_been_2_visits_to_a_small_cave(path)

        return False


class CaveSystem:
    def __init__(self, data):
        self.caves = {}
        for line in data:
            start, end = line.strip().split("-")
            if start not in self.caves:
                self.caves[start] = Cave(start)
            if end not in self.caves:
                self.caves[end] = Cave(end)

            self.caves[start].exits.add(self.cave(end))
            self.caves[end].exits.add(self.cave(start))

    def cave(self, name):
        return self.caves[name]

    def _extend_paths(self, path, allow_revisit):
        last_cave = path[-1]
        visitable = [cave for cave in last_cave.exits if cave.visitable(path, allow_revisit)]

        if last_cave.name != END_CAVE:
            out = []
            for cave in visitable:
                out.append(list(path) + [cave])
            return out

        return [path]

    def paths(self, allow_revisit=False, paths=None):
        if not paths:
            paths = [[self.cave(START_CAVE)]]

        out = []

        for path in paths:
            out.extend(self._extend_paths(path, allow_revisit))

        if out == paths:
            return out

        return self.paths(allow_revisit, out)

def answer_part_one(data):
    caves = CaveSystem(data)
    paths = caves.paths()
    return len(paths)


def answer_part_two(data):
    caves = CaveSystem(data)
    paths = caves.paths(2)
    return len(paths)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("twelve.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

REALLY_SMALL_DATA = ["start-A", "start-b", "A-end", "b-end"]

SMALL_DATA = [
    "start-A",
    "start-b",
    "A-c",
    "A-b",
    "b-d",
    "A-end",
    "b-end"
]

BIGGER_DATA = [
"dc-end",
"HN-start",
"start-kj",
"dc-start",
"dc-HN",
"LN-dc",
"HN-end",
"kj-sa",
"kj-HN",
"kj-dc",
]

class TestCave(unittest.TestCase):
    def test_visitable_with_no_extra_visit(self):
        cave_a = Cave("a")
        cave_b = Cave("b")
        cave_big = Cave("BIG")

        path = [Cave("BIG"), Cave("BIG"), Cave("BIG"), Cave("a")]

        self.assertTrue(cave_big.visitable(path, False))
        self.assertFalse(cave_a.visitable(path, False))
        self.assertTrue(cave_b.visitable(path, False))

    def test_visitable_with_extra_visit(self):
        cave_a = Cave("a")
        cave_b = Cave("b")
        cave_c = Cave("c")
        cave_big = Cave("BIG")
        cave_start = Cave(START_CAVE)
        cave_end = Cave(END_CAVE)

        path = [Cave(START_CAVE), Cave("a"), Cave("BIG"), Cave("b"), Cave("BIG"), Cave("BIG"), Cave("a"), Cave(END_CAVE)]

        self.assertTrue(cave_big.visitable(path, True))
        self.assertFalse(cave_a.visitable(path, True))
        self.assertFalse(cave_b.visitable(path, True))
        self.assertTrue(cave_c.visitable(path, True))
        self.assertFalse(cave_start.visitable(path, True))
        self.assertFalse(cave_end.visitable(path, True))

        path2 = [Cave(START_CAVE), Cave(END_CAVE), Cave("BIG"), Cave("b"), Cave("BIG"), Cave("BIG"), Cave("a")]

        self.assertTrue(cave_big.visitable(path2, True))
        self.assertTrue(cave_a.visitable(path2, True))
        self.assertTrue(cave_b.visitable(path2, True))
        self.assertTrue(cave_c.visitable(path2, True))
        self.assertFalse(cave_start.visitable(path2, True))
        self.assertFalse(cave_end.visitable(path2, True))

class TestCaveSystem(unittest.TestCase):
    def test_init(self):
        caves = CaveSystem(SMALL_DATA)

        self.assertEqual(len(caves.caves), 6)

    def test_cave(self):
        caves = CaveSystem(SMALL_DATA)

        self.assertEqual(len(caves.cave('A').exits), 4)
        self.assertEqual(len(caves.cave('start').exits), 2)
        self.assertEqual(len(caves.cave('b').exits), 4)
        self.assertEqual(len(caves.cave('c').exits), 1)

    def test_paths_really_small(self):
        caves = CaveSystem(REALLY_SMALL_DATA)

        paths = caves.paths()
        
        self.assertEqual(len(paths), 2)
        paths_as_str = paths_as_string_arr(paths)
        self.assertTrue("start,A,end" in paths_as_str)
        self.assertTrue("start,b,end" in paths_as_str)
        
    def test_paths_small(self):
        caves = CaveSystem(SMALL_DATA)

        paths = caves.paths()
        
        self.assertEqual(len(paths), 10)
        paths_as_str = paths_as_string_arr(paths)
        self.assertTrue("start,A,end" in paths_as_str)
        self.assertTrue("start,b,end" in paths_as_str)
        self.assertTrue("start,A,b,A,c,A,end" in paths_as_str)
        self.assertTrue("start,A,c,A,b,end" in paths_as_str)
        self.assertTrue("start,b,A,c,A,end" in paths_as_str)
        
    def test_paths_bigger(self):
        caves = CaveSystem(BIGGER_DATA)

        paths = caves.paths()
        
        self.assertEqual(len(paths), 19)
        paths_as_str = paths_as_string_arr(paths)
        self.assertTrue("start,HN,end" in paths_as_str)
        self.assertTrue("start,dc,end" in paths_as_str)
        self.assertTrue("start,HN,kj,HN,dc,HN,end" in paths_as_str)
        self.assertTrue("start,kj,HN,dc,HN,end" in paths_as_str)
        self.assertTrue("start,HN,dc,HN,kj,HN,end" in paths_as_str)

EXAMPLE_DATA = [
"fs-end",
"he-DX",
"fs-he",
"start-DX",
"pj-DX",
"end-zg",
"zg-sl",
"zg-pj",
"pj-he",
"RW-he",
"fs-DX",
"pj-RW",
"zg-RW",
"start-pj",
"he-WI",
"zg-he",
"pj-fs",
"start-RW",
]

class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        answer = answer_part_one(EXAMPLE_DATA)
        self.assertEqual(answer, 226)


    def test_answer_part_two(self):
        answer = answer_part_two(SMALL_DATA)
        self.assertEqual(answer, 36)

    def test_times_visited(self):
        cave_a = Cave("a")
        cave_b = Cave("b")
        cave_c = Cave("c")
        path = [Cave("a"), Cave("b"), Cave("a")]

        res_a = times_visited(cave_a, path)
        res_b = times_visited(cave_b, path)
        res_c = times_visited(cave_c, path)

        self.assertEqual(res_a, 2)
        self.assertEqual(res_b, 1)
        self.assertEqual(res_c, 0)

unittest.main()
