#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys

OPENER_MAP = {')': '(', '}': '{', ']': '[', '>': '<'}
CLOSER_MAP = {v: k for k, v in OPENER_MAP.items()}
CORRUPT_SCORE_MAP =  {')': 3, '}': 1197, ']': 57, '>': 25137}
INCOMPLETE_SCORE_MAP =  {')': 1, ']': 2, '}': 3, '>': 4}

CORRUPT = "CORRUPT"
INCOMPLETE = "INCOMPLETE"
OK = "OK"

class Result:
    def __init__(self, status, data):
        self.status = status
        self.data = data

def result_for_corrupt(illegal_char):
    return Result(CORRUPT, {'score': CORRUPT_SCORE_MAP[illegal_char], 'illegal_char': illegal_char})

def score_for_closers(closers, current_score=0):
    if not closers:
        return current_score
    
    new_score = current_score * 5 + INCOMPLETE_SCORE_MAP[closers[0]]

    return score_for_closers(closers[1:], new_score)

def result_for_incomplete(openers):
    closers = [CLOSER_MAP[char] for char in reversed(openers)]

    return Result(INCOMPLETE, {'missing_closers': "".join(closers), 'score': score_for_closers(closers)})

def check_line(line):
    stack = []

    for char in line:
        is_opener = char not in OPENER_MAP
        if is_opener:
            stack.append(char)
        else:
            opener = OPENER_MAP[char]
            last_char = stack.pop()

            is_corrupt = last_char != opener

            if is_corrupt:
                return result_for_corrupt(char)

    line_valid = not stack

    if line_valid:
        return Result(OK, {})

    #  closers = [CLOSER_MAP[char] for char in reversed(stack)]
#
    #  return Result(INCOMPLETE, {'missing_closers': "".join(closers)})
    return result_for_incomplete(stack)
    

def answer_part_one(data):
    results = [check_line(res) for res in data]
    corrupt = [res for res in results if res.status == CORRUPT]
    points = [res.data['score'] for res in corrupt]

    return sum(points)


def answer_part_two(data):
    results = [check_line(res) for res in data]
    incomplete = [res for res in results if res.status == INCOMPLETE]
    points = sorted([res.data['score'] for res in incomplete])

    print(points)

    return points[len(points) // 2]


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("ten.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = [
    "[({(<(())[]>[[{[]{<()<>>",
    "[(()[<>])]({[<{<<[]>>(",
    "{([(<{}[<>[]}>{[]{[(<()>",
    "(((({<>}<{<{<>}{[]{[]{}",
    "[[<[([]))<([[{}[[()]]]",
    "[{[{({}]{}}([{[{{{}}([]",
    "{<[[]]>}<{[{[{[]{()[[[]",
    "[<(<(<(<{}))><([]([]()",
    "<{([([[(<>()){}]>(<<{{",
    "<{([{{}}[<[[[<>{}]]]>[]]"
]

class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 26397)

    def test_answer_part_two(self):
        answer = answer_part_two(TEST_DATA)
        self.assertEqual(answer, 288957)

    def test_check_line(self):
        line = "{([(<{}[<>[]}>{[]{[(<()>"
        result = check_line(line)
        self.assertEqual(result.status, CORRUPT)
        self.assertEqual(result.data['illegal_char'], '}')
        self.assertEqual(result.data['score'], 1197)

        line = "[[<[([]))<([[{}[[()]]]"
        result = check_line(line)
        self.assertEqual(result.status, CORRUPT)
        self.assertEqual(result.data['illegal_char'], ')')
        self.assertEqual(result.data['score'], 3)

        line = "[{[{({}]{}}([{[{{{}}([]"
        result = check_line(line)
        self.assertEqual(result.status, CORRUPT)
        self.assertEqual(result.data['illegal_char'], ']')
        self.assertEqual(result.data['score'], 57)

        line = "[<(<(<(<{}))><([]([]()"
        result = check_line(line)
        self.assertEqual(result.status, CORRUPT)
        self.assertEqual(result.data['illegal_char'], ')')
        self.assertEqual(result.data['score'], 3)

        line = "<{([([[(<>()){}]>(<<{{"
        result = check_line(line)
        self.assertEqual(result.status, CORRUPT)
        self.assertEqual(result.data['illegal_char'], '>')
        self.assertEqual(result.data['score'], 25137)

    def test_completions(self):
        line = "[({(<(())[]>[[{[]{<()<>>"
        result = check_line(line)
        self.assertEqual(result.status, INCOMPLETE)
        self.assertEqual(result.data['missing_closers'], '}}]])})]')
        self.assertEqual(result.data['score'], 288957)
        
        line = "[(()[<>])]({[<{<<[]>>("
        result = check_line(line)
        self.assertEqual(result.status, INCOMPLETE)
        self.assertEqual(result.data['missing_closers'], ')}>]})')
        self.assertEqual(result.data['score'], 5566)
        
        line = "(((({<>}<{<{<>}{[]{[]{}"
        result = check_line(line)
        self.assertEqual(result.status, INCOMPLETE)
        self.assertEqual(result.data['missing_closers'], '}}>}>))))')
        self.assertEqual(result.data['score'], 1480781)
        
        line = "{<[[]]>}<{[{[{[]{()[[[]"
        result = check_line(line)
        self.assertEqual(result.status, INCOMPLETE)
        self.assertEqual(result.data['missing_closers'], ']]}}]}]}>')
        self.assertEqual(result.data['score'], 995444)

        line = "<{([{{}}[<[[[<>{}]]]>[]]"
        result = check_line(line)
        self.assertEqual(result.status, INCOMPLETE)
        self.assertEqual(result.data['missing_closers'], '])}>')
        self.assertEqual(result.data['score'], 294)

    def test_score_for_closers(self):
        closers = "}}]])})]"
        score = score_for_closers(closers)
        self.assertEqual(score, 288957)

        closers = ")}>]})"
        score = score_for_closers(closers)
        self.assertEqual(score, 5566)

        closers = "}}>}>))))"
        score = score_for_closers(closers)
        self.assertEqual(score, 1480781)
        
        closers = "]]}}]}]}>"
        score = score_for_closers(closers)
        self.assertEqual(score, 995444)

        closers = "])}>"
        score = score_for_closers(closers)
        self.assertEqual(score, 294)

unittest.main()
