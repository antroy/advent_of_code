#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, math, sys
from collections import defaultdict

def fuel_for_position(crabs, posn):
    fuel_per_crab = [abs(crab - posn) for crab in crabs]
    return sum(fuel_per_crab)

MOVE_DICT = {}

def cost(number_of_moves):
    if number_of_moves in MOVE_DICT:
        return MOVE_DICT[number_of_moves]
    out = 0
    for i in range(1, number_of_moves + 1):
        out += i

    MOVE_DICT[number_of_moves] = out
    return out

def fuel_for_position_revised(crabs, posn):
    fuel_per_crab = [cost(abs(crab - posn)) for crab in crabs]
    return sum(fuel_per_crab)

def answer_part_one(data):
    crabs = sorted([int(n) for n in data[0].split(',')])

    fuel_for_each_posn = [(posn, fuel_for_position(crabs, posn)) for posn in range(crabs[0], crabs[-1])]

    sorted_by_fuel = sorted(fuel_for_each_posn, key = lambda x: x[1])

    return sorted_by_fuel[0][1]

def answer_part_two(data):
    crabs = sorted([int(n) for n in data[0].split(',')])

    fuel_for_each_posn = [(posn, fuel_for_position_revised(crabs, posn)) for posn in range(crabs[0], crabs[-1])]

    sorted_by_fuel = sorted(fuel_for_each_posn, key = lambda x: x[1])

    return sorted_by_fuel[0][1]


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data(filename):
    with open(filename) as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data("seven.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data("seven.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = [16,1,2,0,4,2,7,1,2,14]

class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        fuel = answer_part_one(["16,1,2,0,4,2,7,1,2,14"])
        self.assertEqual(fuel, 37)

    def test_answer_part_two(self):
        fuel = answer_part_two(["16,1,2,0,4,2,7,1,2,14"])
        self.assertEqual(fuel, 168)

    def test_fuel_for_posn(self):
        fuel1 = fuel_for_position(TEST_DATA, 2)
        self.assertEqual(fuel1, 37)

        fuel2 = fuel_for_position(TEST_DATA, 1)
        self.assertEqual(fuel2, 41)

        fuel3 = fuel_for_position(TEST_DATA, 10)
        self.assertEqual(fuel3, 71)

    def test_fuel_for_posn_revised(self):
        fuel1 = fuel_for_position_revised(TEST_DATA, 2)
        self.assertEqual(fuel1, 206)

        fuel2 = fuel_for_position_revised(TEST_DATA, 5)
        self.assertEqual(fuel2, 168)

unittest.main()
