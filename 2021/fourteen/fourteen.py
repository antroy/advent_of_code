#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys
from collections import Counter

class Polymer:
    def __init__(self, data):
        self.original = list(data[0])
        self.rules = {}
        for line in data[2:]:
            m = re.match(r"([A-Z])([A-Z]) -> ([A-Z])", line)
            if m:
                a, b, ins = m.group(1), m.group(2), m.group(3)
                self.rules[a + b] = [a + ins, ins + b]
        self.first = self.original[0]
        self.last = self.original[-1]

        self.pairs = Counter([self.original[n] + self.original[n+1] for n in range(len(self.original) - 1)])

    def letter_count(self):
        counts = Counter([self.first, self.last])
        for pair, count in self.pairs.items():
            for letter in pair:
                counts[letter] += count

        sorted_counts = sorted(counts.values())

        smallest = sorted_counts[0] // 2
        largest = sorted_counts[-1] // 2

        return largest, smallest


    def process(self):
        new_pairs = Counter()
        for pair, count in self.pairs.items():
            #  import pdb; pdb.set_trace()
            next_pairs = self.rules[pair]
            for p in next_pairs:
                new_pairs[p] += count

        self.pairs = new_pairs

    def run_x_times_and_get_difference(self, x):
        for i in range(x):
            self.process()
        most_common, least_common = self.letter_count()
        return most_common - least_common


def answer_part_one(data):
    poly = Polymer(data)

    return poly.run_x_times_and_get_difference(10)


def answer_part_two(data):
    poly = Polymer(data)

    return poly.run_x_times_and_get_difference(40)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("fourteen.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = [
    "NNCB",
    "",
    "CH -> B",
    "HH -> N",
    "CB -> H",
    "NH -> C",
    "HB -> C",
    "HC -> B",
    "HN -> C",
    "NN -> C",
    "BH -> H",
    "NC -> B",
    "NB -> B",
    "BN -> B",
    "BB -> N",
    "BC -> B",
    "CC -> N",
    "CN -> C",
]

class TestPolymer(unittest.TestCase):
    def test_init(self):
        poly = Polymer(TEST_DATA)

        self.assertEquals(poly.first, "N")
        self.assertEquals(poly.last, "B")
        self.assertEquals(poly.pairs, {"NN": 1, "NC": 1, "CB": 1})

        poly.process()
        # NCNBCHB
        self.assertEquals(poly.pairs, {"NC": 1, "CN": 1, "NB": 1, "BC": 1, "CH": 1, "HB": 1})

        poly.process()
        # NBBBCNCCNBBNBNBBCHBHHBCHB
        self.assertEquals(poly.pairs["NB"], 2)
        self.assertEquals(poly.pairs["BC"], 2)
        self.assertEquals(poly.pairs["CC"], 1)
        self.assertEquals(poly.pairs["CN"], 1)


class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 1588)


    def test_answer_part_two(self):
        answer = answer_part_two(TEST_DATA)
        self.assertEqual(answer, 2188189693529)

unittest.main()
