#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys

class Grid:
    def __init__(self, data=[]):
        self.dots = set()
        self.folds = []
        coords_ended = False

        for line in data:
            if not line:
                coords_ended = True
                continue
            if coords_ended:
                m = re.match(r"fold along (x|y)=(\d+)", line)
                if m:
                    self.folds.append((m.group(1), int(m.group(2))))
                continue

            x, y = line.split(",")
            self.dots.add((int(x), int(y)))
        self.folds.reverse()

    def do_next_fold(self):
        if not self.folds:
            return
        direction, number = self.folds.pop()
        if direction == "x":
            self.fold_left(number)
        if direction == "y":
            self.fold_up(number)

    def follow_instructions(self):
        while self.folds:
            self.do_next_fold()

    def fold_up(self, row):
        new_dots = set()
        for x, y in self.dots:
            if y < row:
                new_dots.add((x, y))
            else:
                new_y = row - (y - row)
                new_dots.add((x, new_y))

        self.dots = new_dots

    def fold_left(self, col):
        new_dots = set()
        for x, y in self.dots:
            if x < col:
                new_dots.add((x, y))
            else:
                new_x = col - (x - col)
                new_dots.add((new_x, y))
        self.dots = new_dots

    def __str__(self):
        out = "\n"
        max_x = max(x for x, y in self.dots)
        max_y = max(y for x, y in self.dots)
        for y in range(max_y + 1):
            for x in range(max_x + 1):
                out += "#" if (x, y) in self.dots else "."
            out += "\n"
        return out

def answer_part_one(data):
    grid = Grid(data)
    grid.do_next_fold()

    return len(grid.dots)


def answer_part_two(data):
    grid = Grid(data)
    grid.follow_instructions()

    return str(grid)


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()

def get_data():
    with open("thirteen.txt") as fh:
        return [line.strip() for line in fh]

if __name__ == "__main__":
    options = opts()

    if options.part_one:
        answer = answer_part_one(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(get_data())
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

TEST_DATA = [
    "6,10",
    "0,14",
    "9,10",
    "0,3",
    "10,4",
    "4,11",
    "6,0",
    "6,12",
    "4,1",
    "0,13",
    "10,12",
    "3,4",
    "3,0",
    "8,4",
    "1,10",
    "2,14",
    "8,10",
    "9,0",
    "",
    "fold along y=7",
    "fold along x=5",
]

class TestGrid(unittest.TestCase):
    def test_init(self):
        grid = Grid(TEST_DATA)
        
        self.assertEqual(len(grid.dots), 18)
        self.assertTrue((4,1) in grid.dots)
        self.assertTrue((10,4) in grid.dots)
        self.assertTrue((1,10) in grid.dots)
        self.assertEqual(len(grid.folds), 2)
        self.assertTrue(("y",7) in grid.folds)
        self.assertTrue(("x",5) in grid.folds)

    def test_do_next_fold(self):
        grid = Grid(TEST_DATA)
        grid.do_next_fold()

        self.assertEqual(len(grid.dots), 17)

    def test_follow_instructions(self):
        grid = Grid(TEST_DATA)
        grid.follow_instructions()

        self.assertEqual(len(grid.dots), 16)

    def test_do_next_fold_empty_list(self):
        grid = Grid(["1,2","3,4"])
        grid.do_next_fold()

        self.assertEqual(len(grid.dots), 2)

    def test_fold_up(self):
        data = ["1,2","4,5","5,1","5,7"]
        grid = Grid(data)
        grid.fold_up(4)

        self.assertEqual(len(grid.dots), 3)
        self.assertTrue((1,2) in grid.dots)
        self.assertTrue((4,3) in grid.dots)
        self.assertTrue((5,1) in grid.dots)

    def test_fold_left(self):
        data = ["1,2","4,5","5,2","5,7"]
        grid = Grid(data)
        grid.fold_left(3)

        self.assertEqual(len(grid.dots), 3)
        self.assertTrue((1,2) in grid.dots)
        self.assertTrue((2,5) in grid.dots)
        self.assertTrue((1,7) in grid.dots)


class TestExampleData(unittest.TestCase):
    def test_answer_part_one(self):
        answer = answer_part_one(TEST_DATA)
        self.assertEqual(answer, 17)

    def test_answer_part_two(self):
        answer = answer_part_two(TEST_DATA)
        expected = """
#####
#...#
#...#
#...#
#####
"""
        self.assertEqual(answer, expected)

unittest.main()
