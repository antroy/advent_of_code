#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, math, re, sys
from collections import defaultdict

class Coord:
    def __init__(self, x, y):
        self.x, self.y = x, y

    def __eq__(self, other):
        if isinstance(other, Coord):
            return self.x == other.x and self.y == other.y
        return False

    def __hash__(self):
        return 13 * self.x + self.y

    def __str__(self):
        return "(%s, %s)" % (self.x, self.y)


class Line:
    def __init__(self, start, end):
        self.start, self.end = start, end

    def is_horizontal(self):
        return self.start.y == self.end.y

    def is_vertical(self):
        return self.start.x == self.end.x

    def is_diagonal(self):
        dx = abs(self.start.x - self.end.x)
        dy = abs(self.start.y - self.end.y)

        return dy == dx

    def _horizontal_points(self):
        x_1, x_2 = sorted([self.start.x, self.end.x])
        return [Coord(x, self.start.y) for x in range(x_1, x_2 + 1)]

    def _vertical_points(self):
        y_1, y_2 = sorted([self.start.y, self.end.y])
        return [Coord(self.start.x, y) for y in range(y_1, y_2 + 1)]

    def _diagonal_points(self):
            out = []

            lowest_x, highest_x = (self.start, self.end) if self.start.x <= self.end.x else (self.end, self.start)

            out.append(lowest_x)

            while out[-1].x < highest_x.x:
                next_x = out[-1].x + 1
                next_y = out[-1].y + 1 if lowest_x.y < highest_x.y else out[-1].y -1
                out.append(Coord(next_x, next_y))
            
            return out

    def points(self):
        if self.is_horizontal():
            return self._horizontal_points()
        if self.is_vertical():
            return self._vertical_points()
        if self.is_diagonal():
            return self._diagonal_points()

        return []


def get_vent_map(lines):
    out = defaultdict(lambda: 0)
    for line in lines:
        for coord in line.points():
            out[coord] += 1
    return out

    
def parse_data(data):
    coords_pattern = r"(\d+),(\d+)\s+->\s+(\d+),(\d+)"
    out = []
    for line in data:
        m = re.match(coords_pattern, line)
        if m:
            start_x, start_y, end_x, end_y = [int(x) for x in m.groups()]
            out.append(Line(Coord(start_x, start_y), Coord(end_x, end_y)))

    return out


def answer_part_one(data):
    lines = parse_data(data)
    vents = get_vent_map(lines)

    out = 0

    for coord in vents:
        if vents[coord] > 1:
            out += 1

    return out


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()


def load_data(filename):
    with open(filename) as fh:
        return [line.strip() for line in fh]


if __name__ == "__main__":
    options = opts()
    
    if options.part_one:
        answer = answer_part_one(load_data("five.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

class TestLine(unittest.TestCase):
    def test_is_horizontal(self):
        line1 = Line(Coord(1, 3), Coord(4, 3))
        line2 = Line(Coord(1, 1), Coord(1, 1))

        self.assertTrue(line1.is_horizontal())
        self.assertTrue(line2.is_horizontal())


    def test_is_not_horizontal(self):
        line1 = Line(Coord(1, 2), Coord(4, 3))
        line2 = Line(Coord(1, 2), Coord(1, 3))

        self.assertFalse(line1.is_horizontal())
        self.assertFalse(line2.is_horizontal())


    def test_is_vertical(self):
        line1 = Line(Coord(4, 2), Coord(4, 3))
        line2 = Line(Coord(1, 1), Coord(1, 1))

        self.assertTrue(line1.is_vertical())
        self.assertTrue(line2.is_vertical())


    def test_is_not_vertical(self):
        line1 = Line(Coord(1, 2), Coord(4, 3))
        line2 = Line(Coord(1, 2), Coord(2, 2))

        self.assertFalse(line1.is_vertical())
        self.assertFalse(line2.is_vertical())

    def test_is_diagonal(self):
        line1 = Line(Coord(0, 0), Coord(4, 4))
        line2 = Line(Coord(1, 3), Coord(4, 6))
        line3 = Line(Coord(2, 2), Coord(2, 2))
        line4 = Line(Coord(4, 4), Coord(6, 2))

        self.assertTrue(line1.is_diagonal())
        self.assertTrue(line2.is_diagonal())
        self.assertTrue(line3.is_diagonal())
        self.assertTrue(line4.is_diagonal())


    def test_is_not_diagonal(self):
        line1 = Line(Coord(1, 2), Coord(4, 6))
        line2 = Line(Coord(1, 2), Coord(1, 6))
        line3 = Line(Coord(1, 2), Coord(7, 2))

        self.assertFalse(line1.is_diagonal())
        self.assertFalse(line2.is_diagonal())
        self.assertFalse(line3.is_diagonal())

    def test_points_on_horizontal_line(self):
        line1 = Line(Coord(2, 5), Coord(4, 5))
        line2 = Line(Coord(4, 5), Coord(2, 5))

        self.assertEqual(line1.points(), [Coord(2, 5), Coord(3, 5), Coord(4, 5)])
        self.assertEqual(line2.points(), [Coord(2, 5), Coord(3, 5), Coord(4, 5)])

    def test_points_on_vertical_line(self):
        line1 = Line(Coord(4, 1), Coord(4, 5))
        line2 = Line(Coord(4, 5), Coord(4, 1))

        self.assertEqual(line1.points(), [Coord(4, 1), Coord(4, 2), Coord(4, 3), Coord(4, 4), Coord(4, 5)])
        self.assertEqual(line2.points(), [Coord(4, 1), Coord(4, 2), Coord(4, 3), Coord(4, 4), Coord(4, 5)])

    def test_points_on_diagonal_line(self):
        line1 = Line(Coord(4, 1), Coord(1, 4))
        line2 = Line(Coord(1, 2), Coord(3, 4))

        print("LINE ONE: %s" % "".join([str(c) for c in line1.points()]))

        self.assertEqual(line1.points(), [Coord(1, 4), Coord(2, 3), Coord(3, 2), Coord(4, 1)])
        self.assertEqual(line2.points(), [Coord(1, 2), Coord(2, 3), Coord(3, 4)])

class TestExampleData(unittest.TestCase):
    TEST_DATA = [
        "0,9 -> 5,9",
        "8,0 -> 0,8",
        "9,4 -> 3,4",
        "2,2 -> 2,1",
        "7,0 -> 7,4",
        "6,4 -> 2,0",
        "0,9 -> 2,9",
        "3,4 -> 1,4",
        "0,0 -> 8,8",
        "5,5 -> 8,2",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.TEST_DATA)
        self.assertEqual(answer, 12)


    def test_parse_input(self):
        data = ["0,9 -> 3,4", "5,27 -> 123,4567"]
        lines = parse_data(data)

        self.assertEqual(lines[0].start.x, 0)
        self.assertEqual(lines[0].start.y, 9)
        self.assertEqual(lines[0].end.x, 3)
        self.assertEqual(lines[0].end.y, 4)

        self.assertEqual(lines[1].start.x, 5)
        self.assertEqual(lines[1].start.y, 27)
        self.assertEqual(lines[1].end.x, 123)
        self.assertEqual(lines[1].end.y, 4567)

    def test_get_vent_map(self):
        lines = parse_data(self.TEST_DATA)

        vents = get_vent_map(lines)
        self.assertEqual(vents[Coord(0, 0)], 1)
        self.assertEqual(vents[Coord(0, 8)], 1)
        self.assertEqual(vents[Coord(0, 9)], 2)
        self.assertEqual(vents[Coord(1, 1)], 1)
        self.assertEqual(vents[Coord(1, 4)], 1)
        self.assertEqual(vents[Coord(1, 7)], 1)
        self.assertEqual(vents[Coord(1, 9)], 2)
        self.assertEqual(vents[Coord(2, 0)], 1)
        self.assertEqual(vents[Coord(2, 1)], 1)
        self.assertEqual(vents[Coord(2, 2)], 2)
        self.assertEqual(vents[Coord(2, 4)], 1)
        self.assertEqual(vents[Coord(2, 6)], 1)
        self.assertEqual(vents[Coord(2, 9)], 2)
        self.assertEqual(vents[Coord(3, 1)], 1)
        self.assertEqual(vents[Coord(3, 3)], 1)
        self.assertEqual(vents[Coord(3, 4)], 2)
        self.assertEqual(vents[Coord(3, 5)], 1)
        self.assertEqual(vents[Coord(3, 9)], 1)
        self.assertEqual(vents[Coord(4, 2)], 1)
        self.assertEqual(vents[Coord(4, 4)], 3)
        self.assertEqual(vents[Coord(4, 9)], 1)
        self.assertEqual(vents[Coord(5, 3)], 2)
        self.assertEqual(vents[Coord(5, 4)], 1)
        self.assertEqual(vents[Coord(5, 5)], 2)
        self.assertEqual(vents[Coord(5, 9)], 1)
        self.assertEqual(vents[Coord(6, 2)], 1)
        self.assertEqual(vents[Coord(6, 4)], 3)
        self.assertEqual(vents[Coord(6, 6)], 1)
        self.assertEqual(vents[Coord(7, 0)], 1)
        self.assertEqual(vents[Coord(7, 1)], 2)
        self.assertEqual(vents[Coord(7, 2)], 1)
        self.assertEqual(vents[Coord(7, 3)], 2)
        self.assertEqual(vents[Coord(7, 4)], 2)
        self.assertEqual(vents[Coord(7, 7)], 1)
        self.assertEqual(vents[Coord(8, 0)], 1)
        self.assertEqual(vents[Coord(8, 2)], 1)
        self.assertEqual(vents[Coord(8, 4)], 1)
        self.assertEqual(vents[Coord(8, 8)], 1)
        self.assertEqual(vents[Coord(9, 4)], 1)


unittest.main()
