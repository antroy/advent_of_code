#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, re, sys
from collections import defaultdict

class Coord:
    def __init__(self, x, y):
        self.x, self.y = x, y

    def __eq__(self, other):
        if isinstance(other, Coord):
            return self.x == other.x and self.y == other.y
        return False

    def __hash__(self):
        return 13 * self.x + self.y

    def __str__(self):
        return "(%s, %s)" % (self.x, self.y)


class Line:
    def __init__(self, start, end):
        self.start, self.end = start, end

    def is_horizontal(self):
        return self.start.y == self.end.y

    def is_vertical(self):
        return self.start.x == self.end.x

    def points(self):
        if self.is_horizontal():
            x_1, x_2 = sorted([self.start.x, self.end.x])
            return [Coord(x, self.start.y) for x in range(x_1, x_2 + 1)]
        if self.is_vertical():
            y_1, y_2 = sorted([self.start.y, self.end.y])
            return [Coord(self.start.x, y) for y in range(y_1, y_2 + 1)]


        return []


def get_vent_map(lines):
    out = defaultdict(lambda: 0)
    for line in lines:
        for coord in line.points():
            out[coord] += 1
    return out

    
def parse_data(data):
    coords_pattern = r"(\d+),(\d+)\s+->\s+(\d+),(\d+)"
    out = []
    for line in data:
        m = re.match(coords_pattern, line)
        if m:
            start_x, start_y, end_x, end_y = [int(x) for x in m.groups()]
            out.append(Line(Coord(start_x, start_y), Coord(end_x, end_y)))

    return out


def answer_part_one(data):
    lines = parse_data(data)
    vents = get_vent_map(lines)

    out = 0

    for coord in vents:
        if vents[coord] > 1:
            out += 1

    return out


def answer_part_two(data):

    return None


def opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--part-one", help="Run solution part one", action="store_true", default=False)
    parser.add_argument("-b", "--part-two", help="Run solution part two", action="store_true", default=False)

    return parser.parse_args()


def load_data(filename):
    with open(filename) as fh:
        return [line.strip() for line in fh]


if __name__ == "__main__":
    options = opts()
    
    if options.part_one:
        answer = answer_part_one(load_data("five.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)
    elif options.part_two:
        answer = answer_part_two(load_data("five.txt"))
        print("Answer: %s" % answer)
        sys.exit(0)


import unittest

class TestLine(unittest.TestCase):
    def test_is_horizontal(self):
        line1 = Line(Coord(1, 3), Coord(4, 3))
        line2 = Line(Coord(1, 1), Coord(1, 1))

        self.assertTrue(line1.is_horizontal())
        self.assertTrue(line2.is_horizontal())


    def test_is_not_horizontal(self):
        line1 = Line(Coord(1, 2), Coord(4, 3))
        line2 = Line(Coord(1, 2), Coord(1, 3))

        self.assertFalse(line1.is_horizontal())
        self.assertFalse(line2.is_horizontal())


    def test_is_vertical(self):
        line1 = Line(Coord(4, 2), Coord(4, 3))
        line2 = Line(Coord(1, 1), Coord(1, 1))

        self.assertTrue(line1.is_vertical())
        self.assertTrue(line2.is_vertical())


    def test_is_not_vertical(self):
        line1 = Line(Coord(1, 2), Coord(4, 3))
        line2 = Line(Coord(1, 2), Coord(2, 2))

        self.assertFalse(line1.is_vertical())
        self.assertFalse(line2.is_vertical())

    def test_points_on_horizontal_line(self):
        line1 = Line(Coord(2, 5), Coord(4, 5))
        line2 = Line(Coord(4, 5), Coord(2, 5))

        self.assertEqual(line1.points(), [Coord(2, 5), Coord(3, 5), Coord(4, 5)])
        self.assertEqual(line2.points(), [Coord(2, 5), Coord(3, 5), Coord(4, 5)])

    def test_points_on_vertical_line(self):
        line1 = Line(Coord(4, 1), Coord(4, 5))
        line2 = Line(Coord(4, 5), Coord(4, 1))

        self.assertEqual(line1.points(), [Coord(4, 1), Coord(4, 2), Coord(4, 3), Coord(4, 4), Coord(4, 5)])
        self.assertEqual(line2.points(), [Coord(4, 1), Coord(4, 2), Coord(4, 3), Coord(4, 4), Coord(4, 5)])

class TestExampleData(unittest.TestCase):
    TEST_DATA = [
        "0,9 -> 5,9",
        "8,0 -> 0,8",
        "9,4 -> 3,4",
        "2,2 -> 2,1",
        "7,0 -> 7,4",
        "6,4 -> 2,0",
        "0,9 -> 2,9",
        "3,4 -> 1,4",
        "0,0 -> 8,8",
        "5,5 -> 8,2",
    ]

    def test_answer_part_one(self):
        answer = answer_part_one(self.TEST_DATA)
        self.assertEqual(answer, 5)


    def test_answer_part_two(self):
        data = [
        ]
        answer = answer_part_two(data)
        self.assertEqual(answer, None)


    def test_parse_input(self):
        data = ["0,9 -> 3,4", "5,27 -> 123,4567"]
        lines = parse_data(data)

        self.assertEqual(lines[0].start.x, 0)
        self.assertEqual(lines[0].start.y, 9)
        self.assertEqual(lines[0].end.x, 3)
        self.assertEqual(lines[0].end.y, 4)

        self.assertEqual(lines[1].start.x, 5)
        self.assertEqual(lines[1].start.y, 27)
        self.assertEqual(lines[1].end.x, 123)
        self.assertEqual(lines[1].end.y, 4567)

    def test_get_vent_map(self):
        lines = parse_data(self.TEST_DATA)

        vents = get_vent_map(lines)
        self.assertEqual(vents[Coord(7, 0)], 1)
        self.assertEqual(vents[Coord(2, 1)], 1)
        self.assertEqual(vents[Coord(7, 1)], 1)
        self.assertEqual(vents[Coord(2, 2)], 1)
        self.assertEqual(vents[Coord(7, 2)], 1)
        self.assertEqual(vents[Coord(7, 3)], 1)
        self.assertEqual(vents[Coord(1, 4)], 1)
        self.assertEqual(vents[Coord(2, 4)], 1)
        self.assertEqual(vents[Coord(3, 4)], 2)
        self.assertEqual(vents[Coord(4, 4)], 1)
        self.assertEqual(vents[Coord(5, 4)], 1)
        self.assertEqual(vents[Coord(6, 4)], 1)
        self.assertEqual(vents[Coord(7, 4)], 2)
        self.assertEqual(vents[Coord(8, 4)], 1)
        self.assertEqual(vents[Coord(9, 4)], 1)
        self.assertEqual(vents[Coord(0, 9)], 2)
        self.assertEqual(vents[Coord(1, 9)], 2)
        self.assertEqual(vents[Coord(2, 9)], 2)
        self.assertEqual(vents[Coord(3, 9)], 1)
        self.assertEqual(vents[Coord(4, 9)], 1)
        self.assertEqual(vents[Coord(5, 9)], 1)


unittest.main()
