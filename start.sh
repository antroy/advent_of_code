#!/usr/bin/env bash

day=$1

if [ -z $day ]
then
  echo "Usage: $0 <day>"
  exit 1
fi

year=$(date "+%Y")
folder="${year}/${day}"

git pull

if [ ! -f "${folder}/${day}.py" ]
then
  mkdir -p ${folder}
  cp "${HOME}/Downloads/${day}.txt" "${folder}/"
  cp "template_answer.py" "${folder}/${day}.py"
  sed -i "s/xxx/${day}/" "${folder}/${day}.py"

  git add "${folder}"
fi

tmux split-window -h -d
tmux split-window -v -d
tmux split-window -v -d

tmux send-keys -t "advent:editor.0" "cd ${folder}" Enter
tmux send-keys -t "advent:editor.0" "ipython" Enter
tmux send-keys -t "advent:editor.1" "cd ${folder}" Enter
tmux send-keys -t "advent:editor.1" "echo ${day}.py | entr pytest -q ./${day}.py" Enter
tmux send-keys -t "advent:editor.2" "cd ${folder}" Enter
tmux send-keys -t "advent:editor.3" "cd ${folder}" Enter
tmux send-keys -t "advent:editor.3" "vim ${day}.py" Enter

tmux select-pane -t "advent:editor.3"
